package com.aquila.chess;

import java.util.Random;

import org.junit.jupiter.api.Test;

import com.aquila.chess.exception.EndOfGameException;
import com.aquila.chess.manager.GameManager;
import com.aquila.chess.manager.Sequence;
import com.aquila.chess.player.ChessPlayer;
import com.aquila.chess.player.RandomPlayer;
import com.aquila.chess.player.mcts.ResultGame;
import com.aquila.chess.utils.Utils;

class GameManagerTest {

	@Test
	void test() throws Exception {
		GameManager gameManager = new GameManager("todel.csv", 6, 55);
		Random rand = new Random();
		int seed = (int) System.currentTimeMillis();
		for (int i = 0; i < 47; i++) {
			final Board board = new Board();
			final ChessPlayer whitePlayer = new RandomPlayer(seed + rand.nextInt(1000));
			final ChessPlayer blackPlayer = new RandomPlayer(seed + rand.nextInt(1000));
			final Game game = new Game(board, whitePlayer, blackPlayer);
			game.initWithAllPieces();
			Sequence sequence = gameManager.createSequence();
			try {
				do {
					game.play(false);
					sequence.play();
					Utils.check(game);
				} while (true);
			} catch (final EndOfGameException e) {
				ResultGame resultGame = whitePlayer.getResultGame(e);
				gameManager.endGame(resultGame, game, 0.5555, e, sequence);
			}
		}
	}

}
