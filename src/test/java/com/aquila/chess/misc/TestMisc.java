package com.aquila.chess.misc;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class TestMisc {
	static private Logger logger = LoggerFactory.getLogger(TestMisc.class);


	@Test
	void normaliseDoubleArray() {
		double[] policies = new double[] { 0.1, 0.2, 0.5 };
		double min = DoubleStream.of(policies).min().getAsDouble();
		double max = DoubleStream.of(policies).max().getAsDouble();
		// Min-max feature scaling
		List<Double> normalisedPolicies = DoubleStream.of(policies).map((x) -> {
			return (x - min) / (max - min);
		}).boxed().collect(Collectors.toList());
		DoubleStream.of(policies).boxed().forEach(System.out::println);

		System.out.println("-------  Normalised---------");
		normalisedPolicies.forEach(System.out::println);

		System.out.println("--- Revert Normalised -----");
		List<Double> revertNormalisedPolicies = DoubleStream.of(policies).map((x) -> {
			return 1 - (x - min) / (max - min);
		}).boxed().collect(Collectors.toList());
		revertNormalisedPolicies.forEach(System.out::println);
	}

	@Test
	void testFitChunk() {
		int FIT_CHUNK = 50;
		int nbStep = 230;

		List<Integer> list = new ArrayList<>();
		for (int i = 0; i < nbStep; i++) {
			list.add(i);
		}
		int nbChunk = nbStep / FIT_CHUNK;
		int restChunk = nbStep % FIT_CHUNK;
		Integer[] integers = new Integer[FIT_CHUNK];
		for (int indexChunk = 0; indexChunk < nbChunk; indexChunk++) {
			for (int i = 0; i < FIT_CHUNK; i++) {
				integers[i] = list.get(indexChunk * FIT_CHUNK + i);
			}
			logger.info("\nFIT");
			for (int i = 0; i < FIT_CHUNK; i++) {
				logger.info(integers[i] + ",");
			}
		}
		Integer[] restIntegers = new Integer[restChunk];
		for (int i = 0; i < restChunk; i++) {
			restIntegers[i] = list.get(nbChunk * FIT_CHUNK + i);
		}
		logger.info("\nFIT");
		for (int i = 0; i < restChunk; i++) {
			logger.info(restIntegers[i] + ",");
		}
	}

}
