package com.aquila.chess.misc;

import org.junit.jupiter.api.Test;

public class FlipBoardTest {

	@Test
	public void testFlip() {
		final String[][] board = new String[][] { { "A1", "B1", "C1", "D1" }, { "A2", "B2", "C2", "D2" },
				{ "A3", "B3", "C3", "D3" }, { "A4", "B4", "C4", "D4" } };
				final String[][] newBoard = new String[][] { { "A1", "B1", "C1", "D1" }, { "A2", "B2", "C2", "D2" },
					{ "A3", "B3", "C3", "D3" }, { "A4", "B4", "C4", "D4" } };
		System.out.println("-----------------------------------------------------");
		display(board);
		for (int x = 0; x < 4; x++) {
			for (int y = 0; y < 4; y++) {
				// String s = board[y][x];
				newBoard[x][y] = board[3-x][3-y];
				// board[x][y] = s;
			}
		}
		System.out.println("-----------------------------------------------------");
		display(newBoard);
	}

	public void display(final String[][] board) {
		for (int x = 0; x < 4; x++) {
			for (int y = 0; y < 4; y++) {
				System.out.printf("%s", board[x][y]);
			}
			System.out.println();
		}

	}
}
