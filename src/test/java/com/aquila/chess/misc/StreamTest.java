package com.aquila.chess.misc;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StreamTest {

	static private Logger logger = LoggerFactory.getLogger(StreamTest.class);

	private static class TestChoosedNode {
		public TestChoosedNode(final float whiteWin, final float blackWin, final float drawn, final String sz) {
			this.sz = sz;
			this.whiteWin = whiteWin;
			this.blackWin = blackWin;
			this.drawn = drawn;
		}

		final String sz;
		final float whiteWin;
		final float blackWin;
		final float drawn;

		/**
		 * @return the node
		 */
		public String getSz() {
			return sz;
		}

		/**
		 * @return the whiteWin
		 */
		public float getWhiteWin() {
			return whiteWin;
		}

		/**
		 * @return the blackWin
		 */
		public float getBlackWin() {
			return blackWin;
		}

		/**
		 * @return the drawn
		 */
		public float getDrawn() {
			return drawn;
		}

		@Override
		public String toString() {
			return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
		}
	}

	@Test
	public void testStreamNode() {
		final TestChoosedNode chooseNodes[] = new TestChoosedNode[] { //
				new TestChoosedNode(0.1F, 0.1F, 0.3F, "1"), //
				new TestChoosedNode(0.3F, 0.2F, 0.3F, "2"), //
				new TestChoosedNode(0.1F, 0.2F, 0.3F, "3"), //
				new TestChoosedNode(0.3F, 0.3F, 0.3F, "4"), //
				new TestChoosedNode(0.1F, 0.4F, 0.3F, "5"), //
				new TestChoosedNode(0.3F, 0.1F, 0.1F, "6"), //
				new TestChoosedNode(0.3F, 0.1F, 0.2F, "7"), //
				new TestChoosedNode(0.3F, 0.1F, 0.1F, "8"), //
				new TestChoosedNode(0.1F, 0.2F, 0.3F, "9"), //
				new TestChoosedNode(0.3F, 0.1F, 0.3F, "10") };

		final List<TestChoosedNode> list = Arrays.asList(chooseNodes);

		final float maxWhiteWin = list.stream().max(Comparator.comparingDouble(TestChoosedNode::getWhiteWin)).get()
				.getWhiteWin();
		final List<TestChoosedNode> listMaxWhiteWin = list.stream()//
				.filter(testStreamNode -> testStreamNode.getWhiteWin() == maxWhiteWin)//
				.collect(Collectors.toList());

		final float maxWhiteWinAndMinBlackWin = listMaxWhiteWin.stream()//
				.min(Comparator.comparingDouble(TestChoosedNode::getBlackWin)).get().getBlackWin();
		logger.info(String.format("maxWhiteWinAndMinBlackWin=%f", maxWhiteWinAndMinBlackWin));
		final List<TestChoosedNode> listMaxWhiteWinMinBlackWin = listMaxWhiteWin.stream()//
				.filter(testStreamNode -> testStreamNode.getBlackWin() == maxWhiteWinAndMinBlackWin)//
				.collect(Collectors.toList());
		final float maxWhiteWinAndMinBlackWinAndMinDrawn = listMaxWhiteWinMinBlackWin.stream()//
				.min(Comparator.comparingDouble(TestChoosedNode::getDrawn)).get().getDrawn();
		logger.info(
				String.format("maxWhiteWinAndMinBlackWinAndMinDrawn=%f", maxWhiteWinAndMinBlackWinAndMinDrawn));
		final List<TestChoosedNode> listMaxWhiteWinMinBlackWinAndMinDrawn = listMaxWhiteWinMinBlackWin.stream()//
				.filter(testStreamNode -> testStreamNode.getDrawn() == maxWhiteWinAndMinBlackWinAndMinDrawn)//
				.collect(Collectors.toList());
		listMaxWhiteWinMinBlackWinAndMinDrawn.forEach(System.out::println);
	}

	@Test
	public void testStream() {
		final List<Integer> list = List.of(1, 3, 5, 3, 2, 3, 5);
		list.stream()//
				.filter(i -> i == (list.stream().max(Comparator.comparingInt(i2 -> i2))).get())//
				.forEach(System.out::println);
	}
}
