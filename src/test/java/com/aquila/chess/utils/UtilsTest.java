package com.aquila.chess.utils;

import java.util.stream.DoubleStream;

import org.junit.jupiter.api.Test;

import umontreal.ssj.randvarmulti.DirichletGen;
import umontreal.ssj.rng.MRG32k3a;

import static org.junit.jupiter.api.Assertions.assertEquals;

class UtilsTest {

	@Test
	void testNormalise() {
		double[] policies = new double[] { 0.1, 0.2, 0.4, 0.5 };

		System.out.println("-------  Policies---------");
		DoubleStream.of(policies).forEach(policy -> System.out.print(policy + ","));
		System.out.println("-------  Normalised---------");
		double[] normalisedPolicies = Utils.normalise(policies);
		DoubleStream.of(normalisedPolicies).forEach(policy -> System.out.print(policy + ","));
	}

	@Test
	void testDirichlet() {
		MRG32k3a stream = new MRG32k3a();
		double[] alpha = new double[] { 0.3, 0.3, 0.3 };
		for (int loop = 0; loop < 5; loop++) {
			DirichletGen dirichletGen = new DirichletGen(stream, alpha);
			double[] p = new double[alpha.length];
			dirichletGen.nextPoint(p);
			for (int i = 0; i < p.length; i++) {
				System.out.println(String.format("dirichletGen[%d]: %f", i, p[i]));
			}
		}
	}

	@Test
	void testNormalizeUtilsNoDirichlet() {
		double[] policies = new double[] { 0.1, 0.2, 0.4, 0.9, 0.2, 0.2 };
		int[] indexes={0,1,2,3};
		double[] ret = Utils.toDistribution(policies, indexes,false);
		double sum=0.0;
		for(int i=0; i<ret.length; i++) {
			sum += ret[i];
		}
		assertEquals(1.0, sum);
	}

	@Test
	void testNormalizeUtilsWithDirichlet() {
		double[] policies = new double[] { 0.1, 0.2, 0.4, 0.9, 0.2, 0.2 };
		int[] indexes={0,1,2,3};
		double[] ret = Utils.toDistribution(policies, indexes,true);
		double sum=0.0;
		for(int i=0; i<ret.length; i++) {
			sum += ret[i];
		}
		assertEquals(1.0, sum);
	}
}
