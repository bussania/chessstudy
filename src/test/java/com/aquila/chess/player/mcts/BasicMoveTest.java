package com.aquila.chess.player.mcts;

import com.aquila.chess.Board;
import com.aquila.chess.Game;
import com.aquila.chess.Location;
import com.aquila.chess.Move;
import com.aquila.chess.exception.ChessPositionException;
import com.aquila.chess.pieces.Color;
import com.aquila.chess.player.ChessPlayer;
import com.aquila.chess.player.RandomPlayer;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class BasicMoveTest {

	@Test
	void testMoveAndBasicMoveHashcode() throws ChessPositionException {
		final Board board = new Board();
		final ChessPlayer whitePlayer = new RandomPlayer(0);
		final ChessPlayer blackPlayer = new RandomPlayer(0);
		final Game game = new Game(board, whitePlayer, blackPlayer);
		game.initWithAllPieces();
		game.setColorToPlay(Color.WHITE);

		BasicMove basicMove = new BasicMove(0, 1, 0, 2, game.getPiece(Location.get(0,1)));
		Move move = new Move(basicMove, game.getBoard());
		assertEquals(basicMove.hashCode(), move.hashCode());
		
		basicMove = new BasicMove(0, 1, 0, 3, game.getPiece(Location.get(0,1)));
		move = new Move(game.getBoard(), Color.WHITE, "a2-a4");
		assertEquals(basicMove.hashCode(), move.hashCode());
	}

}
