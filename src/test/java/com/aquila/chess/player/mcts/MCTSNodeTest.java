package com.aquila.chess.player.mcts;

import com.aquila.chess.Board;
import com.aquila.chess.Game;
import com.aquila.chess.Location;
import com.aquila.chess.Move;
import com.aquila.chess.exception.ChessPositionException;
import com.aquila.chess.player.ChessPlayer;
import com.aquila.chess.player.RandomPlayer;
import org.junit.jupiter.api.Test;

class MCTSNodeTest {

    @Test
    void testSetActionValue() throws ChessPositionException {
        final Board board = new Board();
        final ChessPlayer whitePlayer = new RandomPlayer(1);
        final ChessPlayer blackPlayer = new RandomPlayer(1);
        final Game game = new Game(board, whitePlayer, blackPlayer);
        game.initWithAllPieces();
        BasicMove basicMove = new BasicMove(0, 1, 0, 3, game.getPiece(Location.get(0, 1)));
        Move move = new Move(basicMove, game.getBoard());
        new MCTSNode(move, 0.0, 0.0);
    }

}
