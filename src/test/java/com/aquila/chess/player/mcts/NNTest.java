package com.aquila.chess.player.mcts;

import org.deeplearning4j.nn.api.NeuralNetwork;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.aquila.chess.player.mcts.DeepLearningAGZ.MAX_POLICY_INDEX;

public class NNTest implements INN {

    private final double step = 0.001;

    private double value = 0.1;

    @Override
    public void reset() {

    }

    @Override
    public double getScore() {
        return 0;
    }

    @Override
    public void setUpdateLr(UpdateLr updateLr, int nbGames) {

    }

    @Override
    public void updateLr(int nbGames) {

    }

    @Override
    public void save() throws IOException {

    }

    @Override
    public void fit(double[][][][] inputs, double[][] policies, double[][] values) {

    }

    @Override
    public String getFilename() {
        return null;
    }

    @Override
    public double getLR() {
        return 0;
    }

    @Override
    public void setLR(double lr) {

    }

    @Override
    public List<OutputNN> outputs(double[][][][] nbIn, int len) {
        List<OutputNN> ret = new ArrayList<>();
        for (int i = 0; i < len; i++) {
            this.value += this.step;
            double value = this.value;
            double[] policies = new double[MAX_POLICY_INDEX];
            Arrays.fill(policies, 0.1);
            ret.add(new OutputNN(value, policies));
        }
        return ret;
    }

    @Override
    public NeuralNetwork getNetwork() {
        return null;
    }

}
