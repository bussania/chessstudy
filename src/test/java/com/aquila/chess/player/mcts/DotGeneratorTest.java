package com.aquila.chess.player.mcts;

import com.aquila.chess.Move;
import com.aquila.chess.Moves;
import com.aquila.chess.pieces.Color;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aquila.chess.Board;
import com.aquila.chess.Game;
import com.aquila.chess.exception.ChessPositionException;
import com.aquila.chess.exception.EndOfGameException;
import com.aquila.chess.player.ChessPlayer;
import com.aquila.chess.player.RandomPlayerFirstLevel;
import com.aquila.chess.utils.DotGenerator;

import info.leadinglight.jdot.Graph;

import java.util.Random;

import static org.mockito.ArgumentMatchers.any;

class DotGeneratorTest {
	static private Logger logger = LoggerFactory.getLogger(DotGeneratorTest.class);

	DeepLearningAGZ deepLearningWhite;
	DeepLearningAGZ deepLearningBlack;

	@BeforeEach
	public void initMockDeepLearning() {
		deepLearningWhite = Mockito.mock(DeepLearningAGZ.class);
		Mockito.when(deepLearningWhite.getValue(//
				any(Game.class), //
				any(Color.class), //
				any(Move.class), //
				any(Moves.class),
				any(Boolean.class), //
				any(MCTSSearch.Statistic.class))).thenReturn(-0.234);
		Mockito.when(deepLearningWhite.getPolicy(//
				any(Game.class), //
				any(Color.class), //
				any(Move.class), //
				any(Moves.class),
				any(Boolean.class), //
				any(MCTSSearch.Statistic.class))).thenReturn(0.004);
		deepLearningBlack = Mockito.mock(DeepLearningAGZ.class);
		Mockito.when(deepLearningBlack.getValue(//
				any(Game.class), //
				any(Color.class), //
				any(Move.class), //
				any(Moves.class),
				any(Boolean.class), //
				any(MCTSSearch.Statistic.class))).thenReturn(0.456);
		Mockito.when(deepLearningBlack.getPolicy(//
				any(Game.class), //
				any(Color.class), //
				any(Move.class), //
				any(Moves.class),
				any(Boolean.class), //
				any(MCTSSearch.Statistic.class))).thenReturn(0.005);
	}
	UpdateCpuct updateCpuct = (nbStep) -> 
	{
		return Math.exp(-0.04 * nbStep) / 2;
	};

	@RepeatedTest(1)
	void testGenerate() throws ChessPositionException {
		playMCTS(10);
	}

	@Test
	void testBegining() throws ChessPositionException {
		playMCTS(2);
	}
	
	void playMCTS(final int nbStepMax) throws ChessPositionException {
		final MCTSPlayer whitePlayer = new MCTSPlayer(deepLearningWhite, true, 5, updateCpuct, 2000);
		final ChessPlayer blackPlayer = new RandomPlayerFirstLevel(10);
		final Board board = new Board();
		final Game game = new Game(board, whitePlayer, blackPlayer);
		game.initWithAllPieces();
		int nbStep = 0;
		do {
			try {
				if (nbStep++ > nbStepMax)
					break;
				game.play();
			} catch (final EndOfGameException e) {
				logger.error("End of Game: {}", e);
				break;
			}
		} while (true);
		logger.info(game.toPGN());
		final MCTSNode node = whitePlayer.getRoot();
		final DotGenerator dotGenerator = new DotGenerator();
		final Graph graph = dotGenerator.generate(node, 15);
		logger.info(
				"##########################################################################################################");
		logger.info(graph.toDot());
	}

}
