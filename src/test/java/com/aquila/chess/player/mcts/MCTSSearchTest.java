package com.aquila.chess.player.mcts;

import com.aquila.chess.Board;
import com.aquila.chess.Game;
import com.aquila.chess.exception.ChessPositionException;
import com.aquila.chess.exception.EndOfGameException;
import com.aquila.chess.pieces.Color;
import com.aquila.chess.player.RandomPlayer;
import com.aquila.chess.utils.DotGenerator;
import info.leadinglight.jdot.Graph;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Random;

class MCTSSearchTest {

    // static private final String NN_REFERENCE = "../AGZ_NN/AGZ.reference";
    static private final String NN_REFERENCE = "not_used";

    static private Logger logger = LoggerFactory.getLogger(MCTSSearchTest.class);

    INN nnWhite;

    UpdateCpuct updateCpuct = (nbStep) -> {
        return 0.5;
    };

    @BeforeEach
    public void init() {
        nnWhite = new NNTest();
    }

    @Test
    void testSearch() throws ChessPositionException {
        final DeepLearningAGZ deepLearning = new DeepLearningAGZ(nnWhite);
        MCTSPlayer playerWhite = new MCTSPlayer(deepLearning, false, 0, updateCpuct, 200).withNbMaxSearchCalls(10);
        RandomPlayer playerBlack = new RandomPlayer(0);
        final Game game = new Game(//
                new Board(), //
                playerWhite, //
                playerBlack);
        game.initWithAllPieces();
        MCTSSearch mctsSearch = new MCTSSearch(playerWhite, deepLearning, playerWhite.getRoot(), game, Color.WHITE,
                updateCpuct, new Random());
        mctsSearch.search(0, System.currentTimeMillis(), 0, 30);
        logger.info("parent:{}", playerWhite.getRoot());
        final MCTSNode node = playerWhite.getRoot();
        final DotGenerator dotGenerator = new DotGenerator();
        final Graph graph = dotGenerator.generate(node, 15);
        logger.warn(
                "##########################################################################################################");
        logger.warn(graph.toDot());
    }

    /**
     * @throws ChessPositionException
     * @throws EndOfGameException
     * @formatter:off <pre>
     * [a] [b] [c] [d] [e] [f] [g] [h]
     * 8  --- --- --- --- --- --- --- ---  8
     * 7  --- --- --- --- --- --- --- ---  7
     * 6  --- --- --- --- --- --- --- ---  6
     * 5  --- --- --- --- --- --- --- ---  5
     * 4  --- --- --- --- --- --- --- ---  4
     * 3  --- --- --- --- --- --- K-B ---  3
     * 2  P-B --- --- --- --- --- --- ---  2
     * 1  --- --- --- --- --- --- --- K-W  1
     * [a] [b] [c] [d] [e] [f] [g] [h]
     * </pre>
     * @formatter:on
     */
    @Test
    void testSearchFinal() throws ChessPositionException {
        final DeepLearningAGZ deepLearning = new DeepLearningAGZ(nnWhite);
        deepLearning.setDirichlet(game -> {
            return true;
        });
        MCTSPlayer playerWhite = new MCTSPlayer(deepLearning, false, 0, updateCpuct, 200).withNbMaxSearchCalls(15);
        RandomPlayer playerBlack = new RandomPlayer(0);
        final Board board = new Board();
        final Game game = new Game(board, playerWhite, playerBlack);

        playerWhite.addPieces("KH1");
        playerBlack.addPieces("PA2,KG3");

        game.init();
        MCTSSearch mctsSearch = new MCTSSearch(playerWhite, deepLearning, playerWhite.getRoot(), game, Color.WHITE,
                updateCpuct, new Random());
        mctsSearch.search(0, System.currentTimeMillis(), 0, 5);
        logger.info("parent:{}", playerWhite.getRoot());
        final MCTSNode node = playerWhite.getRoot();
        final DotGenerator dotGenerator = new DotGenerator();
        final Graph graph = dotGenerator.generate(node, 15);
        logger.info(
                "##########################################################################################################");
        logger.warn(graph.toDot());
    }

    @Test
    void testInitSearch() throws ChessPositionException {
        final DeepLearningAGZ deepLearning = new DeepLearningAGZ(nnWhite);
        MCTSPlayer playerWhite = new MCTSPlayer(deepLearning, false, 0, updateCpuct, 200);
        RandomPlayer playerBlack = new RandomPlayer(0);
        final Game game = new Game(//
                new Board(), //
                playerWhite, //
                playerBlack);
        game.initWithAllPieces();
        long startTime = System.currentTimeMillis();
        for (int i = 0; i < 1000; i++) {
            game.copy(playerWhite, playerBlack);
        }
        long endTime = System.currentTimeMillis();
        logger.info("Delay: {} ms", (endTime - startTime));
    }

    @Test
    void testSearchWithBatch() throws ChessPositionException {
        final DeepLearningAGZ deepLearning = new DeepLearningAGZ(nnWhite);
        MCTSPlayer playerWhite = new MCTSPlayer(deepLearning, false, 0, updateCpuct, 200);
        RandomPlayer playerBlack = new RandomPlayer(0);
        final Game game = new Game(//
                new Board(), //
                playerWhite, //
                playerBlack);
        game.initWithAllPieces();
        MCTSSearch mctsSearch = new MCTSSearch(playerWhite, deepLearning, playerWhite.getRoot(), game, Color.WHITE,
                updateCpuct, new Random());
        mctsSearch.search(0, System.currentTimeMillis(), 0, 3);
        System.out.println(DotGenerator.toString(playerWhite.getRoot(), 10));
    }

}
