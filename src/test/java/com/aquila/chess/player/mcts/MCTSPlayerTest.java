package com.aquila.chess.player.mcts;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;

import java.io.IOException;
import java.util.Random;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aquila.chess.Board;
import com.aquila.chess.Game;
import com.aquila.chess.Location;
import com.aquila.chess.Move;
import com.aquila.chess.Moves;
import com.aquila.chess.exception.ChessPositionException;
import com.aquila.chess.exception.EndOfGameException;
import com.aquila.chess.exception.EndOfGameException.TypeOfEnding;
import com.aquila.chess.pieces.Color;
import com.aquila.chess.pieces.King;
import com.aquila.chess.pieces.Rook;
import com.aquila.chess.player.ChessPlayer;
import com.aquila.chess.player.RandomPlayer;
import com.aquila.chess.player.RandomPlayerFirstLevel;
import com.aquila.chess.player.StaticPlayer;
import com.aquila.chess.utils.DotGenerator;

import info.leadinglight.jdot.Graph;

class MCTSPlayerTest {

	static private Logger logger = LoggerFactory.getLogger(MCTSPlayerTest.class);
	static private Logger logFile = LoggerFactory.getLogger("FILE");

	UpdateCpuct updateCpuct = (nbStep) -> {
		return 4; // Math.exp(-0.04 * nbStep) / 2;
	};

	DeepLearningAGZ deepLearningWhite;
	DeepLearningAGZ deepLearningBlack;

	Random rand = new Random();

	@BeforeEach
	public void initMockDeepLearning() {
		deepLearningWhite = Mockito.mock(DeepLearningAGZ.class);
		Mockito.when(deepLearningWhite.getValue(//
				any(Game.class), //
				any(Color.class), //
				any(Move.class), //
				any(Moves.class),
				any(Boolean.class), //
				any(MCTSSearch.Statistic.class))).thenReturn(-0.234);
		Mockito.when(deepLearningWhite.getPolicy(//
				any(Game.class), //
				any(Color.class), //
				any(Move.class), //
				any(Moves.class),
				any(Boolean.class), //
				any(MCTSSearch.Statistic.class))).thenReturn(0.004);
		deepLearningBlack = Mockito.mock(DeepLearningAGZ.class);
		Mockito.when(deepLearningBlack.getValue(//
				any(Game.class), //
				any(Color.class), //
				any(Move.class), //
				any(Moves.class),
				any(Boolean.class), //
				any(MCTSSearch.Statistic.class))).thenReturn(0.456);
		Mockito.when(deepLearningBlack.getPolicy(//
				any(Game.class), //
				any(Color.class), //
				any(Move.class), //
				any(Moves.class),
				any(Boolean.class), //
				any(MCTSSearch.Statistic.class))).thenReturn(0.005);
	}

	/**
	 * @throws ChessPositionException 
	 * @throws EndOfGameException 
	 * @formatter:off
	 * <pre>
    [a] [b] [c] [d] [e] [f] [g] [h] 
 8  --- --- --- --- --- --- --- ---  8  
 7  --- --- --- --- --- --- --- ---  7  
 6  --- --- --- K-W --- --- --- ---  6  
 5  --- --- --- --- --- --- --- ---  5  
 4  --- --- --- --- --- --- --- ---  4  
 3  p-B --- K-B --- --- --- --- ---  3  
 2  --- --- --- --- --- --- --- ---  2  
 1  --- --- --- --- --- --- --- ---  1  
    [a] [b] [c] [d] [e] [f] [g] [h] 
	 * </pre>
	 * @formatter:on
	 */
	@Test
	void testSimulationDetectPossiblePromotion() throws ChessPositionException {
		final int seed = 1;
		final Board board = new Board();
		final ChessPlayer whitePlayer = new StaticPlayer("G8-H8;H8-G8;G8-H8;H8-G8");
		final MCTSPlayer blackPlayer = new MCTSPlayer(deepLearningBlack, true, 1, updateCpuct, -1)
				.withNbMaxSearchCalls(1000);
		final Game game = new Game(board, whitePlayer, blackPlayer);

		whitePlayer.addPieces("KG8");
		blackPlayer.addPieces("PA3,KG6");
		game.init();

		for (int i = 0; i < 8; i++) {
			try {
				Move move = game.play();
				logger.warn("move: {}", move);
				if (move.isGettingPromoted()) {
					return;
				}
			} catch (EndOfGameException | ChessPositionException e) {
				assertTrue(false, "End of game not expected: " + e);
			}
		}
		assertTrue(false, "We should have get promoted");
	}

	/**
	 * @throws ChessPositionException 
	 * @throws EndOfGameException 
	 * @formatter:off
	 * <pre>
    [a] [b] [c] [d] [e] [f] [g] [h] 
 8  --- --- --- --- --- --- --- ---  8  
 7  --- --- --- --- --- --- --- ---  7  
 6  --- --- --- --- --- --- --- ---  6  
 5  --- --- --- --- --- --- --- ---  5  
 4  --- --- --- --- --- --- --- ---  4  
 3  --- --- --- --- --- --- K-B ---  3  
 2  P-B --- --- --- --- --- --- ---  2  
 1  --- --- --- --- --- --- --- K-W  1  
    [a] [b] [c] [d] [e] [f] [g] [h] 
	 * </pre>
	 * @formatter:on
	 */
	@Test
	void testEndtWithPromotion() throws ChessPositionException {
		final int seed = 1;
		final Board board = new Board();
		final ChessPlayer whitePlayer = new RandomPlayer(seed);
		final MCTSPlayer blackPlayer = new MCTSPlayer(deepLearningBlack, true, 1, updateCpuct, -1) //
				.withNbMaxSearchCalls(500);
		final Game game = new Game(board, whitePlayer, blackPlayer);

		whitePlayer.addPieces("KH1");
		blackPlayer.addPieces("PA3,KG3");

		game.init();
		boolean good = false;
		try {
			for (int i = 0; i < 5; i++) {
				final Move move = game.play();
				logger.warn("move: {}", move);
			}
		} catch (final EndOfGameException e) {
			assertTrue(e.getTypeOfEnding() == TypeOfEnding.CHESSMATE);
			assertTrue(e.getColor() == Color.BLACK);
			good = true;
		}
		final DotGenerator dotGenerator = new DotGenerator();
		final Graph graph = dotGenerator.generate(blackPlayer.getRoot(), 4);
		logFile.warn("testEndtWithPromotion:\n");
		logFile.warn(graph.toDot());
		assertTrue(good);
	}

	/**
	 * @throws ChessPositionException 
	 * @throws EndOfGameException 
	 * @formatter:off
	 * <pre>
    [a] [b] [c] [d] [e] [f] [g] [h] 
 8  --- --- --- --- --- --- --- ---  8  
 7  --- --- --- --- --- --- --- ---  7  
 6  --- --- --- --- --- --- --- ---  6  
 5  --- --- --- --- --- --- --- ---  5  
 4  --- --- --- --- --- --- --- ---  4  
 3  P-B --- --- --- --- K-B --- ---  3  
 2  --- --- --- --- --- --- --- ---  2  
 1  --- --- --- --- --- --- --- K-W  1  
    [a] [b] [c] [d] [e] [f] [g] [h] 
	 * </pre>
	 * @formatter:on
	 */
	@Test
	void testAvoidEndtWithPromotion() throws ChessPositionException {
		final Board board = new Board();
		final MCTSPlayer whitePlayer = new MCTSPlayer(deepLearningWhite, false, 1, updateCpuct, -1)
				.withNbMaxSearchCalls(100);
		final MCTSPlayer blackPlayer = new MCTSPlayer(deepLearningBlack, true, 1, updateCpuct, 100);
		final Game game = new Game(board, whitePlayer, blackPlayer);

		whitePlayer.addPieces("KH1");
		blackPlayer.addPieces("PA2,KF2");

		game.init();
		try {
			for (int i = 0; i < 5; i++) {
				Move move = game.play();
				logger.warn("move: {}", move);
			}
		} catch (final EndOfGameException e) {
			logger.warn(game.toString());
			final DotGenerator dotGenerator = new DotGenerator();
			final Graph graph = dotGenerator.generate(whitePlayer.getRoot(), 20);
			logger.warn("END OF GAME digraph: {}", graph.toDot());
			assertNotEquals(TypeOfEnding.CHESSMATE, e.getTypeOfEnding());
			return;
		}
		logger.warn(game.toString());
		final DotGenerator dotGenerator = new DotGenerator();
		final Graph graph = dotGenerator.generate(whitePlayer.getRoot(), 20);
		logger.warn("digraph: {}", graph.toDot());
		assertTrue(true);
	}

	/**
	 * @throws ChessPositionException 
	 * @throws EndOfGameException 
	 * @formatter:off
	 * <pre>
    [a] [b] [c] [d] [e] [f] [g] [h] 
 8  R-B --- --- --- --- --- --- ---  8  
 7  --- --- --- --- --- --- --- ---  7  
 6  --- --- --- --- --- --- --- ---  6  
 5  --- --- --- --- --- --- --- ---  5  
 4  --- --- --- --- --- --- --- ---  4  
 3  --- --- --- --- --- --- --- ---  3  
 2  --- --- --- --- K-W --- K-B R-B  2  
 1  --- --- --- --- --- --- --- ---  1  
    [a] [b] [c] [d] [e] [f] [g] [h] 
	 * </pre>
	 * @formatter:on
	 */
	@Test
	void testAvoidChessMate() throws ChessPositionException {
		final Board board = new Board();
		final MCTSPlayer whitePlayer = new MCTSPlayer(deepLearningWhite, true, 1, updateCpuct, -1)
				.withNbMaxSearchCalls(100);
		final ChessPlayer blackPlayer = new StaticPlayer("G2-G3;A8-A1");
		final Game game = new Game(board, whitePlayer, blackPlayer);

		whitePlayer.addPieces("KE2");
		blackPlayer.addPieces("RA8,KG2,RH2");

		game.init();
		try {
			// White
			game.play();
			// Black
			game.play();
			// White
			game.play();
			// Black
			game.play();
		} catch (final EndOfGameException e) {
			logFile.warn(game.toString());
			final DotGenerator dotGenerator = new DotGenerator();
			final Graph graph = dotGenerator.generate(whitePlayer.getRoot(), 20);
			logFile.warn("END OF GAME digraph: {}", graph.toDot());
			assertNotEquals(TypeOfEnding.CHESSMATE, e.getTypeOfEnding());
			return;
		}
		logFile.warn(game.toString());
		final DotGenerator dotGenerator = new DotGenerator();
		final Graph graph = dotGenerator.generate(whitePlayer.getRoot(), 20);
		logFile.warn("digraph: {}", graph.toDot());
		assertTrue(true);
	}

	/**
	 * @throws ChessPositionException 
	 * @throws EndOfGameException 
	 * @formatter:off
	 * <pre>
    [a] [b] [c] [d] [e] [f] [g] [h] 
 8  R-B --- --- --- --- --- --- ---  8  
 7  --- --- --- --- --- --- --- ---  7  
 6  --- --- --- --- --- --- --- ---  6  
 5  --- --- --- --- --- --- --- ---  5  
 4  --- --- --- --- --- --- --- ---  4  
 3  --- --- --- --- --- --- --- ---  3  
 2  --- --- --- --- K-W --- K-B R-B  2  
 1  --- --- --- --- --- --- --- ---  1  
    [a] [b] [c] [d] [e] [f] [g] [h] 
	 * </pre>
	 * @formatter:on
	 */
	@Test
	void testMakeChessMate() throws ChessPositionException {
		final Board board = new Board();
		final MCTSPlayer whitePlayer = new MCTSPlayer(deepLearningWhite, true, 1, updateCpuct, 100);
		final MCTSPlayer blackPlayer = new MCTSPlayer(deepLearningBlack, true, 10000, updateCpuct, -1)
				.withNbMaxSearchCalls(500);
		final Game game = new Game(board, whitePlayer, blackPlayer);

		whitePlayer.addPieces("KE1");
		blackPlayer.addPieces("RA3,KG2,RH2,PA2");

		game.init();
		game.setColorToPlay(Color.BLACK);
		boolean good = false;
		try {
			for (int i = 0; i < 20; i++) {
				final Move move = game.play();
				logger.warn("move: {}", move);
			}
		} catch (final EndOfGameException e) {
			final DotGenerator dotGenerator = new DotGenerator();
			final Graph graph = dotGenerator.generate(whitePlayer.getRoot(), 3);
			logger.warn("digraph: {}", graph.toDot());
			assertTrue(e.getTypeOfEnding() == TypeOfEnding.CHESSMATE);
			good = true;
		}
		assertTrue(good);
	}

	/**
	 * 	 * @formatter:off
	 * <pre>
    [a] [b] [c] [d] [e] [f] [g] [h] 
 8  --- --- --- --- --- --- --- ---  8  
 7  --- --- --- --- Q-W --- --- ---  7  
 6  --- p-B --- --- p-B --- --- K-B  6  
 5  p-B --- --- --- --- --- N-W Q-B  5  
 4  --- --- --- p-W p-B --- --- ---  4  
 3  --- --- --- --- --- --- p-W ---  3  
 2  p-W p-W --- --- --- p-W K-W ---  2  
 1  --- --- --- --- --- --- --- ---  1  
    [a] [b] [c] [d] [e] [f] [g] [h] 

	 * </pre>
	 * @formatter:on

	 * @throws ChessPositionException
	 * @throws EndOfGameException
	 */
	@Test
	void testMCTSChessCheck2Move() throws ChessPositionException, EndOfGameException {
		final Board board = new Board();
		final MCTSPlayer whitePlayer = new MCTSPlayer(deepLearningWhite, true, 1, updateCpuct, -1)
				.withNbMaxSearchCalls(1000);
		final ChessPlayer blackPlayer = new RandomPlayer(1);
		final Game game = new Game(board, whitePlayer, blackPlayer);

		whitePlayer.addPieces("QE7,PA2,PB2,PD4,PF2,PG3,NG5,KG2");
		blackPlayer.addPieces("PA5,PB6,PE4,PE6,PG6,QH5,KH6");

		game.init();

		assertThrows(EndOfGameException.class, () -> {
			for (int i = 0; i < 8; i++) {
				// WHITE MOVE
				logger.warn("move white: {}", game.play());
				// BLACK MOVE
				logger.warn("move black: {}", game.play());
				// logger.info(game.toString());
			}
		});
		logger.info(game.toString());
	}

	/**
@formatter:off
    [a] [b] [c] [d] [e] [f] [g] [h] 
 8  --- --- --- --- R-B --- --- ---  8  
 7  --- --- --- --- --- --- --- ---  7  
 6  --- --- --- --- --- --- --- ---  6  
 5  --- --- --- --- --- --- --- ---  5  
 4  --- --- --- --- --- --- --- ---  4  
 3  --- --- --- --- --- --- K-B ---  3  
 2  --- --- --- --- --- --- --- ---  2  
 1  --- --- --- --- --- --- K-W ---  1  
    [a] [b] [c] [d] [e] [f] [g] [h] 

PGN format to use with -> https://lichess.org/paste 
@formatter:on

		 * @throws ChessPositionException
		 * @throws EndOfGameException
		 * @throws IOException
		 */
	@Test
	void testMCTSChessMateBlack1Move() throws ChessPositionException, EndOfGameException, IOException {
		final Board board = new Board();
		final ChessPlayer whitePlayer = new RandomPlayer(100);
		final MCTSPlayer blackPlayer = new MCTSPlayer(deepLearningBlack, true, 1, updateCpuct, 200);
		final Game game = new Game(board, whitePlayer, blackPlayer);

		final King kW = new King(Color.WHITE, Location.get("G1"));
		final King kB = new King(Color.BLACK, Location.get("G3"));
		final Rook rB = new Rook(Color.BLACK, Location.get("E8"));

		whitePlayer.addPieces(kW);
		blackPlayer.addPieces(kB, rB);
		game.init();
		game.setColorToPlay(Color.BLACK);
		logger.info(game.toString());
		logger.info("### Black Possibles Moves:");
		blackPlayer.getPossibleLegalMoves().forEach((move) -> {
			System.out.println(move + " | ");
		});

		logger.info("move black: {}", game.play(false));

		// BLACK MOVE
		final EndOfGameException endOfGameException = assertThrows(EndOfGameException.class, () -> {
			logger.info("move white: {}", game.play(false));
		});
		assertEquals(TypeOfEnding.CHESSMATE, endOfGameException.getTypeOfEnding());
		logger.info(game.toString());
	}

	/**
 * @formatter:off
    [a] [b] [c] [d] [e] [f] [g] [h] 
 8  R-W --- --- --- --- --- K-B ---  8  
 7  --- --- --- --- --- --- --- ---  7  
 6  --- --- --- --- --- --- K-W ---  6  
 5  --- --- --- --- --- --- --- ---  5  
 4  --- --- --- --- --- --- --- ---  4  
 3  --- --- --- --- --- --- --- ---  3  
 2  --- --- --- --- --- --- --- ---  2  
 1  --- --- --- --- --- --- --- ---  1  
    [a] [b] [c] [d] [e] [f] [g] [h] 

PGN format to use with -> https://lichess.org/paste 
------------ P G N -------------
[Event "Test Aquila Chess Player"]
[Site "MOUGINS 06250"]
[Date "2021.08.23"]
[Round "1"]
[White "MCTSPLAYER MCTS -> Move: null visit:2 win:0 parent:false childs:1 SubNodes:21"]
[Black "RandomPlayer:100"]
[Result "*"]
1.Ra8 
 * @formatter:on

	 * @throws ChessPositionException
	 * @throws EndOfGameException
	 * @throws IOException
	 */
	@Test
	void testMCTSChessMateWhite1Move() throws ChessPositionException, EndOfGameException, IOException {
		final Board board = new Board();
		final MCTSPlayer whitePlayer = new MCTSPlayer(deepLearningWhite, true, 1, updateCpuct, 10000);
		final ChessPlayer blackPlayer = new RandomPlayer(100);
		final Game game = new Game(board, whitePlayer, blackPlayer);

		final Rook rW = new Rook(Color.WHITE, Location.get("A7"));
		final King kW = new King(Color.WHITE, Location.get("G6"));
		final King kB = new King(Color.BLACK, Location.get("G8"));

		whitePlayer.addPieces(rW, kW);
		blackPlayer.addPieces(kB); // , pB1, pB2, pB3, bB);
		game.init();
		logger.info(game.toString());
		logger.info("### Black Possibles Moves:");
		final Moves moves = blackPlayer.getPossibleLegalMoves();
		moves.forEach((move) -> logger.info("### {}", move.getAlgebricNotation()));

		// WHITE MOVE
		logger.info("move white: {}", game.play());
		logger.info(game.toString());
		logger.info("### Black Possibles Moves: {}", blackPlayer.getPossibleLegalMoves());

		// BLACK MOVE
		assertThrows(EndOfGameException.class, () -> {
			logger.info("move black: {}", game.play(false));
		});
		logger.info(whitePlayer.toString());
	}

	@ParameterizedTest
	@ValueSource(ints = { 1 })
	void testStopDoubleGame(final int seed) throws IOException, ChessPositionException {
		Board board = new Board();
		MCTSPlayer whitePlayer = new MCTSPlayer(deepLearningWhite, true, 1, updateCpuct, 200);
		ChessPlayer blackPlayer = new RandomPlayerFirstLevel(seed);
		Game game = new Game(board, whitePlayer, blackPlayer);
		game.initWithAllPieces();
		logger.info("BOARD before play:\n" + game);
		try {
			do {
				game.play(false);
			} while (true);
		} catch (final EndOfGameException e) {
			logger.warn("END OF game:\n{}\n{}", e.getLocalizedMessage(), game);
		}
		logger.warn("END OF game:\n{}", game);
		whitePlayer = new MCTSPlayer(deepLearningWhite, true, 1, updateCpuct, 200);
		blackPlayer = new RandomPlayer(seed);
		board = new Board();
		game = new Game(board, whitePlayer, blackPlayer);
		game.initWithAllPieces();
		logger.warn("BOARD before play:\n" + game);
		try {
			do {
				game.play(false);
			} while (true);
		} catch (final EndOfGameException e) {
			logger.warn("END OF game:\n{}\n{}", e.getLocalizedMessage(), game);
		}
		logger.warn("END OF game:\n{}", game);
	}

	@ParameterizedTest
	@ValueSource(ints = { 1, 2, 3, 4, 5, 6 })
	void testStopGame(final int seed) throws ChessPositionException {
		final Board board = new Board();
		final MCTSPlayer whitePlayer = new MCTSPlayer(deepLearningWhite, true, 1, updateCpuct, 100);
		final ChessPlayer blackPlayer = new RandomPlayerFirstLevel(seed + 1000);
		final Game game = new Game(board, whitePlayer, blackPlayer);
		game.initWithAllPieces();
		logger.info("BOARD before play:\n" + game);
		try {
			do {
				game.play(false);
			} while (true);
		} catch (final EndOfGameException e) {
			logger.info("END OF game:\n{}\n{}", e.getLocalizedMessage(), game);
		}
	}

	@Test
	public void testAvoidChessIn1() throws ChessPositionException, EndOfGameException {
		final String pgn = "1.e3 f5 2.Nh3"; // g5 3.Qh5 ";

		final Board board = new Board();
		final MCTSPlayer whitePlayer = new MCTSPlayer(deepLearningWhite, true, 1, updateCpuct, 1000);
		final MCTSPlayer blackPlayer = new MCTSPlayer(deepLearningWhite, true, 1, updateCpuct, 1000);
		final Game game = new Game(board, whitePlayer, blackPlayer);
		game.initWithAllPieces();
		game.playPGN(pgn);
		logger.warn(game.toString());
		assertEquals(Color.BLACK, game.getColorToPlay());
		Move move;
		move = game.play();
		System.out.println("move: " + move);
		move = game.play();
		System.out.println("move: " + move);
	}

}
