/**
 *
 */
package com.aquila.chess.player.mcts;

import com.aquila.chess.Board;
import com.aquila.chess.Game;
import com.aquila.chess.Move;
import com.aquila.chess.Moves;
import com.aquila.chess.exception.ChessPositionException;
import com.aquila.chess.exception.EndOfGameException;
import com.aquila.chess.pieces.Color;
import com.aquila.chess.player.ChessPlayer;
import com.aquila.chess.player.RandomPlayer;
import com.aquila.chess.player.RandomPlayerFirstLevel;
import org.deeplearning4j.nn.graph.ComputationGraph;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * @author bussa
 *
 */
class DeepLearningAGZTest {

    static private Logger logger = LoggerFactory.getLogger(DeepLearningAGZTest.class);

    final INN nn = new NNTest();

    final DeepLearningAGZ deepLearningWhite = new DeepLearningAGZ(nn);

    UpdateCpuct updateCpuct = (nbStep) -> {
        return Math.exp(-0.04 * nbStep) / 2;
    };

    @Test
    void testLoadingDeepLearningAGZ() {
        final ComputationGraph computationGraph = (ComputationGraph) deepLearningWhite.getNetwork();
        logger.info("network: {}", computationGraph);
    }

    @Test
    void testPolicies() throws ChessPositionException, InterruptedException, EndOfGameException {
        final Board board = new Board();
        final ChessPlayer whitePlayer = new RandomPlayer(1);
        final ChessPlayer blackPlayer = new RandomPlayer(1000);
        final Game game = new Game(board, whitePlayer, blackPlayer);
        game.initWithAllPieces();

        assertEquals(97, deepLearningWhite.indexFromMove(new Move(game.getBoard(), Color.BLACK, "a7-a6")));
        assertEquals(161, deepLearningWhite.indexFromMove(new Move(game.getBoard(), Color.BLACK, "b7-b6")));
        assertEquals(225, deepLearningWhite.indexFromMove(new Move(game.getBoard(), Color.BLACK, "c7-c6")));
        assertEquals(289, deepLearningWhite.indexFromMove(new Move(game.getBoard(), Color.BLACK, "d7-d6")));
        assertEquals(353, deepLearningWhite.indexFromMove(new Move(game.getBoard(), Color.BLACK, "e7-e6")));
    }

    /**
     * Test method for
     * {@link com.aquila.chess.player.mcts.DeepLearningAGZ#DeepLearningAGZ(java.lang.String, boolean, boolean)}.
     *
     * @throws ChessPositionException
     * @throws EndOfGameException
     * @throws InterruptedException
     */
    @Test
    void testDeepLearningAGZ() throws ChessPositionException, EndOfGameException, InterruptedException {
        final Board board = new Board();
        final ChessPlayer whitePlayer = new RandomPlayer(1);
        final ChessPlayer blackPlayer = new RandomPlayer(1000);
        final Game game = new Game(board, whitePlayer, blackPlayer);
        game.initWithAllPieces();

        final int LOOP_SIZE = 50;

        long start = System.currentTimeMillis();
        for (int i = 0; i < LOOP_SIZE; i++) {
            Move move = game.play();
            double[][][] inputs = deepLearningWhite.createInputsForOnePosition(game, move);
            assertNotNull(inputs);
        }
        long end = System.currentTimeMillis();
        long delta = end - start;
        logger.warn("time for {} time:{} speed:{} ", LOOP_SIZE, delta, delta / LOOP_SIZE);
    }

    @Test
    public void testCreateInputs() throws ChessPositionException {
        final int seed = 1;
        final Board board = new Board();
        final MCTSPlayer whitePlayer = new MCTSPlayer(deepLearningWhite, true, 1, updateCpuct, 2000);
        final ChessPlayer blackPlayer = new RandomPlayerFirstLevel(seed + 1000);
        final Game game = new Game(board, whitePlayer, blackPlayer);
        game.initWithAllPieces();
        final Moves moves = whitePlayer.getPossibleLegalMoves();
        for (final Move move : moves) {
            final double[][][] inputs = deepLearningWhite.createInputsForOnePosition(game, move);
            logger.info("{}\n{}", move == null ? "Na" : move, deepLearningWhite.ToStringInputs(inputs[0]));
        }
    }
}
