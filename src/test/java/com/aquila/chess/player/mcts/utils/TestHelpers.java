package com.aquila.chess.player.mcts.utils;

import com.aquila.chess.Board;
import com.aquila.chess.Piece;
import com.aquila.chess.pieces.*;

public class TestHelpers {

    public static boolean checkInputsPiece(final double[][][] inputs, final int x, final int y, final Class<? extends Piece> aClass, final Color color) {
        int indexPiece = getIndex(aClass, color);
        return inputs[indexPiece][x][y] == 1.0;
    }

    private static int getPieceIndex(final Class<? extends Piece> aClass) {
        if (aClass == Pawn.class) return Board.PAWN_INDEX;
        if (aClass == Rook.class) return Board.ROOK_INDEX;
        if (aClass == Bishop.class) return Board.BISHOP_INDEX;
        if (aClass == Knight.class) return Board.KING_INDEX;
        if (aClass == Queen.class) return Board.QUEEN_INDEX;
        if (aClass == King.class) return Board.KING_INDEX;
        return Board.PAWN_INDEX;
    }

    private static int getIndex(final Class<? extends Piece> aClass, final Color color) {
        int indexPiece = getPieceIndex(aClass);
        return indexPiece + (6 * color.ordinal());
    }
}
