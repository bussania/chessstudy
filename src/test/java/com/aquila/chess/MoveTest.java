package com.aquila.chess;

import com.aquila.chess.exception.ChessPositionException;
import com.aquila.chess.pieces.Color;
import com.aquila.chess.player.ChessPlayer;
import com.aquila.chess.player.RandomPlayer;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class MoveTest {

    static private Logger log = LoggerFactory.getLogger(MoveTest.class);

    @Test
    void testHashcode() throws ChessPositionException {
        final Board board = new Board();
        final ChessPlayer whitePlayer = new RandomPlayer(1);
        final ChessPlayer blackPlayer = new RandomPlayer(1);
        final Game game = new Game(board, whitePlayer, blackPlayer);
        game.initWithAllPieces();
        Move move1 = new Move(board, Color.WHITE, "E2-E4");
        int hashcode1 = move1.hashCode();
        System.out.println("move.hashcode1:" + hashcode1);
        assertTrue(hashcode1 > 0);
        Move move2 = new Move(board, Color.WHITE, "E2-E3");
        int hashcode2 = move2.hashCode();
        System.out.println("move.hashcode2:" + hashcode2);
        assertNotEquals(hashcode1, hashcode2);
    }

}