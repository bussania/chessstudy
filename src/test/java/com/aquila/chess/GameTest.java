package com.aquila.chess;

import com.aquila.chess.exception.ChessPositionException;
import com.aquila.chess.exception.EndOfGameException;
import com.aquila.chess.exception.EndOfGameException.TypeOfEnding;
import com.aquila.chess.pieces.*;
import com.aquila.chess.player.*;
import com.aquila.chess.player.mcts.BasicMove;
import com.aquila.chess.player.mcts.DeepLearningAGZ;
import com.aquila.chess.player.mcts.MCTSPlayer;
import com.aquila.chess.player.mcts.UpdateCpuct;
import com.aquila.chess.utils.DotGenerator;
import com.aquila.chess.utils.Utils;
import info.leadinglight.jdot.Graph;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

class GameTest {

    static private Logger logger = LoggerFactory.getLogger(GameTest.class);

    @Test
    void testGenerateMoves() throws ChessPositionException {
        final Game game = new Game(//
                new Board(), //
                new RandomPlayer(1), //
                new RandomPlayer(2));
        game.initWithAllPieces();
        final long start = System.currentTimeMillis();
        for (int i = 0; i < 10000; i++) {
            game.getPossibleLegalMoves(Color.WHITE);
        }
        final long end = System.currentTimeMillis();
        logger.info("Time: {}", (end - start));
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 5, 10, 50})
    void testMoveUnmove(final int iter) throws EndOfGameException, ChessPositionException {
        final Game game = new Game(//
                new Board(), //
                new RandomPlayer(1), //
                new RandomPlayer(2));
        game.initWithAllPieces();
        final int hashcode = game.hashCode();
        for (int i = 0; i < iter; i++) {
            game.play(false);
            Utils.check(game);
        }
        for (int i = 0; i < iter; i++) {
            game.undo();
            Utils.check(game);
        }
        assertEquals(game.getPlayer(Color.WHITE).getCapturePieces().size(), 0);
        assertEquals(game.getPlayer(Color.BLACK).getCapturePieces().size(), 0);
        assertEquals(game.hashCode(), hashcode);
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4, 5, 6})
    void testStopGame(final int seed) throws ChessPositionException {
        final Board board = new Board();
        final ChessPlayer whitePlayer = new RandomPlayer(seed);
        final ChessPlayer blackPlayer = new RandomPlayer(seed);
        final Game game = new Game(board, whitePlayer, blackPlayer);
        game.initWithAllPieces();
        try {
            do {
                game.play(false);
                Utils.check(game);
            } while (true);
        } catch (final EndOfGameException e) {
        }
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4, 5, 6})
    void testStopGameFirstLevelvsRandom(final int seed) throws ChessPositionException {
        final Board board = new Board();
        final ChessPlayer whitePlayer = new RandomPlayerFirstLevel(seed);
        final ChessPlayer blackPlayer = new RandomPlayer(seed);
        final Game game = new Game(board, whitePlayer, blackPlayer);
        game.initWithAllPieces();
        try {
            do {
                final Move move = game.play(false);
                Utils.check(game);
            } while (true);
        } catch (final EndOfGameException e) {
        }
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4, 5, 6})
    void testStopGameFirstLevelvsFirstLevel(final int seed) throws ChessPositionException {
        final Board board = new Board();
        final ChessPlayer whitePlayer = new RandomPlayerFirstLevel(seed);
        final ChessPlayer blackPlayer = new RandomPlayerFirstLevel(seed);
        final Game game = new Game(board, whitePlayer, blackPlayer);
        game.initWithAllPieces();
        try {
            do {
                final Move move = game.play(false);
                Utils.check(game);
            } while (true);
        } catch (final EndOfGameException e) {
        }
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 4, 10, 20, 72, 100})
    void testEaterVsGandi(final int iter) throws ChessPositionException {
        final Board board = new Board();
        final ChessPlayer whitePlayer = new EaterPlayer(1);
        final ChessPlayer blackPlayer = new GandiPlayer(2);
        final Game game = new Game(board, whitePlayer, blackPlayer);
        game.initWithAllPieces();
        for (int i = 0; i < iter; i++) {
            try {
                game.play(false);
            } catch (final EndOfGameException e) {
                return;
            }
            Utils.check(game);
        }
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 10, 20})
    void testMoveCorrectness(final int iter) throws EndOfGameException, ChessPositionException {
        final Board board = new Board();
        final ChessPlayer whitePlayer = new RandomPlayer(1);
        final ChessPlayer blackPlayer = new RandomPlayer(2);
        final Game game = new Game(board, whitePlayer, blackPlayer);
        game.initWithAllPieces();
        for (int i = 0; i < iter; i++) {
            game.play(false);
            Utils.check(game);
        }
    }

    /**
     * Example from wikipedia chapter promotion<br/>
     * <a href="https://fr.wikipedia.org/wiki/%C3%89checs">Chapter promotion</a>
     *
     * @throws ChessPositionException
     * @throws EndOfGameException
     */
    @Test
    void testPromotion() throws ChessPositionException, EndOfGameException {
        final Board board = new Board();
        final ChessPlayer whitePlayer = new StaticPlayer("E7-E8;E8-C6");
        final ChessPlayer blackPlayer = new RandomPlayer(2);
        final Game game = new Game(board, whitePlayer, blackPlayer);

        final Pawn pawnW = new Pawn(Color.WHITE, Location.get("E7"));
        final King kingW = new King(Color.WHITE, Location.get("G6"));
        final King kingB = new King(Color.BLACK, Location.get("G8"));

        whitePlayer.addPieces(pawnW, kingW);
        blackPlayer.addPieces(kingB);
        game.init();
        // WHITE MOVE
        Moves moves = whitePlayer.getPossibleLegalMoves();

        game.play(false);

        assertThrows(EndOfGameException.class, () -> {
            game.play(false);
        });
        moves = blackPlayer.getPossibleLegalMoves();
    }

    /**
     * <pre>
     * @formatter:off
     *   [a] [b] [c] [d] [e] [f] [g] [h]
     * 8 --- --- --- --- R-W --- K-B --- 8
     * 7 --- --- --- --- --- --- --- --- 7
     * 6 --- --- --- --- --- --- K-W --- 6
     * 5 --- --- --- --- --- --- --- --- 5
     * 4 --- --- --- --- --- --- --- --- 4
     * 3 --- --- --- --- --- --- --- --- 3
     * 2 --- --- --- --- --- --- --- --- 2
     * 1 --- --- --- --- --- --- --- --- 1
     *   [a] [b] [c] [d] [e] [f] [g] [h]
     *
     * PGN format to use with -> https://lichess.org/paste
     * ------------ P G N -------------
     * [Event "Test Aquila Chess Player"]
     * [Site "MOUGINS 06250"]
     * [Date "2021.08.23"]
     * [Round "1"]
     * [White "StaticPlayer-1"]
     * [Black "RandomPlayer:0"]
     * [Result "1-0"] 1.Re8
     * @formatter:off
     * </pre>
     *
     * @throws ChessPositionException
     * @throws EndOfGameException
     */
    @Test
    void testChessEndingChessMate0() throws ChessPositionException, EndOfGameException {
        final Board board = new Board();
        final ChessPlayer whitePlayer = new StaticPlayer("E7-E8");
        final ChessPlayer blackPlayer = new RandomPlayer(0);
        final Game game = new Game(board, whitePlayer, blackPlayer);

        final Rook rW = new Rook(Color.WHITE, Location.get("E7"));
        final King kW = new King(Color.WHITE, Location.get("G6"));
        final King kB = new King(Color.BLACK, Location.get("G8"));

        whitePlayer.addPieces(rW, kW);
        blackPlayer.addPieces(kB);
        game.init();
        Moves moves = blackPlayer.getPossibleLegalMoves();
        // WHITE MOVE
        game.play(false);
        moves = blackPlayer.getPossibleLegalMoves();
        // BLACK MOVE
        final EndOfGameException endOfGame = assertThrows(EndOfGameException.class, () -> {
            game.play(false);
        });
        assertEquals(endOfGame.getTypeOfEnding(), TypeOfEnding.CHESSMATE);
        moves = blackPlayer.getPossibleLegalMoves();
    }

    @Test
    void testChessEndingPat() throws ChessPositionException, EndOfGameException {
        final Board board = new Board();
        final ChessPlayer whitePlayer = new StaticPlayer("H6-B6");
        final ChessPlayer blackPlayer = new RandomPlayer(0);
        final Game game = new Game(board, whitePlayer, blackPlayer);

        final Queen qW = new Queen(Color.WHITE, Location.get("H6"));
        final King kW = new King(Color.WHITE, Location.get("C5"));
        final King kB = new King(Color.BLACK, Location.get("A8"));

        whitePlayer.addPieces(qW, kW);
        blackPlayer.addPieces(kB);
        game.init();
        // WHITE MOVE
        game.play(false);
        // BLACK MOVE
        final EndOfGameException endOfGame = assertThrows(EndOfGameException.class, () -> {
            game.play(false);
        });
        assertEquals(endOfGame.getTypeOfEnding(), TypeOfEnding.PAT);
    }

    @Test
    void testChessEndingPat1() throws ChessPositionException, EndOfGameException {
        final Board board = new Board();
        final ChessPlayer whitePlayer = new StaticPlayer("A1-H1");
        final ChessPlayer blackPlayer = new RandomPlayer(0);
        final Game game = new Game(board, whitePlayer, blackPlayer);

        final Queen qW1 = new Queen(Color.WHITE, Location.get("A1"));
        final Queen qW2 = new Queen(Color.WHITE, Location.get("B2"));
        final King kW = new King(Color.WHITE, Location.get("G5"));
        final King kB = new King(Color.BLACK, Location.get("G3"));

        whitePlayer.addPieces(qW1, qW2, kW);
        blackPlayer.addPieces(kB);
        game.init();
        // WHITE MOVE
        game.play(false);
        // BLACK MOVE
        final EndOfGameException endOfGame = assertThrows(EndOfGameException.class, () -> {
            game.play(false);
        });
        assertEquals(endOfGame.getTypeOfEnding(), TypeOfEnding.PAT);
    }

    @Test
    void testChessEndingPat2() throws ChessPositionException, EndOfGameException {
        final Board board = new Board();
        final ChessPlayer whitePlayer = new StaticPlayer("H1-H8");
        final ChessPlayer blackPlayer = new RandomPlayer(0);
        final Game game = new Game(board, whitePlayer, blackPlayer);

        final Queen qW = new Queen(Color.WHITE, Location.get("H1"));
        final Rook rW = new Rook(Color.WHITE, Location.get("D7"));
        final Pawn pW = new Pawn(Color.WHITE, Location.get("C5"));
        final King kW = new King(Color.WHITE, Location.get("F4"));
        final Knight kW1 = new Knight(Color.WHITE, Location.get("B2"));
        final Knight kW2 = new Knight(Color.WHITE, Location.get("C2"));

        final King kB = new King(Color.BLACK, Location.get("G6"));

        whitePlayer.addPieces(qW, rW, pW, kW, kW1, kW2);
        blackPlayer.addPieces(kB);
        game.init();
        game.setColorToPlay(Color.WHITE);

        // WHITE
        game.play();

        // BLACK -> PAT
        // BLACK MOVE
        final EndOfGameException endOfGame = assertThrows(EndOfGameException.class, () -> {
            game.play(false);
        });
        assertEquals(endOfGame.getTypeOfEnding(), TypeOfEnding.PAT);
    }

    /**
     * @throws ChessPositionException
     * @throws EndOfGameException     * @formatter:off
     *                                <pre>
     *                                [a] [b] [c] [d] [e] [f] [g] [h]
     *                                8  --- --- --- --- --- --- --- K-W  8
     *                                7  --- --- --- --- --- --- P-W ---  7
     *                                6  --- --- --- --- --- --- P-W ---  6
     *                                5  --- --- --- --- --- --- --- ---  5
     *                                4  --- Q-B --- --- --- --- --- ---  4
     *                                3  --- --- --- --- --- --- --- ---  3
     *                                2  --- --- --- --- --- --- --- ---  2
     *                                1  --- --- --- --- --- --- --- K-B  1
     *                                [a] [b] [c] [d] [e] [f] [g] [h]
     *                                </pre>
     * @formatter:on
     */
    @Test
    void testEndRepetitionX3() throws ChessPositionException, EndOfGameException {
        final Board board = new Board();
        DeepLearningAGZ deepLearning = Mockito.mock(DeepLearningAGZ.class);
        UpdateCpuct updateCpuct = (step) -> 0.5;
        final MCTSPlayer whitePlayer = new MCTSPlayer(deepLearning, false, 0, updateCpuct, 200);
        final ChessPlayer blackPlayer = new StaticPlayer("B6-B1;B1-H7;H7-B1;B1-H7;H7-B1;B1-H7;H7-B1;B1-H7;H7-B1;B1-H7;H7-B1;B1-H7;H7-B1;B1-H7;H7-B1;B1-H7;H7-B1;B1-H7;H7-B1;B1-H7;H7-B1;");
        final Game game = new Game(board, whitePlayer, blackPlayer);
        whitePlayer.addPieces("KH1,PG2,PG3,PF2");
        blackPlayer.addPieces("KH8,QB6");
        game.init();
        game.setColorToPlay(Color.BLACK);

        final EndOfGameException endOfGame = assertThrows(EndOfGameException.class, () -> {
            while (true) {
                game.play(false);
            }
        });
        final DotGenerator dotGenerator = new DotGenerator();
        final Graph graph = dotGenerator.generate(whitePlayer.getRoot(), 5);
        logger.warn("parent: {}", graph.toDot());
        assertEquals(endOfGame.getTypeOfEnding(), TypeOfEnding.REPETITION_X3);
    }


    /**
     * Normal ChessMate
     *
     * @throws ChessPositionException
     * @throws EndOfGameException
     */
    @Test
    void testChessEndingChessMate1() throws ChessPositionException, EndOfGameException {
        final Board board = new Board();
        final ChessPlayer whitePlayer = new StaticPlayer("A7-A8");
        final ChessPlayer blackPlayer = new RandomPlayer(0);
        final Game game = new Game(board, whitePlayer, blackPlayer);

        final Rook rW = new Rook(Color.WHITE, Location.get("A7"));
        final King kW = new King(Color.WHITE, Location.get("G6"));
        final King kB = new King(Color.BLACK, Location.get("G8"));
        final Pawn pB1 = new Pawn(Color.BLACK, Location.get("H6"));
        final Pawn pB2 = new Pawn(Color.BLACK, Location.get("G4"));
        final Pawn pB3 = new Pawn(Color.BLACK, Location.get("F5"));
        final Bishop bB = new Bishop(Color.BLACK, Location.get("B2"));

        whitePlayer.addPieces(rW, kW);
        blackPlayer.addPieces(kB, pB1, pB2, pB3, bB);
        game.init();

        // WHITE MOVE
        game.play(false);

        // BLACK MOVE
        final EndOfGameException endOfGameException = assertThrows(EndOfGameException.class, () -> {
            game.play(false);
        });
        assertEquals(TypeOfEnding.CHESSMATE, endOfGameException.getTypeOfEnding());
    }

    /**
     * <pre>
     * @formatter:off
     * [a] [b] [c] [d] [e] [f] [g] [h]
     * 8  --- --- --- --- R-W --- K-B ---  8
     * 7  --- --- --- --- --- --- --- ---  7
     * 6  --- --- --- --- --- --- K-W ---  6
     * 5  --- --- --- --- --- --- --- ---  5
     * 4  --- --- --- --- --- --- --- ---  4
     * 3  --- --- --- --- --- --- --- ---  3
     * 2  --- --- --- --- --- --- --- ---  2
     * 1  --- --- --- --- --- --- --- ---  1
     * [a] [b] [c] [d] [e] [f] [g] [h]
     *
     * PGN format to use with -> https://lichess.org/paste
     * ------------ P G N -------------
     * [Event "Test Aquila Chess Player"]
     * [Site "MOUGINS 06250"]
     * [Date "2021.08.23"]
     * [Round "1"]
     * [White "StaticPlayer-1"]
     * [Black "RandomPlayer:0"]
     * [Result "1-0"]
     * 1.Re8
     * @formatter:on
     * </pre>
     * ChessMate
     *
     * @throws ChessPositionException
     * @throws EndOfGameException
     */
    @Test
    void testChessEndingChessMateWhite2() throws ChessPositionException, EndOfGameException {
        final Board board = new Board();
        final ChessPlayer whitePlayer = new StaticPlayer("D8-B8;C7-A7");
        final ChessPlayer blackPlayer = new RandomPlayer(0);
        final Game game = new Game(board, whitePlayer, blackPlayer);

        final Rook rW1 = new Rook(Color.WHITE, Location.get("C7"));
        final Rook rW2 = new Rook(Color.WHITE, Location.get("D8"));
        final King kW = new King(Color.WHITE, Location.get("F1"));
        final King kB = new King(Color.BLACK, Location.get("B4"));

        whitePlayer.addPieces(rW1, rW2, kW);
        blackPlayer.addPieces(kB);
        game.init();

        // WHITE MOVE
        game.play(false);

        // BLACK MOVE
        game.play(false);

        // WHITE MOVE
        game.play(false);

        // BLACK MOVE
        Moves moves = blackPlayer.getPossibleLegalMoves();

        // BLACK MOVE
        final EndOfGameException endOfGameException = assertThrows(EndOfGameException.class, () -> {
            game.play(false);
        });

        assertEquals(TypeOfEnding.CHESSMATE, endOfGameException.getTypeOfEnding());
    }

    /**
     * @throws ChessPositionException
     * @throws EndOfGameException
     * @formatter:off <pre>
     * [a] [b] [c] [d] [e] [f] [g] [h]
     * 8  --- --- --- --- R-B --- --- ---  8
     * 7  --- --- --- --- --- --- --- ---  7
     * 6  --- --- --- --- --- --- --- ---  6
     * 5  --- --- --- --- --- --- --- ---  5
     * 4  --- --- --- --- --- --- --- ---  4
     * 3  --- --- --- --- --- --- K-B ---  3
     * 2  --- --- --- --- --- --- --- ---  2
     * 1  --- --- --- --- --- --- K-W ---  1
     * [a] [b] [c] [d] [e] [f] [g] [h]
     *
     * PGN format to use with -> https://lichess.org/paste
     * </pre>
     * @formatter:on ChessMate
     */
    @Test
    void testChessEndingChessMateBlack() throws ChessPositionException, EndOfGameException {
        final Board board = new Board();
        final ChessPlayer whitePlayer = new RandomPlayer(0);
        final ChessPlayer blackPlayer = new StaticPlayer("E8-E1;G3-H3");
        final Game game = new Game(board, whitePlayer, blackPlayer);

        final King kW = new King(Color.WHITE, Location.get("G1"));
        final King kB = new King(Color.BLACK, Location.get("G3"));
        final Rook rB = new Rook(Color.BLACK, Location.get("E8"));

        whitePlayer.addPieces(kW);
        blackPlayer.addPieces(kB, rB);
        game.init();
        game.setColorToPlay(Color.BLACK);

        game.play(false);

        // BLACK MOVE
        final EndOfGameException endOfGameException = assertThrows(EndOfGameException.class, () -> {
            game.play(false);
        });
        assertEquals(TypeOfEnding.CHESSMATE, endOfGameException.getTypeOfEnding());
    }

    /**
     * @throws ChessPositionException
     * @throws EndOfGameException
     * @formatter:off [a] [b] [c] [d] [e] [f] [g] [h]
     * 8  R-B --- --- --- K-B --- --- R-B  8
     * 7  --- --- --- --- --- --- --- ---  7
     * 6  --- --- --- --- --- --- --- ---  6
     * 5  --- --- --- --- --- --- --- ---  5
     * 4  --- --- --- --- --- --- --- ---  4
     * 3  --- --- --- --- --- --- --- ---  3
     * 2  P-W --- --- --- --- P-W --- P-W  2
     * 1  R-W --- --- --- K-W --- --- R-W  1
     * [a] [b] [c] [d] [e] [f] [g] [h]
     * @formatter:on
     */
    @Test
    void testCastlingAll() throws ChessPositionException, EndOfGameException {
        final Board board = new Board();
        final SequentialPlayer whitePlayer = new SequentialPlayer();
        final ChessPlayer blackPlayer = new RandomPlayer(0);
        final Game game = new Game(board, whitePlayer, blackPlayer);
        game.setColorToPlay(Color.WHITE);

        whitePlayer.addPieces("RH1,RA1,KE1,PA2,PG2,PH2");
        blackPlayer.addPieces("RA8,RH8,KE8");
        game.init();

        assertTrue(whitePlayer.isPossibleCastling(Castling.LONG));
        assertTrue(whitePlayer.isPossibleCastling(Castling.SHORT));
        assertTrue(blackPlayer.isPossibleCastling(Castling.LONG));
        assertTrue(blackPlayer.isPossibleCastling(Castling.SHORT));
        Moves blackMoves = blackPlayer.getPossibleLegalMoves();
        assertTrue(whitePlayer.isPossibleCastling(Castling.LONG));
        assertTrue(whitePlayer.isPossibleCastling(Castling.SHORT));
        assertTrue(blackPlayer.isPossibleCastling(Castling.LONG));
        assertTrue(blackPlayer.isPossibleCastling(Castling.SHORT));
        assertEquals(2, blackMoves.stream().filter(move -> move.getCastling() != Castling.NONE).count());

        assertTrue(whitePlayer.isPossibleCastling(Castling.LONG));
        assertTrue(whitePlayer.isPossibleCastling(Castling.SHORT));
        assertTrue(blackPlayer.isPossibleCastling(Castling.LONG));
        assertTrue(blackPlayer.isPossibleCastling(Castling.SHORT));
        Moves whiteMoves = whitePlayer.getPossibleLegalMoves();
        assertTrue(whitePlayer.isPossibleCastling(Castling.LONG));
        assertTrue(whitePlayer.isPossibleCastling(Castling.SHORT));
        assertTrue(blackPlayer.isPossibleCastling(Castling.LONG));
        assertTrue(blackPlayer.isPossibleCastling(Castling.SHORT));
        assertEquals(2, whiteMoves.stream().filter(move -> move.getCastling() != Castling.NONE).count());

        List<Move> castlingMoves = whiteMoves.stream().filter(move -> move.getCastling() == Castling.SHORT)
                .collect(Collectors.toList());
        assertTrue(castlingMoves.size() == 1);
        whitePlayer.setNextMove(castlingMoves.get(0));
        game.play();
        whiteMoves = whitePlayer.getPossibleLegalMoves();
        assertFalse(whitePlayer.isPossibleCastling(Castling.LONG));
        assertFalse(whitePlayer.isPossibleCastling(Castling.SHORT));
        assertTrue(blackPlayer.isPossibleCastling(Castling.LONG));
        assertTrue(blackPlayer.isPossibleCastling(Castling.SHORT));
        assertEquals(0, whiteMoves.stream().filter(move -> move.getCastling() != Castling.NONE).count());
        blackMoves = blackPlayer.getPossibleLegalMoves();
        assertEquals(1, blackMoves.stream().filter(move -> move.getCastling() != Castling.NONE).count());
    }

    /**
     * @throws ChessPositionException
     * @throws EndOfGameException
     * @formatter:off [a] [b] [c] [d] [e] [f] [g] [h]
     * 8  R-B --- --- --- K-B B-B --- R-B  8
     * 7  --- --- --- --- --- --- --- ---  7
     * 6  --- --- --- --- --- --- --- ---  6
     * 5  --- --- --- --- --- --- --- ---  5
     * 4  --- --- --- --- --- --- --- ---  4
     * 3  --- --- --- --- --- --- --- ---  3
     * 2  P-W --- --- --- --- --- --- P-W  2
     * 1  R-W --- B-W --- K-W --- --- R-W  1
     * [a] [b] [c] [d] [e] [f] [g] [h]
     * @formatter:on
     */
    @Test
    void testCastlingWithPiecesAvoidingCertainCastlings() throws ChessPositionException, EndOfGameException {
        final Board board = new Board();
        final SequentialPlayer whitePlayer = new SequentialPlayer();
        final ChessPlayer blackPlayer = new RandomPlayer(0);
        final Game game = new Game(board, whitePlayer, blackPlayer);
        game.setColorToPlay(Color.WHITE);

        whitePlayer.addPieces("RH1,RA1,KE1,PA2,PH2,BC1");
        blackPlayer.addPieces("RA8,RH8,KE8,BF8");
        game.init();

        assertTrue(whitePlayer.isPossibleCastling(Castling.LONG));
        assertTrue(whitePlayer.isPossibleCastling(Castling.SHORT));
        assertTrue(blackPlayer.isPossibleCastling(Castling.LONG));
        assertTrue(blackPlayer.isPossibleCastling(Castling.SHORT));
        Moves moveBlacks = blackPlayer.getPossibleLegalMoves();
        assertTrue(whitePlayer.isPossibleCastling(Castling.LONG));
        assertTrue(whitePlayer.isPossibleCastling(Castling.SHORT));
        assertTrue(blackPlayer.isPossibleCastling(Castling.LONG));
        assertTrue(blackPlayer.isPossibleCastling(Castling.SHORT));
        assertEquals(1, moveBlacks.stream().filter(move -> move.getCastling() == Castling.LONG).count());
        assertEquals(0, moveBlacks.stream().filter(move -> move.getCastling() == Castling.SHORT).count());

        assertTrue(whitePlayer.isPossibleCastling(Castling.LONG));
        assertTrue(whitePlayer.isPossibleCastling(Castling.SHORT));
        assertTrue(blackPlayer.isPossibleCastling(Castling.LONG));
        assertTrue(blackPlayer.isPossibleCastling(Castling.SHORT));
        Moves moveWhites = whitePlayer.getPossibleLegalMoves();
        assertTrue(whitePlayer.isPossibleCastling(Castling.LONG));
        assertTrue(whitePlayer.isPossibleCastling(Castling.SHORT));
        assertTrue(blackPlayer.isPossibleCastling(Castling.LONG));
        assertTrue(blackPlayer.isPossibleCastling(Castling.SHORT));
        assertEquals(0, moveWhites.stream().filter(move -> move.getCastling() == Castling.LONG).count());
        assertEquals(1, moveWhites.stream().filter(move -> move.getCastling() == Castling.SHORT).count());

        List<Move> castlingMoves = moveWhites.stream().filter(move -> move.getCastling() == Castling.SHORT)
                .collect(Collectors.toList());
        assertTrue(castlingMoves.size() == 1);
        whitePlayer.setNextMove(castlingMoves.get(0));
        game.play();
        moveWhites = whitePlayer.getPossibleLegalMoves();
        assertFalse(whitePlayer.isPossibleCastling(Castling.LONG));
        assertFalse(whitePlayer.isPossibleCastling(Castling.SHORT));
        assertTrue(blackPlayer.isPossibleCastling(Castling.LONG));
        assertTrue(blackPlayer.isPossibleCastling(Castling.SHORT));
        assertEquals(0, moveWhites.stream().filter(move -> move.getCastling() == Castling.LONG).count());
        assertEquals(0, moveWhites.stream().filter(move -> move.getCastling() == Castling.SHORT).count());
    }

    /**
     * @throws ChessPositionException
     * @throws EndOfGameException
     * @formatter:off [a] [b] [c] [d] [e] [f] [g] [h]
     * 8  R-B --- --- --- K-B --- --- B-B  8
     * 7  --- --- --- --- --- --- --- ---  7
     * 6  --- --- --- --- --- --- --- ---  6
     * 5  --- --- --- --- --- --- --- ---  5
     * 4  --- --- --- --- --- --- --- ---  4
     * 3  --- --- --- --- --- --- --- ---  3
     * 2  P-W --- --- --- --- --- --- P-W  2
     * 1  R-W --- --- --- K-W --- --- R-W  1
     * [a] [b] [c] [d] [e] [f] [g] [h]
     * @formatter:on
     */
    @Test
    void testCastlingWhenEatingLeftRook() throws ChessPositionException, EndOfGameException {
        final Board board = new Board();
        final SequentialPlayer whitePlayer = new SequentialPlayer();
        final SequentialPlayer blackPlayer = new SequentialPlayer();
        final Game game = new Game(board, whitePlayer, blackPlayer);
        game.setColorToPlay(Color.WHITE);

        whitePlayer.addPieces("RA1,KE1,RH1,PA2,PH2");
        blackPlayer.addPieces("RA8,KE8,BH8");
        game.init();

        assertTrue(whitePlayer.isPossibleCastling(Castling.LONG));
        assertTrue(whitePlayer.isPossibleCastling(Castling.SHORT));
        assertTrue(blackPlayer.isPossibleCastling(Castling.LONG));
        assertFalse(blackPlayer.isPossibleCastling(Castling.SHORT));
        Moves moveBlacks = blackPlayer.getPossibleLegalMoves();
        assertTrue(whitePlayer.isPossibleCastling(Castling.LONG));
        assertTrue(whitePlayer.isPossibleCastling(Castling.SHORT));
        assertTrue(blackPlayer.isPossibleCastling(Castling.LONG));
        assertFalse(blackPlayer.isPossibleCastling(Castling.SHORT));
        assertEquals(1, moveBlacks.stream().filter(move -> move.getCastling() == Castling.LONG).count());
        assertEquals(0, moveBlacks.stream().filter(move -> move.getCastling() == Castling.SHORT).count());

        assertTrue(whitePlayer.isPossibleCastling(Castling.LONG));
        assertTrue(whitePlayer.isPossibleCastling(Castling.SHORT));
        assertTrue(blackPlayer.isPossibleCastling(Castling.LONG));
        assertFalse(blackPlayer.isPossibleCastling(Castling.SHORT));
        Moves moveWhites = whitePlayer.getPossibleLegalMoves();
        assertTrue(whitePlayer.isPossibleCastling(Castling.LONG));
        assertTrue(whitePlayer.isPossibleCastling(Castling.SHORT));
        assertTrue(blackPlayer.isPossibleCastling(Castling.LONG));
        assertFalse(blackPlayer.isPossibleCastling(Castling.SHORT));
        assertEquals(0, moveWhites.stream().filter(move -> move.getCastling() == Castling.LONG).count());
        assertEquals(1, moveWhites.stream().filter(move -> move.getCastling() == Castling.SHORT).count());

        List<Move> castlingMoves = moveWhites.stream().filter(move -> move.getCastling() == Castling.SHORT)
                .collect(Collectors.toList());
        assertTrue(castlingMoves.size() == 1);
        game.setColorToPlay(Color.BLACK);
        blackPlayer.setNextMove(new Move(new BasicMove(7, 7, 0, 0, game.getPiece(Location.get(7, 7))), game.getBoard()));
        game.play();
        moveWhites = whitePlayer.getPossibleLegalMoves();
        assertFalse(whitePlayer.isPossibleCastling(Castling.LONG));
        assertTrue(whitePlayer.isPossibleCastling(Castling.SHORT));
        assertTrue(blackPlayer.isPossibleCastling(Castling.LONG));
        assertFalse(blackPlayer.isPossibleCastling(Castling.SHORT));
        assertEquals(0, moveWhites.stream().filter(move -> move.getCastling() == Castling.LONG).count());
        assertEquals(1, moveWhites.stream().filter(move -> move.getCastling() == Castling.SHORT).count());
    }

    /**
     * @throws ChessPositionException
     * @throws EndOfGameException
     * @formatter:off [a] [b] [c] [d] [e] [f] [g] [h]
     * 8  B-B --- --- --- K-B --- --- R-B  8
     * 7  --- --- --- --- --- --- --- ---  7
     * 6  --- --- --- --- --- --- --- ---  6
     * 5  --- --- --- --- --- --- --- ---  5
     * 4  --- --- --- --- --- --- --- ---  4
     * 3  --- --- --- --- --- --- --- ---  3
     * 2  P-W --- --- --- --- --- --- P-W  2
     * 1  R-W --- --- --- K-W --- --- R-W  1
     * [a] [b] [c] [d] [e] [f] [g] [h]
     * @formatter:on
     */
    @Test
    void testCastlingWhenEatingRightRook() throws ChessPositionException, EndOfGameException {
        final Board board = new Board();
        final SequentialPlayer whitePlayer = new SequentialPlayer();
        final SequentialPlayer blackPlayer = new SequentialPlayer();
        final Game game = new Game(board, whitePlayer, blackPlayer);
        game.setColorToPlay(Color.WHITE);

        whitePlayer.addPieces("RA1,KE1,RH1,PA2,PH2");
        blackPlayer.addPieces("BA8,KE8,RH8");
        game.init();

        assertTrue(whitePlayer.isPossibleCastling(Castling.LONG));
        assertTrue(whitePlayer.isPossibleCastling(Castling.SHORT));
        assertFalse(blackPlayer.isPossibleCastling(Castling.LONG));
        assertTrue(blackPlayer.isPossibleCastling(Castling.SHORT));
        Moves moveBlacks = blackPlayer.getPossibleLegalMoves();
        assertTrue(whitePlayer.isPossibleCastling(Castling.LONG));
        assertTrue(whitePlayer.isPossibleCastling(Castling.SHORT));
        assertFalse(blackPlayer.isPossibleCastling(Castling.LONG));
        assertTrue(blackPlayer.isPossibleCastling(Castling.SHORT));
        assertEquals(0, moveBlacks.stream().filter(move -> move.getCastling() == Castling.LONG).count());
        assertEquals(1, moveBlacks.stream().filter(move -> move.getCastling() == Castling.SHORT).count());

        assertTrue(whitePlayer.isPossibleCastling(Castling.LONG));
        assertTrue(whitePlayer.isPossibleCastling(Castling.SHORT));
        assertFalse(blackPlayer.isPossibleCastling(Castling.LONG));
        assertTrue(blackPlayer.isPossibleCastling(Castling.SHORT));
        Moves moveWhites = whitePlayer.getPossibleLegalMoves();
        assertTrue(whitePlayer.isPossibleCastling(Castling.LONG));
        assertTrue(whitePlayer.isPossibleCastling(Castling.SHORT));
        assertFalse(blackPlayer.isPossibleCastling(Castling.LONG));
        assertTrue(blackPlayer.isPossibleCastling(Castling.SHORT));
        assertEquals(1, moveWhites.stream().filter(move -> move.getCastling() == Castling.LONG).count());
        assertEquals(0, moveWhites.stream().filter(move -> move.getCastling() == Castling.SHORT).count());

        game.setColorToPlay(Color.BLACK);
        blackPlayer.setNextMove(new Move(new BasicMove(0, 7, 7, 0, game.getPiece(Location.get(0, 7))), game.getBoard()));
        game.play();
        moveWhites = whitePlayer.getPossibleLegalMoves();
        assertTrue(whitePlayer.isPossibleCastling(Castling.LONG));
        assertFalse(whitePlayer.isPossibleCastling(Castling.SHORT));
        assertFalse(blackPlayer.isPossibleCastling(Castling.LONG));
        assertTrue(blackPlayer.isPossibleCastling(Castling.SHORT));
        assertEquals(1, moveWhites.stream().filter(move -> move.getCastling() == Castling.LONG).count());
        assertEquals(0, moveWhites.stream().filter(move -> move.getCastling() == Castling.SHORT).count());
    }

    @Test
    void testTrueCastling() throws ChessPositionException, EndOfGameException {
        final Board board = new Board();
        final SequentialPlayer whitePlayer = new SequentialPlayer();
        final SequentialPlayer blackPlayer = new SequentialPlayer();
        final Game game = new Game(board, whitePlayer, blackPlayer);
        game.initWithAllPieces();
        whitePlayer.setNextMove("G1-F3");
        game.play();
        blackPlayer.setNextMove("B8-C6");
        game.play();
        System.out.println(game);
        whitePlayer.setNextMove("G2-G3");
        game.play();
        blackPlayer.setNextMove("D7-D5");
        game.play();
        System.out.println(game);
        whitePlayer.setNextMove("F1-H3");
        game.play();
        blackPlayer.setNextMove("C8-E6");
        game.play();
        System.out.println(game);
        Moves moveWhites = whitePlayer.getPossibleLegalMoves();
        assertEquals(1, moveWhites.stream().filter(move -> move.getCastling() == Castling.SHORT).count());
        assertEquals(0, moveWhites.stream().filter(move -> move.getCastling() == Castling.LONG).count());
        Moves moveBlacks = blackPlayer.getPossibleLegalMoves();
        assertEquals(0, moveBlacks.stream().filter(move -> move.getCastling() == Castling.SHORT).count());
        assertEquals(0, moveBlacks.stream().filter(move -> move.getCastling() == Castling.LONG).count());
        whitePlayer.setNextMove("B1-C3");
        game.play();
        blackPlayer.setNextMove("D8-D7");
        game.play();
        System.out.println(game);
        moveWhites = whitePlayer.getPossibleLegalMoves();
        assertEquals(1, moveWhites.stream().filter(move -> move.getCastling() == Castling.SHORT).count());
        assertEquals(0, moveWhites.stream().filter(move -> move.getCastling() == Castling.LONG).count());
        moveBlacks = blackPlayer.getPossibleLegalMoves();
        assertEquals(0, moveBlacks.stream().filter(move -> move.getCastling() == Castling.SHORT).count());
        assertEquals(1, moveBlacks.stream().filter(move -> move.getCastling() == Castling.LONG).count());
    }

}
