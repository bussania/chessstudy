package com.aquila.chess.pgn;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aquila.chess.Board;
import com.aquila.chess.Game;
import com.aquila.chess.exception.ChessPositionException;
import com.aquila.chess.player.ChessPlayer;
import com.aquila.chess.player.RandomPlayerFirstLevel;

class PGNParserTest {
	static private Logger logger = LoggerFactory.getLogger(PGNParserTest.class);
	
	@Test
	void testParsePGN() throws ChessPositionException {
		final String pgn="1.e3 f5 2.Nh3 g5 3.Qh5";
		
		final Board board = new Board();
		final ChessPlayer whitePlayer = new RandomPlayerFirstLevel(1000);
		final ChessPlayer blackPlayer = new RandomPlayerFirstLevel(1000);
		final Game game = new Game(board, whitePlayer, blackPlayer);
		game.initWithAllPieces();
		game.playPGN(pgn);
		logger.warn(game.toString());
		assertNotNull(game.getPiece("e3"));
		assertNotNull(game.getPiece("f5"));
		assertNotNull(game.getPiece("h3"));
		assertNotNull(game.getPiece("g5"));
		assertNotNull(game.getPiece("h5"));
	}

}
