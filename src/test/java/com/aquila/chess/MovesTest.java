package com.aquila.chess;

import static org.junit.jupiter.api.Assertions.assertFalse;

import java.util.Random;

import org.junit.jupiter.api.Test;

import com.aquila.chess.exception.ChessPositionException;
import com.aquila.chess.pieces.Color;
import com.aquila.chess.pieces.King;
import com.aquila.chess.pieces.Pawn;
import com.aquila.chess.pieces.Rook;
import com.aquila.chess.player.ChessPlayer;
import com.aquila.chess.player.RandomPlayer;

class MovesTest {

	/**
	 *
@throws ChessPositionException 
	 * @formatter:off
<pre>
    [a] [b] [c] [d] [e] [f] [g] [h] 
 8  --- --- --- --- --- --- --- ---  8  
 7  --- --- --- --- --- --- --- ---  7  
 6  --- --- --- --- --- --- K-B ---  6  
 5  --- P-W --- --- --- --- --- ---  5  
 4  --- --- --- --- --- --- --- ---  4  
 3  --- --- --- --- --- --- --- ---  3  
 2  --- --- --- --- --- --- --- ---  2  
 1  --- --- --- --- --- --- K-W ---  1  
    [a] [b] [c] [d] [e] [f] [g] [h] 

PGN format to use with -> https://lichess.org/paste
</pre> 
@formatter:on
	 */
	@Test
	void testGetPossibleMoveWithout() throws ChessPositionException {
		final Board board = new Board();
		final ChessPlayer whitePlayer = new RandomPlayer(0);
		final ChessPlayer blackPlayer = new RandomPlayer(0);
		final Game game = new Game(board, whitePlayer, blackPlayer);

		final King kW = new King(Color.WHITE, Location.get("G1"));
		final King kB = new King(Color.BLACK, Location.get("G6"));
		final Pawn pW = new Pawn(Color.WHITE, Location.get("B5"));

		whitePlayer.addPieces(kW, pW);
		blackPlayer.addPieces(kB);
		game.init();
		
		Moves moves = whitePlayer.getPossibleLegalMoves();
		for(int i=0; i<20; i++) {
			Move move = moves.getPossibleMoveWithout(new Random(), King.class, Rook.class);
			Piece piece = move.getPiece();
			assertFalse(piece instanceof King); 
			assertFalse(piece instanceof Rook);
		}
	}

}
