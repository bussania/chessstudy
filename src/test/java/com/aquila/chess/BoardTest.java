package com.aquila.chess;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;

import com.aquila.chess.exception.ChessPositionException;
import com.aquila.chess.exception.EndOfGameException;
import com.aquila.chess.pieces.Bishop;
import com.aquila.chess.pieces.Color;
import com.aquila.chess.pieces.King;
import com.aquila.chess.pieces.Pawn;
import com.aquila.chess.pieces.Rook;
import com.aquila.chess.player.ChessPlayer;
import com.aquila.chess.player.RandomPlayer;
import com.aquila.chess.player.SequentialPlayer;

class BoardTest {

	@Test
	void testHashcode() throws EndOfGameException, ChessPositionException {
		Board board = new Board();
		ChessPlayer whitePlayer = new RandomPlayer(0);
		ChessPlayer blackPlayer = new RandomPlayer(0);
		Game game = new Game(board, whitePlayer, blackPlayer);
		game.initWithAllPieces();
		System.out.println("game.hashcode: " + game.hashCode());
		game.play();
		System.out.println("game.hashcode: " + game.hashCode());
	}

	@Test
	void testHashcodeWithMove() throws EndOfGameException, ChessPositionException {
		Board board = new Board();
		SequentialPlayer whitePlayer = new SequentialPlayer();
		SequentialPlayer blackPlayer = new SequentialPlayer();
		Game game = new Game(board, whitePlayer, blackPlayer);
		game.initWithAllPieces();
		int originalHashcode = game.hashCode();
		System.out.println("originalHashcode: " + originalHashcode);
		int newHashcode = game.hashCode(null);
		System.out.println("newHashcode: " + newHashcode);
		assertEquals(originalHashcode, newHashcode);
		Move move = new Move(game.getBoard(), Color.WHITE, "e2-e4");
		originalHashcode = game.hashCode(move);
		System.out.println("originalHashcode: " + originalHashcode);
		whitePlayer.setNextMove(move);
		game.play();
		newHashcode = game.hashCode();
		System.out.println("newHashcode: " + newHashcode);
		assertEquals(originalHashcode, newHashcode);
	}

	@Test
	void testCheckAndAddStraitLine() throws ChessPositionException {
		Board board = new Board();
		Moves moves = new Moves();
		ChessPlayer whitePlayer = new RandomPlayer(0);
		ChessPlayer blackPlayer = new RandomPlayer(0);
		new Game(board, whitePlayer, blackPlayer);
		Rook rook = new Rook(Color.WHITE, Location.get("B3"));
		whitePlayer.addPiece(rook);
		// Full West
		board.checkAndAddLine(moves, rook, 10, rook.getY(), true);
		assertEquals(6, moves.size());
		// Full North
		moves.clear();
		board.checkAndAddLine(moves, rook, rook.getX(), 10, true);
		assertEquals(5, moves.size());
		// full East
		moves.clear();
		board.checkAndAddLine(moves, rook, -10, rook.getY(), true);
		assertEquals(1, moves.size());
		// full Down
		moves.clear();
		board.checkAndAddLine(moves, rook, rook.getX(), -10, true);
		assertEquals(2, moves.size());
		// 2 checboxes Up
		moves.clear();
		board.checkAndAddLine(moves, rook, rook.getX(), rook.getY() + 2, true);
		assertEquals(2, moves.size());
		// 21 checboxes Down
		moves.clear();
		board.checkAndAddLine(moves, rook, rook.getX(), rook.getY() - 1, true);
		assertEquals(1, moves.size());
	}

	@Test
	void testCheckAndAddStraitLineRook() throws ChessPositionException {
		Board board = new Board();
		Moves moves = new Moves();
		ChessPlayer whitePlayer = new RandomPlayer(0);
		ChessPlayer blackPlayer = new RandomPlayer(0);
		new Game(board, whitePlayer, blackPlayer);
		Rook rookW = new Rook(Color.WHITE, Location.get("E4"));
		Pawn p1W = new Pawn(Color.WHITE, Location.get("E5"));
		Pawn p2W = new Pawn(Color.WHITE, Location.get("D4"));
		Pawn p3W = new Pawn(Color.WHITE, Location.get("E2"));
		Bishop bishopW = new Bishop(Color.WHITE, Location.get("D3"));
		King kingB = new King(Color.BLACK, Location.get("G4"));
		whitePlayer.addPieces(rookW, p1W, p2W, p3W, bishopW);
		blackPlayer.addPieces(kingB);
		// Full West
		board.checkAndAddLine(moves, rookW, 10, rookW.getY(), true);
		assertEquals(2, moves.size());
		assertNotNull(moves.get(1).getCapturePiece());
		assertEquals(kingB, moves.get(1).getCapturePiece());
		// Full North
		moves.clear();
		board.checkAndAddLine(moves, rookW, rookW.getX(), 10, true);
		assertEquals(0, moves.size());
		// full East
		moves.clear();
		board.checkAndAddLine(moves, rookW, -10, rookW.getY(), true);
		assertEquals(0, moves.size());
		// full Down
		moves.clear();
		board.checkAndAddLine(moves, rookW, rookW.getX(), -10, true);
		assertEquals(1, moves.size());
	}

	@Test
	void testCheckAndAddLineDiag() throws ChessPositionException {
		Board board = new Board();
		Moves moves = new Moves();
		ChessPlayer whitePlayer = new RandomPlayer(0);
		ChessPlayer blackPlayer = new RandomPlayer(0);
		new Game(board, whitePlayer, blackPlayer);
		Bishop bishop = new Bishop(Color.WHITE, Location.get("C2"));
		whitePlayer.addPiece(bishop);
		board.checkAndAddDiag(moves, bishop, 10, 10, true);
		assertEquals(5, moves.size());
		moves.clear();
		board.checkAndAddDiag(moves, bishop, -10, -10, true);
		assertEquals(1, moves.size());
		moves.clear();
		board.checkAndAddDiag(moves, bishop, -10, 10, true);
		assertEquals(2, moves.size());
		moves.clear();
		board.checkAndAddDiag(moves, bishop, 10, -10, true);
		assertEquals(1, moves.size());
	}

}
