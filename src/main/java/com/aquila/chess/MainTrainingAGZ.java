package com.aquila.chess;

import com.aquila.chess.exception.EndOfGameException;
import com.aquila.chess.manager.GameManager;
import com.aquila.chess.manager.Record.Status;
import com.aquila.chess.manager.Sequence;
import com.aquila.chess.player.mcts.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MainTrainingAGZ {

    static private final String NN_REFERENCE = "../AGZ_NN/AGZ.reference";
    // static public int NB_STEP = 50;
    static private final String NN_OPPONENT = "../AGZ_NN/AGZ.partner";
    static public int NB_STEP = 800; // 500;
    @SuppressWarnings("unused")
    static private Logger logger = LoggerFactory.getLogger(MainTrainingAGZ.class);
    static private Logger logFile = LoggerFactory.getLogger("FILE");

    public static void main(final String[] args) throws Exception {
        int numSaveGame = 539;
        logFile.info("START MainTrainingAGZ");
        Dirichlet dirichlet = (game) -> true;
        // game.getNbStep() < 10;
        UpdateCpuct updateCpuct = (nbStep) -> 2.0; // * Math.exp(-0.01 * nbStep);
        // return nbStep < 30 ? 4.0 : 0.01; // 0.1;
        // return 4.0;
        UpdateLr updateLr = nbGames -> 1e-5;
        GameManager gameManager = new GameManager("../AGZ_NN/sequences.csv", 40, 55);
        INN nnWhite = new NN(NN_REFERENCE, true, gameManager.getNbGames());
        final DeepLearningAGZ deepLearningWhite = new DeepLearningAGZ(nnWhite);
        deepLearningWhite.setDirichlet(dirichlet);
        deepLearningWhite.setUpdateLr(updateLr, 0);
        INN nnBlack = new NN(NN_OPPONENT, true, gameManager.getNbGames());
        DeepLearningAGZ deepLearningBlack = new DeepLearningAGZ(nnBlack);
        deepLearningBlack.setDirichlet(dirichlet);
        deepLearningBlack.setUpdateLr(updateLr, 0);
        while (true) {
            Sequence sequence = gameManager.createSequence();
            long seed = System.nanoTime();
            final MCTSPlayer whitePlayer = new MCTSPlayer(deepLearningWhite, false, seed, updateCpuct, -1)
                    .withNbMaxSearchCalls(NB_STEP);
            final MCTSPlayer blackPlayer = new MCTSPlayer(deepLearningBlack, true, seed + 1000, updateCpuct, -1)
                    .withNbMaxSearchCalls(NB_STEP);

            final Board board = new Board();
            final Game game = new Game(board, whitePlayer, blackPlayer);
            game.logBoard(true);

            game.initWithAllPieces();
            try {
                do {
                    deepLearningWhite.storeInputs(game);
                    game.play(false);
                    sequence.play();
                    deepLearningWhite.commitInputs(game, whitePlayer.getRoot());
                } while (true);
            } catch (final EndOfGameException e) {
                logFile.info("#########################################################################");
                logFile.info("END OF game [{}] :\n{}\n{}", gameManager.getNbGames(), e.getLocalizedMessage(), game);
                ResultGame resultGame = whitePlayer.getResultGame(e);
                deepLearningWhite.saveBatch(resultGame, numSaveGame++);
                deepLearningWhite.clearCache();
                deepLearningBlack.clearCache();
                logFile.info("NN score: {}", deepLearningWhite.toString());
                Status status = gameManager.endGame(resultGame, game, deepLearningWhite.getScore(), e, sequence);
                if (status == Status.SWITCHING) {
                    final Path reference = Paths.get(NN_REFERENCE);
                    final Path opponent = Paths.get(NN_OPPONENT);
                    SimpleDateFormat formater = new SimpleDateFormat("dd-MM-yyyy_hh-mm-ss");
                    final Path backupOpponent = Paths.get(NN_OPPONENT + "_" + formater.format(new Date()));
                    logFile.info("BACKUP PARTNER {} -> {}", opponent.toString(), backupOpponent.toString());
                    if (opponent.toFile().canRead()) {
                        Files.copy(opponent, backupOpponent, StandardCopyOption.REPLACE_EXISTING);
                    }
                    Files.copy(reference, opponent, StandardCopyOption.REPLACE_EXISTING);
                    logFile.info("Switching DP {} <-> {}", reference, opponent);
                    nnBlack = new NN(NN_OPPONENT, true, gameManager.getNbGames());
                    deepLearningBlack = new DeepLearningAGZ(nnBlack);
                }
            }
        }

    }

}
