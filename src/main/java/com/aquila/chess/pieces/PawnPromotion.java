package com.aquila.chess.pieces;

import com.aquila.chess.Board;
import com.aquila.chess.Move;
import com.aquila.chess.Piece;

public interface PawnPromotion {
	default public Class<? extends Piece> doPromote(Board board, Move move) {
		return Queen.class;
	}
}
