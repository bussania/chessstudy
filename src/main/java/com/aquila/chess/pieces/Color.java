package com.aquila.chess.pieces;

public enum Color {
	WHITE, BLACK, NOT_DEFINED;

	public Color complement() {
		return this == WHITE ? BLACK : WHITE;
	}
}