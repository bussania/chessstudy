/**
 * 
 */
package com.aquila.chess.pieces;

import com.aquila.chess.Board;
import com.aquila.chess.Location;
import com.aquila.chess.Moves;
import com.aquila.chess.Piece;
import com.aquila.chess.exception.ChessPositionException;


/**
 * A King Model
 *
 */
public class King extends Piece {

	boolean check = false;

	public King(final Color color, final Location location) {
		super(color, location);
	}

	@Override
	public final String getLabel() {
		return "K";
	}

	@Override
	public void getPossibleRawMove(Moves moves, Board board) throws ChessPositionException {
		board.checkAndAdd(moves, this ,this.getX() - 1, this.getY() - 1, true);
		board.checkAndAdd(moves, this ,this.getX() + 0, this.getY() - 1, true);
		board.checkAndAdd(moves, this ,this.getX() + 1, this.getY() - 1, true);
		board.checkAndAdd(moves, this ,this.getX() - 1, this.getY() + 0, true);
		board.checkAndAdd(moves, this ,this.getX() + 1, this.getY() + 0, true);
		board.checkAndAdd(moves, this ,this.getX() - 1, this.getY() + 1, true);
		board.checkAndAdd(moves, this ,this.getX() + 0, this.getY() + 1, true);
		board.checkAndAdd(moves, this ,this.getX() + 1, this.getY() + 1, true);
	}

	public boolean isCheck() {
		return check;
        }

	public void setCheck(boolean check) {
		this.check = check;
	}

	@Override
	public int getPower() {
		return 20;
	}

	@Override
	public String toPGN() {
		return "K";
	}

}
