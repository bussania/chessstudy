/**
 * 
 */
package com.aquila.chess.pieces;

import com.aquila.chess.Board;
import com.aquila.chess.Location;
import com.aquila.chess.Moves;
import com.aquila.chess.Piece;
import com.aquila.chess.exception.ChessPositionException;

/**
 * A Queen Model
 *
 */
public class Queen extends Piece {

	public Queen(final Color color, final Location location) {
		super(color, location);
	}

	@Override
	public final String getLabel() {
		return "Q";
	}

	@Override
	public void getPossibleRawMove(Moves moves, Board board) throws ChessPositionException {
		// like a Rook
		board.checkAndAddLine(moves, this, this.getX(), 0, true);
		board.checkAndAddLine(moves, this, this.getX(), 7, true);
		board.checkAndAddLine(moves, this, 0, this.getY(), true);
		board.checkAndAddLine(moves, this, 7, this.getY(), true);
		// like a bishop
		board.checkAndAddDiag(moves, this, this.getX() - 7, this.getY() - 7, true);
		board.checkAndAddDiag(moves, this, this.getX() + 7, this.getY() - 7, true);
		board.checkAndAddDiag(moves, this, this.getX() - 7, this.getY() + 7, true);
		board.checkAndAddDiag(moves, this, this.getX() + 7, this.getY() + 7, true);
	}

	@Override
	public int getPower() {
		return 5;
	}

	@Override
	public String toPGN() {
		return "Q";
	}

}
