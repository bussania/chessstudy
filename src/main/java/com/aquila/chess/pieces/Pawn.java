/**
 * 
 */
package com.aquila.chess.pieces;

import com.aquila.chess.Board;
import com.aquila.chess.Location;
import com.aquila.chess.Move;
import com.aquila.chess.Moves;
import com.aquila.chess.Piece;
import com.aquila.chess.exception.ChessPositionException;

/**
 * A Pawn Model
 *
 */
public class Pawn extends Piece {

	public Pawn(final Color color, final Location location) {
		super(color, location);
	}

	@Override
	public final String getLabel() {
		return "p";
	}

	@SuppressWarnings("incomplete-switch")
	@Override
	public void getPossibleRawMove(Moves moves, Board board) throws ChessPositionException {
		Piece piece;
		switch (this.getColor()) {
		case WHITE:
			if (this.getY() == 1) {
				board.checkAndAddLine(moves, this, this.getX(), this.getY() + 2, false);
			} else {
				board.checkAndAddLine(moves, this, this.getX(), this.getY() + 1, false);
			}
			// diagonals capture
			piece = board.getPiece(this.getX() - 1, getY() + 1);
			if (piece!=null && piece.getColor() == Color.BLACK)
				moves.add(new Move(board, this, piece.getLocation()));
			piece = board.getPiece(this.getX() + 1, getY() + 1);
			if (piece!=null && piece.getColor() == Color.BLACK)
				moves.add(new Move(board, this, piece.getLocation()));
			break;
		case BLACK:
			if (this.getY() == 6) {
				board.checkAndAddLine(moves, this, this.getX(), this.getY() - 2, false);
			} else {
				board.checkAndAddLine(moves, this, this.getX(), this.getY() - 1, false);
			}
			// diagonals capture
			piece = board.getPiece(this.getX() - 1, getY() - 1);
			if (piece!=null && piece.getColor() == Color.WHITE)
				moves.add(new Move(board, this, piece.getLocation()));
			piece = board.getPiece(this.getX() + 1, getY() - 1);
			if (piece!=null && piece.getColor() == Color.WHITE)
				moves.add(new Move(board, this, piece.getLocation()));
			break;
		}
	}

	@Override
	public int getPower() {
		return 1;
	}

	@Override
	public String toPGN() {
		return "";
	}

}
