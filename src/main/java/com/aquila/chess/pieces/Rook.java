/**
 * 
 */
package com.aquila.chess.pieces;

import com.aquila.chess.Board;
import com.aquila.chess.Location;
import com.aquila.chess.Moves;
import com.aquila.chess.Piece;
import com.aquila.chess.exception.ChessPositionException;

/**
 * a Rook Model
 *
 */
public class Rook extends Piece {

	public Rook(final Color color, final Location location) {
		super(color, location);
	}

	@Override
	public final String getLabel() {
		return "R";
	}

	@Override
	public void getPossibleRawMove(Moves moves, Board board) throws ChessPositionException {
		board.checkAndAddLine(moves, this, this.getX(), 0, true);
		board.checkAndAddLine(moves, this, this.getX(), 7, true);
		board.checkAndAddLine(moves, this, 0, this.getY(), true);
		board.checkAndAddLine(moves, this, 7, this.getY(), true);
	}

	@Override
	public int getPower() {
		return 4;
	}

	@Override
	public String toPGN() {
		return "R";
	}

}
