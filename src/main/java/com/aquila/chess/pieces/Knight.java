/**
 * 
 */
package com.aquila.chess.pieces;

import com.aquila.chess.Board;
import com.aquila.chess.Location;
import com.aquila.chess.Moves;
import com.aquila.chess.Piece;
import com.aquila.chess.exception.ChessPositionException;

/**
 * A Knight Model
 *
 */
public class Knight extends Piece {

	public Knight(final Color color, final Location location) {
		super(color, location);
	}

	@Override
	public final String getLabel() {
		return "N";
	}

	@Override
	public void getPossibleRawMove(Moves moves, Board board) throws ChessPositionException {
		board.checkAndAdd(moves, this, this.getX() - 2, this.getY() - 1, true);
		board.checkAndAdd(moves, this, this.getX() - 2, this.getY() + 1, true);
		board.checkAndAdd(moves, this, this.getX() + 2, this.getY() - 1, true);
		board.checkAndAdd(moves, this, this.getX() + 2, this.getY() + 1, true);
		board.checkAndAdd(moves, this, this.getX() - 1, this.getY() - 2, true);
		board.checkAndAdd(moves, this, this.getX() - 1, this.getY() + 2, true);
		board.checkAndAdd(moves, this, this.getX() + 1, this.getY() - 2, true);
		board.checkAndAdd(moves, this, this.getX() + 1, this.getY() + 2, true);
	}

	@Override
	public int getPower() {
		return 3;
	}

	@Override
	public String toPGN() {
		return "N";
	}

}
