/**
 * 
 */
package com.aquila.chess.pieces;

import com.aquila.chess.Board;
import com.aquila.chess.Location;
import com.aquila.chess.Moves;
import com.aquila.chess.Piece;
import com.aquila.chess.exception.ChessPositionException;

/**
 * A Bishop Model
 *
 */
public class Bishop extends Piece {

	public Bishop(final Color color, final Location location) {
		super(color, location);
	}

	@Override
	public String getLabel() {
		return "B";
	}

	@Override
	public void getPossibleRawMove(final Moves moves, final Board board) throws ChessPositionException {
		board.checkAndAddDiag(moves, this, this.getX() - 7, this.getY() - 7, true);
		board.checkAndAddDiag(moves, this, this.getX() + 7, this.getY() - 7, true);
		board.checkAndAddDiag(moves, this, this.getX() - 7, this.getY() + 7, true);
		board.checkAndAddDiag(moves, this, this.getX() + 7, this.getY() + 7, true);
	}

	@Override
	public int getPower() {
		return 2;
	}

	@Override
	public String toPGN() {
		return "B";
	}
}
