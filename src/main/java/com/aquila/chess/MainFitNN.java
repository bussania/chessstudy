package com.aquila.chess;

import com.aquila.chess.player.mcts.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.util.Properties;

public class MainFitNN {
    static private final String NN_REFERENCE = "../AGZ_NN/AGZ.reference";
    private static final String TRAIN_SETTINGS = "train/train-settings.properties";
    @SuppressWarnings("unused")
    static private Logger logger = LoggerFactory.getLogger(MainFitNN.class);
    static private Logger logFile = LoggerFactory.getLogger("FILE");

    public static void main(final String[] args) throws Exception {
        Properties appProps = new Properties();
        appProps.load(new FileInputStream(TRAIN_SETTINGS));
        logFile.info("START MainFitNN");
        int startGame = Integer.valueOf(appProps.getProperty("start.game"));
        int endGame = Integer.valueOf(appProps.getProperty("end.game"));
        logFile.info("startGame: {}", startGame);
        logFile.info("endGame: {}", endGame);
        INN nnWhite = new NN(NN_REFERENCE, true, 0);
        final DeepLearningAGZ deepLearningWhite = new DeepLearningAGZ(nnWhite);
        UpdateLr updateLr = nbGames -> 1e-5;
        deepLearningWhite.setUpdateLr(updateLr, 0);
        for (int numGame = startGame; numGame <= endGame; numGame++) {
            logger.info("load game:{}", numGame);
            TrainGame trainGame = TrainGame.load(numGame);
            deepLearningWhite.train(trainGame);
        }
        deepLearningWhite.save();
    }

}
