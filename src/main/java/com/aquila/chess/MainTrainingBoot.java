package com.aquila.chess;

import com.aquila.chess.exception.EndOfGameException;
import com.aquila.chess.manager.GameManager;
import com.aquila.chess.manager.Record;
import com.aquila.chess.manager.Sequence;
import com.aquila.chess.player.mcts.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MainTrainingBoot {

    static public int NB_STEP = 800;
    @SuppressWarnings("unused")
    static private Logger logger = LoggerFactory.getLogger(MainTrainingBoot.class);
    static private Logger logFile = LoggerFactory.getLogger("FILE");

    public static void main(final String[] args) throws Exception {
        int numSaveGame = 1;
        logFile.info("START MainTrainingBoot");
        Dirichlet dirichlet = (game) -> false;
        UpdateCpuct updateCpuct = (nbStep) -> 2.0;
        UpdateLr updateLr = nbGames -> 1e-5;
        GameManager gameManager = new GameManager("../AGZ_NN/sequences.csv", 40, 55);
        INN nnWhite = new NNSimul();
        final DeepLearningAGZ deepLearningWhite = new DeepLearningAGZ(nnWhite);
        deepLearningWhite.setDirichlet(dirichlet);
        deepLearningWhite.setUpdateLr(updateLr, 0);
        INN nnBlack = new NNSimul();
        DeepLearningAGZ deepLearningBlack = new DeepLearningAGZ(nnBlack);
        deepLearningBlack.setDirichlet(dirichlet);
        deepLearningBlack.setUpdateLr(updateLr, 0);
        while (true) {
            Sequence sequence = gameManager.createSequence();
            long seed = System.nanoTime();
            final MCTSPlayer whitePlayer = new MCTSPlayer(deepLearningWhite, false, seed, updateCpuct, -1)
                    .withNbMaxSearchCalls(NB_STEP);
            final MCTSPlayer blackPlayer = new MCTSPlayer(deepLearningBlack, true, seed + 1000, updateCpuct, -1)
                    .withNbMaxSearchCalls(NB_STEP);

            final Board board = new Board();
            final Game game = new Game(board, whitePlayer, blackPlayer);
            game.logBoard(true);

            game.initWithAllPieces();
            try {
                do {
                    deepLearningWhite.storeInputs(game);
                    Move move = game.play();
                    sequence.play();
                    deepLearningWhite.commitInputs(game, whitePlayer.getRoot());
                    // if( game.getNbStep()==2) throw new EndOfGameException(Color.WHITE, "Todel", EndOfGameException.TypeOfEnding.CHESSMATE, move);
                } while (true);
            } catch (final EndOfGameException e) {
                logFile.info("#########################################################################");
                logFile.info("END OF game [{}] :\n{}\n{}", gameManager.getNbGames(), e.getLocalizedMessage(), game);
                ResultGame resultGame = whitePlayer.getResultGame(e);
                deepLearningWhite.saveBatch(resultGame, numSaveGame++);
                deepLearningWhite.clearCache();
                deepLearningBlack.clearCache();
                logFile.info("NN score: {}", deepLearningWhite.toString());
                Record.Status status = gameManager.endGame(resultGame, game, deepLearningWhite.getScore(), e, sequence);
                nnWhite.reset();
                nnBlack.reset();
            }
        }

    }

}
