package com.aquila.chess;

public enum Castling {
	NONE, SHORT, LONG
}