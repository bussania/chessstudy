package com.aquila.chess;

import com.aquila.chess.exception.ChessPositionException;
import com.aquila.chess.player.ChessPlayer;
import com.aquila.chess.player.mcts.BasicMove;
import com.aquila.chess.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class Moves extends LinkedList<Move> {

    /**
     *
     */
    private static final long serialVersionUID = -8858562666399962167L;

    @SuppressWarnings("unused")
    static private Logger logger = LoggerFactory.getLogger(Moves.class);

    public Moves(List<Move> moves) {
        super(moves);
    }

    public Moves() {
    }

    public final boolean setPossibleKingCheck(final ChessPlayer player) {
        for (final Move move : this) {
            if (move.canCaptureAKing()) {
                player.getKing().setCheck(true);
                // logger.info("CHESS");
                return true;
            }
        }
        return false;
    }

    public final void notateDoubleDestination() {
        for (final Move move1 : this) {
            if (move1.getCastling() != Castling.NONE)
                continue;
            for (final Move move2 : this) {
                if (move2.getCastling() != Castling.NONE)
                    continue;
                if (move1 != move2 && move1.getEndLocation() == move2.getEndLocation()
                        && move1.getPiece().getClass() == move2.getPiece().getClass()) {
//					logger.info("Double Destination detected: {} <-> {}  Destination: {}", move1, move2,
//							move1.getEndChessBox());
                    move1.setDoubleDestination(true);
                    move2.setDoubleDestination(true);
                }
            }
        }
    }

    @SafeVarargs
    public final Move getPossibleMoveWithout(final Random rand, final Class<? extends Piece>... classes) {
        Object[] arrayMove = this.toArray();
        int length = arrayMove.length;
        Utils.randomize(arrayMove, rand);
        for (int i = 0; i < arrayMove.length; i++) {
            Move move = (Move) arrayMove[i];
            if (isMovingPieceofType(move, classes) == false)
                return move;
        }

        return (Move) arrayMove[rand.nextInt(length)];
    }

    @SafeVarargs
    public final boolean isMovingPieceofType(final Move move, final Class<? extends Piece>... classes) {
        if (move.getPiece() == null)
            return false;
        if (move.getCastling() != Castling.NONE)
            return false;
        final Piece piece = move.getPiece();
        for (final Class<? extends Piece> class1 : classes) {
            if (class1.isInstance(piece))
                return true;
        }
        return false;
    }

    public final boolean canCapture(final Location location) {
        for (final Move move : this) {
            if (move.getEndLocation() == location)
                return true;
        }
        return false;
    }

    public final Move getCastlingMove() {
        for (final Move move : this) {
            if (move.getCastling() != Castling.NONE)
                return move;
        }
        return null;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer();
        for (final Move move : this) {
            sb.append(move + ", ");
        }
        return sb.toString();
    }

    public Move findRandom(final Random rand) {
        final int size = this.size();
        if (size == 0)
            return null;
        return this.get(rand.nextInt(size));
    }

    public Move find(BasicMove basicMove) throws ChessPositionException {
        for (Move move : this) {
            if (basicMove.equals(move))
                return move;
        }
        return null;
    }

    public Moves find(Class<? extends Piece> classPiece) {
        return new Moves(
                this.stream().filter(move -> classPiece.isInstance(move.getPiece())).collect(Collectors.toList()));
    }

    public Move find(final Move move2check) {
        for (Move move : this) {
            if (move.hashCode() == move2check.hashCode()) return move;
        }
        return null;
    }
}
