package com.aquila.chess;

import com.aquila.chess.exception.EndOfGameException;
import com.aquila.chess.player.ChessPlayer;
import com.aquila.chess.player.HumanPlayer;
import com.aquila.chess.player.mcts.DeepLearningAGZ;
import com.aquila.chess.player.mcts.MCTSPlayer;
import com.aquila.chess.player.mcts.NN;
import com.aquila.chess.player.mcts.UpdateCpuct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MainPlayAgainstHuman {

    @SuppressWarnings("unused")
    static private Logger logger = LoggerFactory.getLogger(MainPlayAgainstHuman.class);
    static private Logger logFile = LoggerFactory.getLogger("FILE");
    static private final String NN_REFERENCE = "../AGZ_NN/AGZ.reference";

    public static void main(final String[] args) throws Exception {
        logFile.info("START MainTrainingAGZ");
        int nbGames = 0;
        long seed = System.nanoTime();
        NN nnWhite = new NN(NN_REFERENCE, true, 0);
        final DeepLearningAGZ deepLearningWhite = new DeepLearningAGZ(nnWhite);

        deepLearningWhite.setDirichlet(game -> {
            return game.getNbStep() == 0;
        });
        UpdateCpuct updateCpuct = (nbStep) -> {
            return Math.exp(-0.02 * nbStep) / 2;
        };

        nbGames++;

        final MCTSPlayer whitePlayer = new MCTSPlayer(deepLearningWhite, false, seed, updateCpuct, 20000);

        ChessPlayer blackPlayer = new HumanPlayer();

        final Board board = new Board();
        final Game game = new Game(board, whitePlayer, blackPlayer);
        game.initWithAllPieces();
        try {
            do {
                System.out.println(game.toString());
                game.play(false);
            } while (true);
        } catch (final EndOfGameException e) {
            logFile.info("#########################################################################");
            logFile.info("END OF game [{}] :\n{}\n{}", nbGames, e.getLocalizedMessage(), game);
            logFile.info("NN score: {}", deepLearningWhite.toString());
        }

    }

}
