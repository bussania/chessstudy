/**
 * 
 */
package com.aquila.chess;

import java.util.logging.Logger;

import com.aquila.chess.exception.ChessPositionException;
import com.aquila.chess.pieces.Color;
import com.aquila.chess.player.ChessPlayer;

/**
 * An Abstact Chess Piece Model
 *
 */
public abstract class Piece implements Cloneable {

	@SuppressWarnings("unused")
	static private final Logger logger = Logger.getLogger(Piece.class.getName());

	private Color color;
	private Location location;

	public Piece(final Color color, final Location location) {
		this.color = color;
		this.location = location;
	}

	/**
	 * @return the X coordinate
	 */
	public final int getX() {
		return this.location.getX();
	}

	/**
	 * @return the Y coordinate
	 */
	public final int getY() {
		return this.location.getY();
	}

	public final Location getLocation() {
		return this.location;
	}

	/**
	 * @param board
	 * @return list of moves that can be done by this piece
	 * @throws ChessPositionException
	 */
	public final Moves getPossibleRawMove(final Board board) throws ChessPositionException {
		final Moves moves = new Moves();
		getPossibleRawMove(moves, board);
		return moves;
	}

	/**
	 * @param moves list of moves that will be updated by that can be done by this
	 *              piece
	 * @param board
	 * @throws ChessPositionException
	 */
	public abstract void getPossibleRawMove(Moves moves, Board board) throws ChessPositionException;

	/**
	 * Set the piece to the given position. The board is <strong>NOT</strong> touch
	 * by this method.
	 * 
	 * @param chessBox - the destination of this piece
	 */
	public final void set(final Location location) {
		this.location = location;
	}

	public String getLabel() {
		return "?";
	}

	public abstract String toPGN();

	public String displayBoard() {
		return getLabel() + "-" + this.getColor().toString().charAt(0);
	}

	@Override
	public String toString() {
		return displayBoard() + ":" + this.location.toString();
	}

	public final Color getColor() {
		return this.color;
	}

	/**
	 * @return arbitrary power number between 1 and 9
	 */
	public abstract int getPower();

	public static final int maxPower() {
		return 20;
	}

	@Override
	public Object clone() {
		Piece piece = null;
		try {
			piece = (Piece) super.clone();
		} catch (final CloneNotSupportedException cnse) {
			cnse.printStackTrace(System.err);
		}
		return piece;
	}

	public Piece copy(final ChessPlayer chessPlayer, final Location location) {
		final Piece piece = (Piece) this.clone();
		piece.location = location;
		piece.color = chessPlayer.getColor();
		return piece;
	}

	/**
	 *
	 * @return value in 16 bits
	 */
	public int hashCode() {
		return this.getPower() | this.color.ordinal() << 4;
	}
}
