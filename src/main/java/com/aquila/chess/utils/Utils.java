/**
 *
 */
package com.aquila.chess.utils;

import com.aquila.chess.Game;
import com.aquila.chess.Location;
import com.aquila.chess.Move;
import com.aquila.chess.Piece;
import com.aquila.chess.exception.ChessIncoherencyError;
import com.aquila.chess.exception.ChessPositionException;
import com.aquila.chess.pieces.Color;
import com.aquila.chess.pieces.Pawn;
import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import umontreal.ssj.randvarmulti.DirichletGen;
import umontreal.ssj.rng.MRG32k3a;
import umontreal.ssj.rng.RandomStream;

import java.util.Arrays;
import java.util.Random;

/**
 * @author bussa
 */
public class Utils {

    static private Logger logger = LoggerFactory.getLogger(Utils.class);

    static private RandomStream stream = new MRG32k3a();

    /**
     * @param x 0 - 7
     * @param y 0 - 7
     * @return Algebrique Coordinate, E2, etc ...
     */
    public static String coordAlgebrique(int x, int y) {
        return String.format("%c%d", (char) ('a' + x), y + 1);
    }

    @SuppressWarnings("incomplete-switch")
    public static int scoreFirstLevel(Move move) {
        int score = 0;
        if (move.isCreateChessMate()) {
            return 100;
        }
        if (move.isCreateCheck() && move.getCanBeCaptured() == null) {
            return 10;
        }
        if (move.getCapturePiece() != null && move.getCanBeCaptured() == null
                && score < move.getCapturePiece().getPower()) {
            score += move.getCapturePiece().getPower();
        }
        if (move.getPiece().getClass() == Pawn.class && move.getCanBeCaptured() == null) {
            final int y = move.getEndLocation().getY();
            switch (move.getColor()) {
                case WHITE:
                    if (y > 5)
                        score += y;
                    break;
                case BLACK:
                    if (y < 3)
                        score += y;
                    break;
            }
        }
        return score;
    }

    /**
     * Check the validity of the board at any time, validity in term of internal
     * structures
     *
     * @throws ChessPositionException
     */
    public static void check(Game game) throws ChessPositionException {
        int nbWhitePieces = 0;
        int nbBlackPieces = 0;
        Piece piece;
        for (int x = 0; x < 8; x++) {
            for (int y = 0; y < 8; y++) {
                piece = game.getBoard().getPiece(x, y);
                if (piece != null) {
                    if (!(piece.getX() == x && piece.getY() == y)) {
                        throw new RuntimeException(("ERROR [" + x + "," + y + "]-> piece: " + piece.displayBoard()
                                + piece.getLocation().coordAlgebrique()));
                    }
                    switch (piece.getColor()) {
                        case WHITE:
                            nbWhitePieces++;
                            break;
                        case BLACK:
                            nbBlackPieces++;
                            break;
                        default:
                            throw new RuntimeException(String.format("Wrong color for piece %s in coordinate: %s", piece,
                                    Location.get(x, y).toString()));
                    }
                }
            }
        }
        for (Piece pieceLoop : game.getPlayer(Color.BLACK).getPieces()) {
            piece = game.getBoard().getPiece(pieceLoop.getX(), pieceLoop.getY());
            if (pieceLoop.getLocation() != piece.getLocation()) {
                logger.error("BOARD ERROR:\n" + game);
                throw new ChessIncoherencyError("Wrong ChessBox for piece:" + pieceLoop);
            }
        }
        for (Piece pieceLoop : game.getPlayer(Color.WHITE).getPieces()) {
            piece = game.getBoard().getPiece(pieceLoop.getX(), pieceLoop.getY());
            if (pieceLoop.getLocation() != piece.getLocation()) {
                logger.error("BOARD ERROR:\n" + game);
                throw new ChessIncoherencyError("Wrong ChessBox for piece:" + pieceLoop);
            }
        }
        if (nbWhitePieces != game.getPlayer(Color.WHITE).getPieces().size()) {
            logger.error("BOARD ERROR:\n" + game);
            throw new ChessIncoherencyError(
                    "Wrong number of WHITE pieces! found piece on board: %d <-> %d number of pieces for the player",
                    nbWhitePieces, game.getPlayer(Color.WHITE).getPieces().size());
        }
        if (nbBlackPieces != game.getPlayer(Color.BLACK).getPieces().size()) {
            logger.error("BOARD ERROR:\n" + game);
            throw new ChessIncoherencyError(
                    "Wrong number of BLACK pieces! found piece on board: %d <-> %d number of pieces for the player",
                    nbBlackPieces, game.getPlayer(Color.BLACK).getPieces().size());
        }
        if (!((nbWhitePieces + game.getPlayer(Color.BLACK).getCapturePieces().size()) == 16
                && (nbBlackPieces + game.getPlayer(Color.WHITE).getCapturePieces().size()) == 16)) {
            logger.error("BOARD ERROR:\n" + game);
            String msg = String.format(
                    "Wrong number of pieces: White board pieces:%s White captured:%s | Black board pieces:%s Black captured:%s", //
                    nbWhitePieces, game.getPlayer(Color.BLACK).getCapturePieces().size(), //
                    nbBlackPieces, game.getPlayer(Color.WHITE).getCapturePieces().size());
            throw new ChessIncoherencyError(msg);
        }
    }

    public static double getRandom(double min, double max, Random rand) {
        double zeroOne = rand.nextDouble();
        return (max - min) * zeroOne + min;
    }

    /**
     * Fisher Yates shuffle Algorithm
     *
     * @param arr
     * @param rand
     */
    public static void randomize(Object arr[], Random rand) {
        int n = arr.length;

        // Start from the last element and swap one by one. We don't
        // need to run for the first element that's why i > 0
        for (int i = n - 1; i > 0; i--) {
            // Pick a random index from 0 to i
            int j = rand.nextInt(i);
            // Swap arr[i] with the element at random index
            Object temp = arr[i];
            arr[i] = arr[j];
            arr[j] = temp;
        }
    }

    public static double[] normalise(double[] policies) {
        double min = Double.POSITIVE_INFINITY;
        double max = Double.NEGATIVE_INFINITY;
        for (double policy : policies) {
            if (policy < min)
                min = policy;
            if (policy > max)
                max = policy;
        }
        double maxMin = max - min;
        if (maxMin > 0) {
            for (int i = 0; i < policies.length; i++) {
                policies[i] = (policies[i] - min) / maxMin;
            }
        }
        return policies;
    }


    public static double[] toDistribution(double[] policies, int[] indexes, boolean dirichlet) {
        int nbMoves = indexes.length;
        double sum = 0;
        for (int i = 0; i < policies.length; i++) {
            if (ArrayUtils.contains(indexes, i)) {
                sum += policies[i];
            }
        }
        for (int i = 0; i < policies.length; i++) {
            if (ArrayUtils.contains(indexes, i)) {
                policies[i] = policies[i] / sum;
            } else {
                policies[i] = 0;
            }
        }
        if (dirichlet) {
            double alpha[] = new double[indexes.length];
            Arrays.fill(alpha, 0.3);
            DirichletGen dirichletGen = new DirichletGen(stream, alpha);
            double epsilon = 0.25;
            int index = 0;
            double d[] = new double[alpha.length];
            dirichletGen.nextPoint(d);
            double p;
            for (int i = 0; i < policies.length; i++) {
                if (ArrayUtils.contains(indexes, i)) {
                    p = policies[i];
                    double newP = (1 - epsilon) * p + epsilon * d[index];
                    policies[i] = newP;
                    index++;
                }
            }
            logger.info("dirichlet: indexes: {}", indexes.length);
        }
        return policies;
    }

    @Deprecated
    public static double[] toDistributionOld(double[] policies, int[] indexes, boolean dirichlet) {
        double sum = 0;
        for (int i = 0; i < policies.length; i++) {
            sum += policies[i];
        }
        for (int i = 0; i < policies.length; i++) {
            policies[i] = policies[i] / sum;
        }
        if (dirichlet) {
            double alpha[] = new double[policies.length];
            Arrays.fill(alpha, 0.3);
            DirichletGen dirichletGen = new DirichletGen(stream, alpha);
            double epsilon = 0.25;
            int index = 0;
            double d[] = new double[alpha.length];
            dirichletGen.nextPoint(d);
            double p;
            for (int i = 0; i < policies.length; i++) {
                p = policies[i];
                double newP = (1 - epsilon) * p + epsilon * d[i];
                policies[i] = newP;
            }
        }
        return policies;
    }

}
