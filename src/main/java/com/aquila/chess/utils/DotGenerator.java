/**
 * 
 */
package com.aquila.chess.utils;

import com.aquila.chess.pieces.Color;
import com.aquila.chess.player.mcts.MCTSNode;

import info.leadinglight.jdot.Edge;
import info.leadinglight.jdot.Graph;
import info.leadinglight.jdot.Node;
import info.leadinglight.jdot.enums.Shape;

/**
 * @author bussa
 *
 */
public class DotGenerator {

	public static String toString(MCTSNode node, int depthMax) {
		final DotGenerator dotGenerator = new DotGenerator();
		final Graph graph = dotGenerator.generate(node, depthMax);
		return graph.toDot();	
	}
	
	// digraph structs { node [shape=record] "1552326679" [label=" { ligne1 | ligne2
	// |{ <fils1> fils1|<fils2> fils2 }}" shape=record] }
	/*
	 * digraph structs { node [shape=record] "400731411"
	 * [label="{ BLACK | d7 - e8 | Visits: 3 | <1995054261> 0.094828704992930 | <1788132713> 0.2844861894845962 }"
	 * ] node [shape=record] "1995054261" [label="{ WHITE | d3 - b5 | Visits: 2  }"
	 * ] node [shape=record] "1788132713" [label="{ BLACK | d7 - d8 | Visits: 1 }" ]
	 * 400731411 -> { 1995054261 } [ label="0.094828704992930" ]; 400731411 ->
	 * 1788132713 [ label="0.094828704992930" ]; }
	 */
	public Graph generate(MCTSNode node, int depthMax) {
		Graph g = new Graph("structs");
		generate(g, node, 0, depthMax);
		return g;
	}

	private int generate(Graph g, final MCTSNode node, int depth, int depthMax) {
		if (depth >= depthMax)
			return 0;
		String szMove = node.getBasicMove() == null ? "root" : node.getBasicMove().toString();
		Color color = node.getColor();
		String core = String.format("%s | %s | %s | V:%f | P:%f | Sum:%f", //
				color == null ? "no color" : color.toString(), //
				node.getPiece(), //
				szMove, //
				node.getValue(), //
				node.getPolicy(), //
				node.getSumValues());
		Shape shape = null;
		switch (node.getState()) {
		case INTERMEDIATE:
			shape = Shape.record;
			break;
		case WIN:
			shape = Shape.tripleoctagon;
			core = String.format("%s | %s", core, node.getState());
			break;
		case LOOSE:
			shape = Shape.octagon;
			core = String.format("%s | %s", core, node.getState());
			break;
		case PAT:
		case REPEAT_50:
		case REPETITION_X3:
		case NOT_ENOUGH_PIECES:
		case NB_MOVES_200:
			shape = Shape.doubleoctagon;
			core = String.format("%s | %s", core, node.getState());
			break;
		}
		if (node.getChilds().size() == 0) {
			g.addNodes(new Node().setShape(Shape.record),
					new Node("" + node.hashCode()).setShape(shape).setLabel(core));
			return node.hashCode();
		}
		String nodeLabel = String.format("{ %s }", core);
		g.addNodes(new Node().setShape(Shape.record),
				new Node("" + node.hashCode()).setShape(shape).setLabel(nodeLabel));
		node.getChilds().forEach(child -> {
			Double reward = child.getExpectedReward();
			int visits = child.getVisits();
			int hashCode = generate(g, child, depth+1, depthMax);
			g.addEdges(new Edge().addNode("" + node.hashCode(), "").addNode("" + hashCode, "")
					.setLabel(String.format("%d | %f", visits, reward)));
		});
		return node.hashCode();
	}
}
