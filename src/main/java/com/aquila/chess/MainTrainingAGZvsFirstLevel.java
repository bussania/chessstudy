package com.aquila.chess;

import com.aquila.chess.exception.EndOfGameException;
import com.aquila.chess.manager.GameManager;
import com.aquila.chess.manager.Record.Status;
import com.aquila.chess.manager.Sequence;
import com.aquila.chess.player.ChessPlayer;
import com.aquila.chess.player.RandomPlayerFirstLevel;
import com.aquila.chess.player.mcts.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MainTrainingAGZvsFirstLevel {

    @SuppressWarnings("unused")
    static private Logger logger = LoggerFactory.getLogger(MainTrainingAGZvsFirstLevel.class);
    static private Logger logFile = LoggerFactory.getLogger("FILE");
    static private final String NN_REFERENCE = "../AGZ_NN/AGZ.reference";

    public static void main(final String[] args) throws Exception {
        int numGame = 0;
        logFile.info("START MainTrainingAGZ");
        GameManager gameManager = new GameManager("../AGZ_NN/sequences.csv", 40, 55);
        UpdateLr updateLr = new UpdateLr() {

            @Override
            public double update(int nbGames) {
                return 1e-5;
            }
        };
        NN nnWhite = new NN(NN_REFERENCE, true, gameManager.getNbGames());
        final DeepLearningAGZ deepLearningWhite = new DeepLearningAGZ(nnWhite);
        deepLearningWhite.setDirichlet(game -> {
            return true; // game.getNbStep() == 0;
        });
        deepLearningWhite.setUpdateLr(updateLr, 0);
        UpdateCpuct updateCpuct = (nbStep) -> {
            return 2.0 * Math.exp(-0.01 * nbStep);
            // return nbStep < 30 ? 2.0 : 0.01;
            // return 2.0;
        };
        while (true) {
            Sequence sequence = gameManager.createSequence();
            long seed = System.nanoTime();
            final MCTSPlayer whitePlayer = new MCTSPlayer(deepLearningWhite, false, seed, updateCpuct, -1)
                    .withNbMaxSearchCalls(800);

            final ChessPlayer blackPlayer = new RandomPlayerFirstLevel(seed + 1000);

            final Board board = new Board();
            final Game game = new Game(board, whitePlayer, blackPlayer);
            game.logBoard(true);
            game.initWithAllPieces();
            try {
                do {
                    deepLearningWhite.storeInputs(game);
                    game.play(false);
                    sequence.play();
                    deepLearningWhite.commitInputs(game, whitePlayer.getRoot());
                } while (true);
            } catch (final EndOfGameException e) {
                logFile.info("#########################################################################");
                logFile.info("END OF game [{}] :\n{}\n{}", gameManager.getNbGames(), e.getLocalizedMessage(), game);
                ResultGame resultGame = whitePlayer.getResultGame(e);
                deepLearningWhite.saveBatch(resultGame, numGame++);
                // ResultGame resultGame = whitePlayer.storeResultGame(e, gameManager.getNbGames());
                logFile.info("NN score: {}", deepLearningWhite.toString());
                Status status = gameManager.endGame(resultGame, game, deepLearningWhite.getScore(), e, sequence);
            }
        }

    }

}
