package com.aquila.chess;

import java.util.ArrayList;

import com.aquila.chess.exception.ChessIncoherencyError;
import com.aquila.chess.exception.ChessPositionException;
import com.aquila.chess.pieces.King;
import com.aquila.chess.player.ChessPlayer;

public class Pieces extends ArrayList<Piece> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8333780083702348852L;

	public int nbPieceOfType(final Class<? extends Piece> classToCheck) {
		int ret = 0;
		for (Piece piece : this) {
			if (piece.getClass() == classToCheck)
				ret++;
		}
		return ret;
	}

	public King findKing() {
		for (Piece piece : this) {
			if (piece instanceof King)
				return (King) piece;
		}
		return null;
	}

	public void check(Board board) throws ChessIncoherencyError {
		for (Piece piece : this) {
			Piece optionalPiece = board.getPiece(piece.getX(), piece.getY());
			if (optionalPiece == null)
				throw new ChessIncoherencyError("Piece: %s <-//-> board(%d,Md)", piece, piece.getX(), piece.getY());
		}
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		for (Piece piece : this) {
			sb.append(piece + ", ");
		}
		return sb.toString();
	}

	public Pieces copy(ChessPlayer chessPlayer, Board board) throws ChessPositionException {
		Pieces pieces = new Pieces();
		for (Piece piece : this) {
			Piece copyPiece = piece.copy(chessPlayer, piece.getLocation());
			pieces.add(copyPiece);
		}
		return pieces;
	}

}
