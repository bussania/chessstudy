/**
 * 
 */
package com.aquila.chess;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aquila.chess.exception.ChessIncoherencyError;
import com.aquila.chess.exception.ChessPositionException;
import com.aquila.chess.pieces.Color;

/**
 * The pieces are identified by their initials. In English, these are K (king),
 * Q (queen), R (rook), B (bishop), and N (knight; N is used to avoid confusion
 * with king). For example, Qg5 means "queen moves to the g-file, 5th rank"
 * (that is, to the square g5). Different initials may be used for other
 * languages. In chess literature figurine algebraic notation (FAN) may be used
 * to avoid language issues.
 */
public final class Board {

	@SuppressWarnings("unused")
	static private Logger logger = LoggerFactory.getLogger(Board.class);

	public static final int NB_COL = 8;

	public static final int PAWN_INDEX = 0;
	public static final int KNIGHT_INDEX = 1;
	public static final int BISHOP_INDEX = 2;
	public static final int ROOK_INDEX = 3;
	public static final int QUEEN_INDEX = 4;
	public static final int KING_INDEX = 5;

	@SuppressWarnings("unchecked")
	Piece[][] chessBox = new Piece[NB_COL][NB_COL];

	public Board() {
	}

	public void setPiece(int x, int y, Piece piece) {
		this.chessBox[x][y] = piece;
	}

	public void setPiece(String coordAlgebique, Piece piece) throws ChessPositionException {
		Location location = Location.get(coordAlgebique);
		this.chessBox[location.getX()][location.getY()] = piece;
	}

	/**
	 * @param xIndex x coordinate
	 * @param yIndex Y coordinate
	 * @return the piece located on the corrdinate or null if nothing was found
	 */
	public final Piece getPiece(final int xIndex, final int yIndex) {
		if (Board.isInsideBoard(xIndex, yIndex))
			return this.chessBox[xIndex][yIndex];
		else
			return null;
	}

	/**
	 * @param moves the list of moves where to add a move if possible
	 * @param x     the destination horizontal coordinate
	 * @param y     the destination vertical coordinate
	 * @return true if the destination is empty
	 * @throws ChessPositionException
	 */
	public final boolean checkAndAdd(final Moves moves, final Piece piece, final int x, final int y,
			final boolean canCapture) throws ChessPositionException {
		if (Board.isInsideBoard(x, y) == false)
			return false;
		Piece destPiece = this.getPiece(x, y);
		Move move = new Move(this, piece, Location.get(x, y));
		if (destPiece != null) {
			if (destPiece.getColor() != piece.getColor() && canCapture) {
				move.setCapturePiece(destPiece);
				moves.add(move);
			}
			return false;
		} else {
			moves.add(move);
		}
		return true;
	}

	/**
	 * @param moves
	 * @param endX
	 * @param endY
	 * @throws ChessPositionException
	 */
	public final void checkAndAddLine(final Moves moves, final Piece piece, final int endX, final int endY,
			final boolean canCapture) throws ChessPositionException {
		int startX = piece.getX();
		int startY = piece.getY();
		int deltaX = Integer.signum(endX - startX);
		int deltaY = Integer.signum(endY - startY);
		int start, end;

		if (deltaX != 0 && deltaY != 0)
			throw new ChessIncoherencyError("checkAndAddLine: %s wrong deltaX:%d or deltaY:%d", piece.toPGN(), deltaX,
					deltaY);
		if (deltaX != 0) {
			start = startX + deltaX;
			end = endX + deltaX;

			for (int x = start; x != end && checkAndAdd(moves, piece, x, startY, canCapture); x += deltaX)
				;
		} else {
			start = startY + deltaY;
			end = endY + deltaY;
			;

			for (int y = start; y != end && checkAndAdd(moves, piece, startX, y, canCapture); y += deltaY)
				;
		}
	}

	/**
	 * @param moves
	 * @param endX
	 * @param endY
	 * @throws ChessPositionException
	 */
	public final void checkAndAddDiag(final Moves moves, final Piece piece, final int endX, final int endY,
			final boolean canCapture) throws ChessPositionException {
		int startX = piece.getX();
		int startY = piece.getY();
		int deltaX = (startX < endX ? +1 : -1);
		int deltaY = (startY < endY ? +1 : -1);
		boolean stop = false;

		if (deltaX == 0 || deltaY == 0)
			throw new ChessIncoherencyError("checkAndAddDiag: %s wrong deltaX:%d or deltaY:%d", piece.toPGN(), deltaX,
					deltaY);
		for (int x = startX + deltaX, y = startY + deltaY; !stop; x += deltaX, y += deltaY) {
			if (checkAndAdd(moves, piece, x, y, canCapture) == false)
				return;
			if (startX == endX || Board.isInsideBoard(y) == false)
				stop = true;
		}
	}

	/**
	 * @return a unique hashcode based only on the board configuration
	 */
	@SuppressWarnings("incomplete-switch")
	@Override
	public int hashCode() {
		return hashCode(null);
	}

	@SuppressWarnings("incomplete-switch")
	public int hashCode(Move move) {
		int chess[][] = new int[NB_COL][NB_COL];
		Piece piece;

		for (int x = 0; x < NB_COL; x++) {
			for (int y = 0; y < NB_COL; y++) {
				piece = this.getPiece(x, y);
				if (piece != null) {
					if (!(piece.getX() == x && piece.getY() == y)) {
						throw new RuntimeException(
								("ERROR [" + x + "," + y + "]-> piece: " + piece.displayBoard() + piece.toPGN()));
					}
					switch (piece.getColor()) {
					case WHITE:
						chess[x][y] = piece.getPower();
						break;
					case BLACK:
						chess[x][y] = 2 * piece.getPower();
						break;
					}
				}
			}
		}
		if (move != null) {
			int y = 0;
			if (move.getColor() == Color.WHITE) {
				y = 0;
			} else {
				y = 7;
			}
			switch (move.getCastling()) {
			case NONE:
				int startX = move.getStartLocation().getX();
				int startY = move.getStartLocation().getY();
				int endX = move.getEndLocation().getX();
				int endY = move.getEndLocation().getY();
				chess[endX][endY] = chess[startX][startY];
				chess[startX][startY] = 0;
				break;
			case SHORT:
				// king
				chess[6][y] = chess[4][y];
				chess[4][y] = 0;
				// rook
				chess[5][y] = chess[7][y];
				chess[7][y] = 0;
				break;
			case LONG:
				// king
				chess[2][y] = chess[4][y];
				chess[4][y] = 0;
				// rook
				chess[3][y] = chess[0][y];
				chess[0][y] = 0;
				break;
			}
		}
		int factor = 1;
		int ret = 0;
		for (int x = 0; x < NB_COL; x++) {
			for (int y = 0; y < NB_COL; y++) {
				ret += Piece.maxPower() * factor * chess[x][y];
				factor = factor + 2 * (Piece.maxPower() + 1);
			}
		}
		return ret;
	}

	public final Piece getPiece(final String coordAlgebique) throws ChessPositionException {
		Location location = Location.get(coordAlgebique);
		return this.chessBox[location.getX()][location.getY()];
	}

	public Piece getPiece(Location location) {
		return this.chessBox[location.getX()][location.getY()];
	}

	static public final boolean isInsideBoard(final int x, final int y) {
		return Location.isInsideBoard(x, y);
	}

	public static final boolean isInsideBoard(final int xOry) {
		return Location.isInsideBoard(xOry, xOry);
	}

	/**
	 * Return the chess board as String
	 */
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append(String.format("BOARD HASHCODE: %s\n", this.hashCode()));
		sb.append("    ");
		for (int x = 0; x < NB_COL; x++) {
			sb.append("[" + (char) ('a' + x) + "] ");
		}
		sb.append("\n");
		for (int y = NB_COL - 1; y >= 0; y--) {
			sb.append(" " + (y + 1) + "  ");
			for (int x = 0; x < NB_COL; x++) {
				Piece piece = this.chessBox[x][y];
				if (piece != null) {
					sb.append(piece.displayBoard());
					sb.append(" ");
				} else {
					sb.append("--- ");
				}
			}
			sb.append(" " + (y + 1) + "  ");
			sb.append("\n");
		}
		sb.append("    ");
		for (int x = 0; x < NB_COL; x++) {
			sb.append("[" + (char) ('a' + x) + "] ");
		}
		sb.append("\n");
		return sb.toString();
	}


	public final boolean contains(final int x, final int y, final Class<? extends Piece> class1, final Color color) {
		if (this.getPiece(x, y) == null)
			return false;
		Piece piece = this.getPiece(x, y);
		if (piece.getColor() != color)
			return false;
		return this.getPiece(x, y).getClass() == class1;
	}

}
