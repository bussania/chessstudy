/**
 *
 */
package com.aquila.chess;

import com.aquila.chess.exception.ChessIncoherencyError;
import com.aquila.chess.exception.ChessPositionException;
import com.aquila.chess.exception.EndOfGameException;
import com.aquila.chess.pieces.Color;
import com.aquila.chess.pieces.King;
import com.aquila.chess.pieces.Pawn;
import com.aquila.chess.player.mcts.BasicMove;
import com.aquila.chess.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Move {

    static private Logger logger = LoggerFactory.getLogger(Move.class);

    static public final String CASTLING_SHORT = "O-O";

    static public final String CASTLING_LONG = "O-O-O";

    private Castling castling;

    boolean done = false;

    private Location start;
    private Location end;

    private Piece capturePiece = null;

    private Piece promotedPiece = null;

    private Piece piece;

    private boolean gettingPromoted = false;

    private boolean createCheck = false;

    private boolean createChessMate = false;

    /**
     * true, set the move as creating a check to the opponent player
     */
    private boolean check = false;

    private boolean doubleDestination = false;

    private Piece canBeCaptured;

    private EndOfGameException endOfGameException = null;

    /**
     * Create a move of the given piece from its origin to the given chessBox
     *
     * @param piece       the piece to move
     * @param endChessBox the destination chessbox
     * @throws ChessPositionException
     */
    public Move(final Board board, final Piece piece, final Location destLocation) throws ChessPositionException {
        init(board, piece, destLocation);
    }

    public Move(final Piece piece, final Castling castling) throws ChessPositionException {
        this.piece = piece;
        this.castling = castling;
        initCastlingCoord();
    }

    public Move(final Board board, final Color color, final String moveSz) throws ChessPositionException {
        Piece piece;
        switch (moveSz.toUpperCase()) {
            case "O-O":
            case "0-0":
                String coordinatePiece = color == Color.WHITE ? "E1" : "E8";
                piece = board.getPiece(coordinatePiece);
                if (piece == null)
                    throw new ChessPositionException("Not piece found on the source: " + coordinatePiece);
                this.piece = piece;
                this.castling = Castling.SHORT;
                initCastlingCoord();
                return;
            case "O-O-O":
            case "0-0-0":
                coordinatePiece = this.getColor() == Color.WHITE ? "E1" : "E8";
                piece = board.getPiece(coordinatePiece);
                if (piece == null)
                    throw new ChessPositionException("Not piece found on the source: " + coordinatePiece);
                this.piece = piece;
                this.castling = Castling.LONG;
                initCastlingCoord();
                return;
        }
        String sz[] = moveSz.split("-");
        if (sz.length != 2)
            throw new ChessPositionException("StaticPLayer: move incorrect: " + moveSz);
        Location src = Location.get(sz[0]);
        Location dst = Location.get(sz[1]);
        piece = board.getPiece(src);
        if (piece == null) {
            throw new ChessPositionException("Not piece found on the source: " + src);
        }
        init(board, piece, dst);
    }

    public Move(final Move move) {
        this.capturePiece = move.capturePiece;
        this.start = move.start;
        this.end = move.end;
        this.castling = move.castling;
        this.piece = move.piece;
        this.done = move.done;
        this.doubleDestination = move.doubleDestination;
        this.check = move.check;
    }

    public Move(final BasicMove basicMove, final Board board) throws ChessPositionException {
        final Piece piece = board.getPiece(basicMove.getStartX(), basicMove.getStartY());
        basicMove.setPiece(piece);
        switch (basicMove.getCastling()) {
            case NONE:
                if (piece == null) {
                    logger.error(board.toString());
                    throw new ChessIncoherencyError("Transformation BasicMove -> Move not working, piece not found in %s",
                            Utils.coordAlgebrique(basicMove.getStartX(), basicMove.getStartY()));
                }

                final Location end = Location.get(basicMove.getEndX(), basicMove.getEndY());
                init(board, piece, end);
                break;
            case LONG:
            case SHORT:
                this.castling = basicMove.getCastling();
                this.piece = piece;
                break;
        }
        initCastlingCoord();
    }

    @SuppressWarnings("incomplete-switch")
    private void initCastlingCoord() throws ChessPositionException {
        switch (castling) {
            case SHORT:
                switch (this.getColor()) {
                    case WHITE:
                        this.start = Location.get("E1");
                        this.end = Location.get("G1");
                        break;
                    case BLACK:
                        this.start = Location.get("E8");
                        this.end = Location.get("G8");
                        break;
                }
                break;
            case LONG:
                switch (this.getColor()) {
                    case WHITE:
                        this.start = Location.get("E1");
                        this.end = Location.get("C1");
                        break;
                    case BLACK:
                        this.start = Location.get("E8");
                        this.end = Location.get("C8");
                        break;
                }
        }

    }

    private void init(final Board board, final Piece piece, final Location destLocation) throws ChessPositionException {
        this.castling = Castling.NONE;
        this.piece = piece;
        this.start = piece.getLocation();
        this.end = destLocation;
        if (board.getPiece(end) != null) {
            this.capturePiece = board.getPiece(end);
        }
        if (this.piece instanceof Pawn && //
                (this.end.getY() == 0 || this.end.getY() == 7)) {
            this.gettingPromoted = true;
        }
        initCastlingCoord();
    }

    @Override
    public String toString() {
        return getAlgebricNotation();
    }

    /**
     * @return algebric notation
     */
    public String getAlgebricNotation() {
        switch (this.castling) {
            case SHORT:
                return "O-O";
            case LONG:
                return "O-O-O";
            default:
                String promotion = "";
                String label = this.piece.getLabel();
                if (this.capturePiece != null)
                    label += "x";
                if (isGettingPromoted()) {
                    promotion = "+";
                    label = " ";
                }
                return String.format("%s%s %s%s", label, this.start.coordAlgebrique(), " ", this.end.coordAlgebrique(),
                        promotion);
        }
    }

    public boolean isGettingPromoted() {
        return this.gettingPromoted;
    }

    /**
     * At this point the piece is located by its own position piece.getCheckBox()
     * startPosition and end Position are kept the same as when the move was build
     * for a possible undo() on the move
     *
     * @return move in Portable Notation Game
     */
    public String toPGN() {
        final StringBuffer sb = new StringBuffer();
        switch (this.castling) {
            case NONE:
                sb.append(piece.toPGN());
                if ((this.capturePiece != null && piece instanceof Pawn) || this.doubleDestination) {
                    sb.append(this.start.coordAlgebrique());
                }
                if (this.capturePiece != null) {
                    sb.append("x");
                }
                sb.append(this.end.coordAlgebrique());
                if (this.promotedPiece != null) {
                    sb.append("=");
                    sb.append(promotedPiece.toPGN());
                }
                if (this.isCheck()) {
                    sb.append("+");
                }
                break;
            case SHORT:
                sb.append(CASTLING_SHORT);
                break;
            case LONG:
                sb.append(CASTLING_LONG);
                break;
        }
        return sb.toString();
    }

    private boolean isCheck() {
        return this.check;
    }

    public void setDone(final boolean done) {
        this.done = done;
    }

    /**
     * <strong>This can be not working if you increase the value of {@link Board#NB_COL}</strong>
     */
    @Override
    public int hashCode() {
        return this.start.moveHashcode(this.end) | (this.getPiece().hashCode() << 16);
    }

    @Override
    public boolean equals(Object object) {
        if (object instanceof Move) {
            Move move = (Move) object;
            return this.start == move.start && this.end == move.end;
        } else {
            return object.equals(this);
        }
    }

    public static boolean equals(final Move moveRef, final Move... moves) {
        for (final Move move : moves) {
            if (move.equals(moveRef) == false)
                return false;
        }
        return true;
    }

    /**
     * @return true if the move can capture the opponent king, this should be only a
     *         Raw move
     */
    public boolean canCaptureAKing() {
        final boolean ret = this.capturePiece != null && this.capturePiece instanceof King;
        if (ret) {
            // logger.info("canCaptureAKing ({},{}) ret: {}", this.piece.getColor(),
            // this.capturePiece.getColor(), ret);
        }
        return ret;
    }

    public final Color getColor() {
        return this.piece.getColor();
    }

    public void setEndGameException(final EndOfGameException endOfGameException) throws EndOfGameException {
        this.endOfGameException = endOfGameException;
        throw endOfGameException;
    }

    /**
     * @return the promotedPiece
     */
    public Piece getPromotedPiece() {
        return promotedPiece;
    }

    public void setPromotedPiece(Piece promotedPiece) {
        this.promotedPiece = promotedPiece;
    }

    public void setCreateChessMate(boolean chessmate) {
//		logger.info("setCreateChessMate({}): {}", chessmate, this);
        this.createChessMate = chessmate;
    }

    public boolean isCreateChessMate() {
        return this.createChessMate;
    }

    public EndOfGameException getEndOfGameException() {
        return this.endOfGameException;
    }

    public Castling getCastling() {
        return this.castling;
    }

    public void setCapturePiece(Piece piece) {
        this.capturePiece = piece;
    }

    public Piece getPiece() {
        return this.piece;
    }

    public void setCheck(boolean check) {
        this.check = check;
    }

    public Piece getCapturePiece() {
        return this.capturePiece;
    }

    public void setDoubleDestination(boolean doubleDestination) {
        this.doubleDestination = doubleDestination;
    }

    public void setCreateCheck(boolean createCheck) {
        this.createCheck = createCheck;
    }

    public void setCanBeCaptured(Piece canBeCaptured) {
        this.canBeCaptured = canBeCaptured;
    }

    public Location getStartLocation() {
        return this.start;
    }

    public Location getEndLocation() {
        return this.end;
    }

    public boolean isCreateCheck() {
        return createCheck;
    }

    public Piece getCanBeCaptured() {
        return canBeCaptured;
    }

    public Location getStart() {
        return this.start;
    }

    public Move reverse() {
        Move retMove = new Move(this);
        Location newEnd = retMove.start;
        retMove.start = retMove.end;
        retMove.end = newEnd;
        return retMove;
    }
}
