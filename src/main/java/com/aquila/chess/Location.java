package com.aquila.chess;

import com.aquila.chess.exception.ChessPositionException;
import com.aquila.chess.utils.Utils;

public class Location {
    static public final Location PLAYERSIDE = new Location(-1, -1);

    static private final Location[][] table = new Location[Board.NB_COL][Board.NB_COL];

    private int x;
    private int y;

    public static Location get(int x, int y) throws ChessPositionException {
        if (isInsideBoard(x, y) == false)
            throw new ChessPositionException("Position outside of the board: " + x + "," + y);
        return table[x][y];
    }

    public static Location get(String coordAlgebique) throws ChessPositionException {
        if (coordAlgebique == null || coordAlgebique.length() != 2)
            throw new ChessPositionException("Incorrect given position: " + coordAlgebique);
        int x = coordAlgebique.toLowerCase().toCharArray()[0] - 'a';
        int y = coordAlgebique.toCharArray()[1] - '1';
        if (isInsideBoard(x, y) == false)
            throw new ChessPositionException("Position outside of the board: " + coordAlgebique);
        return Location.get(x, y);
    }

    /**
     * @param x
     * @param y
     */
    private Location(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /**
     *
     * @param end
     * @return a value in 16 bits
     */
    public int moveHashcode(Location end) {
        return (this.getX()) //
                | (this.getY() << 4) //
                | (end.getX() << 8) //
                | (end.getY() << 12);
    }

    /**
     * @return the x - between 0 and NB_COL {@link Board#NB_COL} - 1 (typically 0 : 7)
     */
    public int getX() {
        return x;
    }

    /**
     * @return the y- between 0 and {@link Board#NB_COL} - 1 (typically 0 : 7)
     */
    public int getY() {
        return y;
    }

    static public final boolean isInsideBoard(final int x, final int y) {
        return x >= 0 && x < Board.NB_COL && y >= 0 && y < Board.NB_COL;
    }

    public String coordAlgebrique() {
        return Utils.coordAlgebrique(getX(), getY());
    }

    @Override
    public String toString() {
        return coordAlgebrique();
    }

    static {
        for (int x = 0; x < Board.NB_COL; x++) {
            for (int y = 0; y < Board.NB_COL; y++) {
                Location.table[x][y] = new Location(x, y);
            }
        }
    }
}
