/**
 * 
 */
package com.aquila.chess.player;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aquila.chess.Game;
import com.aquila.chess.Move;
import com.aquila.chess.Moves;
import com.aquila.chess.exception.ChessIncoherencyError;
import com.aquila.chess.exception.ChessPositionException;
import com.aquila.chess.exception.EndOfGameException;
import com.aquila.chess.pieces.Color;

public class StaticPlayer extends ChessPlayer {

	static private Logger logger = LoggerFactory.getLogger(StaticPlayer.class);

	final private List<String> moveSzs = new ArrayList<>();
	private int sequenceNumber = 0;

	private String sequence;

	/**
	 * @param color    color of the player
	 * @param sequence algebraic moves (e2-e4) separated by semi-colon
	 */
	public StaticPlayer(String sequence) {
		this.sequence = sequence;
	}

	@Override
	public void init(Game game, Color color) {
		super.init(game, color);
		Stream.of(sequence.split(";")).forEach(moveSz -> {
			moveSzs.add(moveSz);
		});
	}

	@Override
	public Move nextPlay(Move moveOpponent, Moves moves) throws EndOfGameException, ChessPositionException {
		String moveSz = moveSzs.get(sequenceNumber++);
		Move retMove = new Move(game.getBoard(), color, moveSz);
		boolean found = false;
		for (Move move : moves) {
			if (move.equals(retMove))
				found = true;
		}
		if (found == false) {
			String msg = String.format("Move: %s not found in the list of correct moves:\n%s", retMove, moves);
			logger.error(msg);
			throw new ChessIncoherencyError(msg);
		}
		return retMove;
	}

	@Override
	public String getName() {
		return this.getClass().getSimpleName() + "-" + sequenceNumber;
	}

}
