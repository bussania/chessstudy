/**
 * 
 */
package com.aquila.chess.player;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aquila.chess.Move;
import com.aquila.chess.Moves;
import com.aquila.chess.exception.EndOfGameException;
import com.aquila.chess.pieces.King;
import com.aquila.chess.pieces.Rook;

/**
 * @author bussa
 *
 */
public class GandiPlayer extends ChessPlayer {

	@SuppressWarnings("unused")
	static private Logger logger = LoggerFactory.getLogger(GandiPlayer.class);

	private Random rand;

	private final long seed;

	/**
	 * @param board the board on which this player will ... play
	 * @param color color of the partner
	 * @param seed  0 to use a random seed, otherwise we use the given seed
	 */
	public GandiPlayer(final long seed) {
		this.seed = seed;
		if (seed == 0)
			rand = new Random();
		else
			rand = new Random(seed);
	}

	@Override
	public Move nextPlay(final Move moveOpponent, final Moves moves) throws EndOfGameException {
		final Move moveCastling = moves.getCastlingMove();
		if (moveCastling != null)
			return moveCastling;
		List<Move> filteredMoves;
		filteredMoves = moves.//
				stream().//
				filter(move -> move.getCapturePiece() == null).//
				collect(Collectors.toList());

		if (filteredMoves.size() == 0)
			filteredMoves = moves;
//		else {
//			Optional<Move> filtered = moves.//
//					stream().//
//					filter(move -> move.getCapturePiece() != null).//
//					sorted(Comparator.comparingInt(move -> move.getCapturePiece().getPower())).//
//					
//			if (filtered.isPresent())
//				return filtered.get();
//		}
		
		final Move move= moves.getPossibleMoveWithout(rand, King.class, Rook.class);
		// logger.info("## Move: " + move.getPiece() + " -> " + move.getAlgebricNotation());
		return move;
	}

	@Override
	public String getName() {
		return this.getClass().getSimpleName() + ":" + seed;
	}

}
