/**
 * 
 */
package com.aquila.chess.player;

import java.util.Comparator;
import java.util.Optional;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aquila.chess.Move;
import com.aquila.chess.Moves;
import com.aquila.chess.exception.EndOfGameException;
import com.aquila.chess.pieces.King;
import com.aquila.chess.pieces.Rook;

/**
 * @author bussa
 *
 */
public class EaterPlayer extends ChessPlayer {

	@SuppressWarnings("unused")
	static private Logger logger = LoggerFactory.getLogger(EaterPlayer.class);

	private Random rand;

	private long seed;

	/**
	 * @param board the board on which this player will ... play
	 * @param color color of the partner
	 * @param seed  0 to use a random seed, otherwise we use the given seed
	 */
	public EaterPlayer(long seed) {
		this.seed = seed;
		if (seed == 0)
			rand = new Random();
		else
			rand = new Random(seed);
	}

	@Override
	public Move nextPlay(Move moveOpponent, Moves moves) throws EndOfGameException {
		Move moveCastling = moves.getCastlingMove();
		if (moveCastling != null)
			return moveCastling;
		Optional<Move> filtered = moves.//
				stream().//
				filter(move -> move.getCapturePiece() != null).//
				sorted(Comparator.comparingInt(move -> move.getCapturePiece().getPower())).//
				findFirst();
		if (filtered.isPresent())
			return filtered.get();
		Move move = moves.getPossibleMoveWithout(rand, King.class, Rook.class);
		return move;
	}

	@Override
	public String getName() {
		return this.getClass().getSimpleName() + ":" + seed;
	}

}
