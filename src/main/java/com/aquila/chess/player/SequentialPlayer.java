package com.aquila.chess.player;

import com.aquila.chess.Move;
import com.aquila.chess.Moves;
import com.aquila.chess.exception.ChessPositionException;
import com.aquila.chess.exception.EndOfGameException;

public class SequentialPlayer extends ChessPlayer {

	private Move nextMove;

	public SequentialPlayer() {
	}

	public void setNextMove(Move move) {
		this.nextMove = move;
	}

	@Override
	public Move nextPlay(Move moveOpponent, Moves moves) throws EndOfGameException, ChessPositionException {
		return nextMove;
	}

	@Override
	public String getName() {
		return "SimulationPlayer";
	}

}