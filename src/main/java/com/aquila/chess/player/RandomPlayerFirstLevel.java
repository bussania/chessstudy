/**
 * 
 */
package com.aquila.chess.player;

import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aquila.chess.Move;
import com.aquila.chess.Moves;
import com.aquila.chess.exception.ChessPositionException;
import com.aquila.chess.exception.EndOfGameException;
import com.aquila.chess.pieces.King;
import com.aquila.chess.pieces.Pawn;
import com.aquila.chess.pieces.Rook;

/**
 * @author bussa
 *
 */
public class RandomPlayerFirstLevel extends ChessPlayer {

	@SuppressWarnings("unused")
	static private Logger logger = LoggerFactory.getLogger(RandomPlayerFirstLevel.class);

	private Random rand;

	private final long seed;

	/**
	 * @param board the board on which this player will ... play
	 * @param color color of the partner
	 * @param seed  0 to use a random seed, otherwise we use the given seed
	 */
	public RandomPlayerFirstLevel(final long seed) {
		this.seed = seed;
		if (seed == 0)
			rand = new Random();
		else
			rand = new Random(seed);
	}

	@Override
	public Move nextPlay(final Move moveOpponent, final Moves moves) throws EndOfGameException, ChessPositionException {
		final Move moveCastling = moves.getCastlingMove();
		if (moveCastling != null)
			return moveCastling;
		Move moveRet = null;
		int scoreMax = 0;
		// chessmate
		for (final Move move : moves) {
			if (move.isCreateChessMate()) {
				return move;
			}
		}
		// promotion
		for (final Move move : moves) {
			if (move.isGettingPromoted()) {
				return move;
			}
		}
		for (final Move move : moves) {
			int score = 0;
			if (move.isCreateCheck() && move.getCanBeCaptured() == null) {
				score = 10;
				if (score > scoreMax) {
					moveRet = move;
					scoreMax = score;
				}
			}
			if (move.getCapturePiece() != null && move.getCanBeCaptured() == null
					&& score < move.getCapturePiece().getPower()) {
				score += move.getCapturePiece().getPower();
				if (score > scoreMax) {
					moveRet = move;
					scoreMax = score;
				}
			}
			if (move.getPiece().getClass() == Pawn.class && move.getCanBeCaptured() == null) {
				final int y = move.getEndLocation().getY();
				switch (this.getColor()) {
				case WHITE:
					if (y > 5)
						score += y;
					break;
				case BLACK:
					if (y < 3)
						score += y;
					break;
				}
				if (score > scoreMax) {
					moveRet = move;
					scoreMax = score;
				}
			}
		}
		if (scoreMax > 0 && moveRet != null)
			return moveRet;
		int step = 5;
		Move move = null;
		do {
			move = moves.getPossibleMoveWithout(rand, King.class, Rook.class);
			if (move.getCanBeCaptured() == null)
				return move;
		} while (step-- != 0);
		// logger.info("## Move: " + move.getPiece() + " -> " +
		// move.getAlgebricNotation());
		return move;
	}

	@Override
	public String getName() {
		return this.getClass().getSimpleName() + ":" + seed;
	}

}
