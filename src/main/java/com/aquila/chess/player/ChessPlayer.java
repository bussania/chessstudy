/**
 * 
 */
package com.aquila.chess.player;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aquila.chess.Board;
import com.aquila.chess.Castling;
import com.aquila.chess.Game;
import com.aquila.chess.Location;
import com.aquila.chess.Move;
import com.aquila.chess.Moves;
import com.aquila.chess.Piece;
import com.aquila.chess.Pieces;
import com.aquila.chess.exception.ChessIncoherencyError;
import com.aquila.chess.exception.ChessPositionException;
import com.aquila.chess.exception.EndOfGameException;
import com.aquila.chess.pieces.Bishop;
import com.aquila.chess.pieces.Color;
import com.aquila.chess.pieces.King;
import com.aquila.chess.pieces.Knight;
import com.aquila.chess.pieces.Pawn;
import com.aquila.chess.pieces.PawnPromotion;
import com.aquila.chess.pieces.Queen;
import com.aquila.chess.pieces.Rook;
import com.aquila.chess.player.mcts.ResultGame;

public abstract class ChessPlayer implements PawnPromotion {

	static private Logger logger = LoggerFactory.getLogger(ChessPlayer.class);
	@SuppressWarnings("unused")
	static private Logger logFile = LoggerFactory.getLogger("FILE");

	private Pieces pieces = new Pieces();

	private Pieces capturePieces = new Pieces();

	protected King king = null;

	protected Game game;

	protected Color color;

	private ChessPlayer opponent;

	private boolean possibleCastlingShort = false;

	private boolean possibleCastlingLong = false;
	private Move nextMove;

	public void init(final Game game, final Color color) {
		this.game = game;
		this.opponent = game.getPlayer(color.complement());
		if (opponent == null)
			throw new ChessIncoherencyError("Opponent not found for game: %s", game);
		this.color = color;
	}

	public boolean isPossibleCastling(Castling castling) {
		switch (castling) {
		case LONG:
			return possibleCastlingLong;
		case SHORT:
			return possibleCastlingShort;
		default:
			return false;
		}
	}

	public void setNextMove(Move move) {
		this.nextMove = move;
	}

	public void setNextMove(String moveSz) throws ChessPositionException {
		Move move = new Move(this.game.getBoard(), this.color, moveSz);
		setNextMove(move);
	}

	public Board getBoard() {
		return game.getBoard();
	}

	public Move play(Move moveOpponent, Moves moves) throws EndOfGameException, ChessPositionException {
		Move ret;
		if (this.nextMove != null) {
			ret = this.nextMove;
			this.nextMove = null;
		} else {
			ret = nextPlay(moveOpponent, moves);
		}
		return ret;
	}

	/**
	 * Return the next move, the move should be check before given
	 * 
	 * @param moveOpponent last move of the opponent
	 * @param moves        possible moves for the current player
	 * 
	 * @return give the next move to do
	 * @throws EndOfGameException
	 * @throws ChessPositionException
	 * @throws InterruptedException
	 */
	public abstract Move nextPlay(Move moveOpponent, Moves moves) throws EndOfGameException, ChessPositionException;

	public abstract String getName();

	public Piece getPromoted(final Piece piece) {
		return new Queen(piece.getColor(), piece.getLocation());
	}

	/**
	 * Add a piece in the board and update coordinate of the piece
	 * 
	 * @param piece    piece to place
	 */
	public void addPiece(final Piece piece) {
		this.getBoard().setPiece(piece.getX(), piece.getY(), piece);
		this.getPieces().add(piece);
		if (piece instanceof King) {
			if (this.king != null) {
				throw new ChessIncoherencyError("Try to create a new King ...");
			}
			this.king = (King) piece;
		}
	}

	public Pieces getPieces() {
		return this.pieces;
	}

	/**
	 * Add a piece in the board and update coordinate of the piece
	 * 
	 * @param piece          piece to place
	 * @param coordAlgebique coordinate in algebric form within the board ("A1"
	 *                       "A2"... "H8")
	 * @throws ChessPositionException
	 */
	public void addPiece(final Piece piece, final String coordAlgebique) throws ChessPositionException {
		this.getBoard().setPiece(coordAlgebique, piece);
		addPiece(piece);
	}

	public void addPieces(final Piece... pieces) {
		for (final Piece piece : pieces) {
			addPiece(piece);
		}
	}

	/**
	 * @param pieces - pieces
	 */
	public void addPieces(final String pieces) {
		for (final String pieceSz : pieces.split(",")) {
			if (pieceSz.length() == 3) {
				final String coordinates = pieceSz.substring(1);
				final char typePiece = pieceSz.charAt(0);
				Piece piece;
				try {
					switch (Character.toUpperCase(typePiece)) {
					case 'P':
						piece = new Pawn(getColor(), Location.get(coordinates));
						break;
					case 'B':
						piece = new Bishop(getColor(), Location.get(coordinates));
						break;
					case 'N':
						piece = new Knight(getColor(), Location.get(coordinates));
						break;
					case 'R':
						piece = new Rook(getColor(), Location.get(coordinates));
						break;
					case 'Q':
						piece = new Queen(getColor(), Location.get(coordinates));
						break;
					case 'K':
						piece = new King(getColor(), Location.get(coordinates));
						break;
					default:
						logger.warn(String.format("Piece %c not recognized", typePiece));
						continue;
					}
					this.addPieces(piece);
				} catch (final ChessPositionException e) {
					logger.warn(String.format("coordinates not valid: %s", coordinates), e);
					continue;
				}

			}
		}
	}

	/**
	 * Get possible Move for a piece, legal move that can not let the king behing
	 * captured by the opponent
	 * 
	 * @param retPossibleMoves
	 * @param piece
	 * @throws ChessPositionException
	 */
	public final void getPossibleLegalMoves(final Moves retPossibleMoves, final Piece piece)
			throws ChessPositionException {
		final Moves possibleRawMoves = piece.getPossibleRawMove(this.getBoard());
		for (final Move possibleRawMove : possibleRawMoves) {
			this.move(possibleRawMove, true, true);
			final Moves possibleOpponentRawMoves = this.game.getPlayer(color.complement()).getPossibleRawMoves();
			boolean add = true;
			for (final Move possibleOpponentMove : possibleOpponentRawMoves) {
				final Piece maybeKing = possibleOpponentMove.getCapturePiece();
				if (maybeKing != null && maybeKing == king) {
					add = false;
					break;
				}
			}
			if (add) {
				detectOpponentCapture(possibleRawMove, possibleOpponentRawMoves);
				for (final Move move : getPossibleRawMoves()) {
					if (move.canCaptureAKing()) {
						final Moves possibleOpponentLegalMovesStep1 = this.game.getPlayer(color.complement())
								.getPossibleLegalMoves();
						if (possibleOpponentLegalMovesStep1.size() == 0) {
							possibleRawMove.setCreateChessMate(true);
						}
						possibleRawMove.setCreateCheck(true);
					}
				}
				retPossibleMoves.add(possibleRawMove);
			}
			this.unmove(possibleRawMove);
		}
	}

	private final void detectOpponentCapture(final Move possibleRawMove, final Moves possibleOpponentRawMoves) {
		final Piece piece = possibleRawMove.getPiece();
		for (final Move possibleOpponentRawMove : possibleOpponentRawMoves) {
			final Piece capturePiece = possibleOpponentRawMove.getCapturePiece();
			if (capturePiece != null && capturePiece == piece) {
				possibleRawMove.setCanBeCaptured(possibleOpponentRawMove.getPiece());
			}
		}
	}

	/**
	 * Unmove the piece, undo the operation of move
	 * 
	 * @param move the move to Undo
	 * @throws ChessPositionException
	 */
	public final void unmove(final Move move) throws ChessPositionException {
		move.setDone(false);
		if (move.getCastling() != Castling.NONE) {
			unmoveCastling(move);
			return;
		}
		final Piece capturePiece = move.getCapturePiece();
		Piece piece = move.getPiece();
		if (capturePiece != null) {
			unCapture(move);
		} else {
			this.getBoard().setPiece(move.getEndLocation().getX(), move.getEndLocation().getY(), null);
		}
		this.getBoard().setPiece(move.getStartLocation().getX(), move.getStartLocation().getY(), piece);
		if (move.getPromotedPiece() != null) {
			this.getPieces().remove(move.getPromotedPiece());
			piece = new Pawn(getColor(), move.getStartLocation());
			this.getPieces().add(piece);
		} else {
			piece.set(move.getStartLocation());
		}
	}

	private void unmoveCastling(final Move move) throws ChessPositionException {
		final int y = color == Color.WHITE ? 0 : 7;
		Piece rook;
		switch (move.getCastling()) {
		case SHORT:
			rook = this.getBoard().getPiece(5, y);
			movePiece(this.getBoard().getPiece(6, y), king, Location.get(6, y), Location.get(4, y));
			movePiece(rook, rook, Location.get(5, y), Location.get(7, y));
			break;
		case LONG:
			rook = this.getBoard().getPiece(3, y);
			movePiece(this.getBoard().getPiece(2, y), king, Location.get(2, y), Location.get(4, y));
			movePiece(rook, rook, Location.get(3, y), Location.get(0, y));
			break;
		default:
			break;
		}
	}

	/**
	 * Move the given piece to the given position, the board and the piece will be
	 * touch. Captures pieces will be also updated.
	 * 
	 * @param move the move to Do
	 * @throws ChessPositionException
	 */
	public final void move(final Move move, final boolean simulation, final boolean retrievePossibleMove)
			throws ChessPositionException {
		move.setDone(true);
		if (move.getCastling() != Castling.NONE) {
			moveCastling(move);
			if (retrievePossibleMove == false)
				processCastlingCancelation(move);
			return;
		}
		final Piece capturePiece = move.getCapturePiece();
		if (capturePiece != null) {
			capture(move, simulation);
		}
		Piece optionalBoardPiece;
		if (move.getPromotedPiece() != null) {
			this.getPieces().remove(move.getPiece());
			optionalBoardPiece = move.getPromotedPiece();
			this.getPieces().add(optionalBoardPiece);
		} else {
			optionalBoardPiece = move.getPiece();
		}
		movePiece(optionalBoardPiece, move.getPiece(), move.getStartLocation(), move.getEndLocation());
		if (retrievePossibleMove == false)
			processCastlingCancelation(move);
	}

	private void movePiece(final Piece optionalBoardPiece, final Piece movePiece, final Location startLocation,
			final Location endLocation) {
		this.getBoard().setPiece(endLocation.getX(), endLocation.getY(), optionalBoardPiece);
		if (optionalBoardPiece == null) {
			logger.debug(this.toString());
			logger.error("Try to move piece: " + movePiece.toString());
			logger.error("Can not move without piece in the start location: " + startLocation);
			throw new RuntimeException(
					"Error when Trying to move piece: " + movePiece.toString() + " to " + endLocation);
		}
		optionalBoardPiece.set(endLocation);
		this.getBoard().setPiece(startLocation.getX(), startLocation.getY(), null);
	}

	private void moveCastling(final Move move) throws ChessPositionException {
		final int y = color == Color.WHITE ? 0 : 7;
		Piece rook;
		switch (move.getCastling()) {
		case SHORT:
			rook = this.getBoard().getPiece(7, y);
			movePiece(this.getBoard().getPiece(4, y), king, Location.get(4, y), Location.get(6, y));
			movePiece(rook, rook, Location.get(7, y), Location.get(5, y));
			break;
		case LONG:
			rook = this.getBoard().getPiece(0, y);
			movePiece(this.getBoard().getPiece(4, y), king, Location.get(4, y), Location.get(2, y));
			movePiece(rook, rook, Location.get(0, y), Location.get(3, y));
			break;
		default:
			break;
		}
	}

	@SuppressWarnings("incomplete-switch")
	private void processCastlingCancelation(final Move move) throws ChessPositionException {
		if (possibleCastlingShort || possibleCastlingLong) {
			if (move.getCastling() != Castling.NONE || move.getPiece().getClass() == King.class) {
				possibleCastlingShort = false;
				possibleCastlingLong = false;
			} else if (move.getPiece().getClass() == Rook.class) {
				switch (color) {
				case WHITE:
					if (move.getStartLocation() == Location.get(0, 0)) {
						possibleCastlingLong = false;
					} else if (move.getStartLocation() == Location.get(7, 0)) {
						possibleCastlingShort = false;
					}
					break;
				case BLACK:
					if (move.getStartLocation() == Location.get(0, 7)) {
						possibleCastlingLong = false;
					} else if (move.getStartLocation() == Location.get(7, 7)) {
						possibleCastlingShort = false;
					}
					break;
				}
			}
		}
	}

	public void processCastlingCancelationFromOpponentMove(Move move) throws ChessPositionException {
		final int y = color == Color.WHITE ? 0 : 7;
		Piece piece = move.getCapturePiece();
		if (this.possibleCastlingLong) {
			if (piece.getLocation() == Location.get(0, y))
				this.possibleCastlingLong = false;
		}
		if (this.possibleCastlingShort) {
			if (piece.getLocation() == Location.get(7, y))
				this.possibleCastlingShort = false;
		}
	}

	private void unCapture(final Move move) {
		final Piece capturePiece = move.getCapturePiece();
		if (this.capturePieces.remove(capturePiece) == false)
			throw new ChessIncoherencyError(
					"[UNCAPTURE] Cannot find capture piece: %s on list of capture pieces of player: %s", capturePiece,
					this);
		if (opponent.pieces.add(capturePiece) == false)
			throw new ChessIncoherencyError("[UNCAPTURE] Cannot add piece: %s on list of pieces of player: %s",
					capturePiece, opponent);
		this.getBoard().setPiece(move.getEndLocation().getX(), move.getEndLocation().getY(), capturePiece);
	}

	/**
	 * @param move
	 */
	private void capture(final Move move, final boolean simulation) {
		final Piece capturePiece = move.getCapturePiece();
		if (capturePiece instanceof King && !simulation)
			throw new ChessIncoherencyError("[CAPTURE] Move: %s King %s getCaptured !!", move, capturePiece);
		if (this.capturePieces.add(capturePiece) == false)
			throw new ChessIncoherencyError(
					"[CAPTURE] Cannot add capture piece: %s on list of capture pieces of player: %s", capturePiece,
					this);
		if (opponent.pieces.remove(capturePiece) == false) {
			logger.error(this.game.toPGN());
			throw new ChessIncoherencyError("[CAPTURE] Cannot find piece: %s on list of pieces of player: %s",
					capturePiece, opponent);
		}
		this.getBoard().setPiece(capturePiece.getX(), capturePiece.getY(), null);
	}

	/**
	 * Give all possible move for this player
	 * 
	 * @return all possible moves
	 * @throws ChessPositionException
	 */
	public Moves getPossibleRawMoves() throws ChessPositionException {
		final Moves possibleMoves = new Moves();
		final List<Piece> pieces = this.getPieces();
		for (Piece piece : pieces) {
			piece.getPossibleRawMove(possibleMoves, this.getBoard());
		}
		return possibleMoves;
	}

	/**
	 * Give all possible move for this player
	 * 
	 * @return all possible moves
	 * @throws ChessPositionException
	 */
	public final Moves getPossibleLegalMoves() throws ChessPositionException {
		final Moves possibleMoves = new Moves();
		@SuppressWarnings("unchecked")
		final List<Piece> pieces = (List<Piece>) this.pieces.clone();
		for (final Piece piece : pieces) {
			getPossibleLegalMoves(possibleMoves, piece);
		}
		addCastlingMoves(possibleMoves);
		possibleMoves.notateDoubleDestination();
		return possibleMoves;
	}

	public final void initCastlingPossibility() {
		final int y = color == Color.WHITE ? 0 : 7;
		this.possibleCastlingLong = false;
		this.possibleCastlingShort = false;
		if (getBoard().contains(0, y, Rook.class, color) && getBoard().contains(4, y, King.class, color))
			this.possibleCastlingLong = true;
		if (getBoard().contains(7, y, Rook.class, color) && getBoard().contains(4, y, King.class, color))
			this.possibleCastlingShort = true;
	}

	private final void addCastlingMoves(final Moves possibleMoves) throws ChessPositionException {
		if (this.possibleCastlingShort == false && this.possibleCastlingLong == false)
			return;
		final Moves possibleOpponentMoves = this.game.getPlayer(color.complement()).getPossibleRawMoves();
		if (this.possibleCastlingShort) {
			addShortCastlingMove(possibleMoves, possibleOpponentMoves);
		}
		if (this.possibleCastlingLong) {
			addLongCastlingMove(possibleMoves, possibleOpponentMoves);
		}
	}

	private void addShortCastlingMove(final Moves possibleMoves, final Moves possibleOpponentMoves)
			throws ChessPositionException {
		final int y = color == Color.WHITE ? 0 : 7;
		if (this.getBoard().getPiece(5, y) != null)
			return;
		if (this.getBoard().getPiece(6, y) != null)
			return;
		if (possibleOpponentMoves.canCapture(Location.get(4, y)))
			return;
		if (possibleOpponentMoves.canCapture(Location.get(5, y)))
			return;
		if (possibleOpponentMoves.canCapture(Location.get(6, y)))
			return;
		if (possibleOpponentMoves.canCapture(Location.get(7, y)))
			return;
		possibleMoves.add(new Move(king, Castling.SHORT));
	}

	private void addLongCastlingMove(final Moves possibleMoves, final Moves possibleOpponentMoves)
			throws ChessPositionException {
		final int y = color == Color.WHITE ? 0 : 7;
		if (this.getBoard().getPiece(1, y) != null)
			return;
		if (this.getBoard().getPiece(2, y) != null)
			return;
		if (this.getBoard().getPiece(3, y) != null)
			return;
		if (possibleOpponentMoves.canCapture(Location.get(0, y)))
			return;
		if (possibleOpponentMoves.canCapture(Location.get(1, y)))
			return;
		if (possibleOpponentMoves.canCapture(Location.get(2, y)))
			return;
		if (possibleOpponentMoves.canCapture(Location.get(3, y)))
			return;
		if (possibleOpponentMoves.canCapture(Location.get(4, y)))
			return;
		possibleMoves.add(new Move(king, Castling.LONG));
	}

	@Override
	public String toString() {
		final StringBuffer sb = new StringBuffer();
		sb.append(this.getName());
		sb.append("\n");
		sb.append(this.getColor());
		sb.append("\npossibleCastlingShort:" + this.possibleCastlingShort);
		sb.append("\npossibleCastlingLong:" + this.possibleCastlingLong);
		sb.append("\nPieces:");
		sb.append(pieces);
		sb.append("\n");
		sb.append("Capture Pieces:");
		sb.append(capturePieces);
		sb.append("\n");
		return sb.toString();
	}

	public final King getKing() {
		return this.king;
	}

	public final int nbPiecesOfType(final Class<? extends Piece> class1) {
		return this.pieces.nbPieceOfType(class1);
	}

	public final void check() {
		this.pieces.check(getBoard());
	}

	public void inheritProperties(final ChessPlayer chessPlayer, final ChessPlayer opponent, final Game game)
			throws ChessPositionException {
		this.game = game;
		this.opponent = opponent;
		this.possibleCastlingLong = chessPlayer.possibleCastlingLong;
		this.possibleCastlingShort = chessPlayer.possibleCastlingShort;
		this.color = chessPlayer.color;
		this.pieces = chessPlayer.pieces.copy(this, game.getBoard());
		this.capturePieces = chessPlayer.capturePieces.copy(this, game.getBoard());
		this.king = this.pieces.findKing();
	}

	public void installPieces() {
		final Board board = game.getBoard();
		for (final Piece piece : this.pieces) {
			board.setPiece(piece.getX(), piece.getY(), piece);
		}
	}

	public Color getColor() {
		return this.color;
	}

	public Game getGame() {
		return this.game;
	}

	public Pieces getCapturePieces() {
		return this.capturePieces;
	}

	@SuppressWarnings("incomplete-switch")
	public ResultGame getResultGame(final EndOfGameException e) throws Exception {
		ResultGame resultGame = null;

		switch (e.getTypeOfEnding()) {
		case REPEAT_50:
		case PAT:
		case REPETITION_X3:
		case NOT_ENOUGH_PIECES:
		case NB_MOVES_200:
			resultGame = new ResultGame(1, 1);
			break;
		case CHESSMATE:
			switch (e.getColor()) {
			case WHITE:
				resultGame = new ResultGame(1, 0);
				break;
			case BLACK:
				resultGame = new ResultGame(0, 1);
				break;
			}
			break;
		case UNKNOWN:
			logFile.error("Strange typeOfEnding: {}", e.getTypeOfEnding());
			throw new Exception(String.format("Strange typeOfEnding: %s", e.getTypeOfEnding()));
		}
		return resultGame;
	}
}
