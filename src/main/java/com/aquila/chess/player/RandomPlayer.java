/**
 * 
 */
package com.aquila.chess.player;

import java.util.List;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aquila.chess.Move;
import com.aquila.chess.Moves;
import com.aquila.chess.exception.ChessPositionException;
import com.aquila.chess.exception.EndOfGameException;
import com.aquila.chess.player.mcts.MCTSNode;

/**
 * @author bussa
 *
 */
public class RandomPlayer extends ChessPlayer {

	static private Logger logger = LoggerFactory.getLogger(RandomPlayer.class);

	private Random rand;

	private final long seed;

	private int nbStep = 0;

	private List<MCTSNode> nodesPath = null;

	/**
	 * @param board the board on which this player will ... play
	 * @param color color of the partner
	 * @param seed  0 to use a random seed, otherwise we use the given seed
	 */
	public RandomPlayer(final List<MCTSNode> nodesPath, final long seed) {
		this.nodesPath = nodesPath;
		this.seed = seed;
		if (seed == 0)
			rand = new Random();
		else
			rand = new Random(seed);
	}

	public RandomPlayer(final long seed) {
		this(null, seed);
	}

	@Override
	public Move nextPlay(final Move moveOpponent, final Moves moves) throws EndOfGameException, ChessPositionException {
		final Move moveCastling = moves.getCastlingMove();
		if (moveCastling != null)
			return moveCastling;
		Move move;
		if (this.nodesPath != null && nbStep+1 < this.nodesPath.size()) {
			move = new Move(this.nodesPath.get(nbStep++).getBasicMove(), this.game.getBoard());
		} else {
			move = moves.getPossibleMoveWithout(rand);
		}
		return move;
	}

	@Override
	public String getName() {
		return this.getClass().getSimpleName() + ":" + seed;
	}

}
