/**
 * 
 */
package com.aquila.chess.player;

import java.util.Scanner;

import com.aquila.chess.Move;
import com.aquila.chess.Moves;
import com.aquila.chess.exception.ChessPositionException;
import com.aquila.chess.exception.EndOfGameException;

/**
 * @author bussa
 *
 */
public class HumanPlayer extends ChessPlayer {

	@Override
	public Move nextPlay(Move moveOpponent, Moves moves) throws EndOfGameException, ChessPositionException {
		boolean isMoveCorrect = false;
		Scanner myObj = new Scanner(System.in);
		Move retMove = null;
		do {
			try {
				System.out.println("Opponent move:" + moveOpponent);
				System.out.println("Possible moves:");
				moves.stream().forEach(move -> System.out.print(move + ","));
				System.out.print("\nEnter the move (e2-e4): ");
				String moveSz = myObj.nextLine();
				retMove = new Move(getGame().getBoard(), getColor(), moveSz);
				boolean found = false;
				for (Move move : moves) {
					if (move.equals(retMove))
						found = true;
				}
				if (found == false) {
					String msg = String.format("Move: %s not found in the list of correct moves:\n%s", moveSz, moves);
					System.err.println(msg);
					continue;
				}
				isMoveCorrect = true;
			} catch (ChessPositionException e) {
				System.err.println("Error: " + e);
			}
		} while (isMoveCorrect == false);
		return retMove;
	}

	@Override
	public String getName() {
		return "HumanPlayer:" + this.getColor();
	}

}
