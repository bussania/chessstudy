package com.aquila.chess.player.mcts;

import com.aquila.chess.*;
import com.aquila.chess.exception.ChessPositionException;
import com.aquila.chess.pieces.*;
import com.aquila.chess.player.mcts.agz.DL4JAlphaGoZeroBuilder;
import com.aquila.chess.utils.Utils;
import org.deeplearning4j.nn.api.NeuralNetwork;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * <p>Network Input</p>
 * The input encoding follows the approach taken for AlphaZero.
 * The main difference is that the move count is no longer encoded — it is technically not required since it’s just some superfluous extra-information. We should
 * also mention that Leela Chess Zero is an ongoing project, and naturally improvements and code changes happen. The input format was subject to such changes
 * as well, for example to cope with chess variants such as Chess960 or Armageddon, or simply to experiment with encodings. The encoding described here is
 * the classic encoding, referred to in source code as INPUT_CLASSICAL_112_PLANE.
 * For those who want to look up things in code, the relevant source files are
 * lc0/src/neural/encoder.cc and lc0/src/neural/encoder_test.cc.
 * The input consists of 112 planes of size 8 × 8. Information w.r.t. the placement
 * of pieces is encoded from the perspective of the player whose current turn it
 * is. Assume that we take that player’s perspective. The first plane encodes
 * the position of our own pawns. The second plane encodes the position of our
 * knights, then our bishops, rooks, queens and finally the king. Starting from
 * plane 6 we encode the position of the enemy’s pawns, then knights, bishops,
 * rooks, queens and the enemy’s king. Plane 12 is set to all ones if one or more
 * repetitions occurred.
 * These 12 planes are repeated to encode not only the current position, but also
 * the seven previous ones. Planes 104 to 107 are set to 1 if White can castle
 * queenside, White can castle kingside, Black can castle queenside and Black can
 * 176 4. MODERN AI APPROACHES - A DEEP DIVE
 * castle kingside (in that order). Plane 108 is set to all ones if it is Black’s turn and
 * to 0 otherwise. Plane 109 encodes the number of moves where no capture has
 * been made and no pawn has been moved, i.e. the 50 moves rule. Plane 110 used
 * to be a move counter, but is simply set to always 0 in current generations of Lc0.
 * Last, plane 111 is set to all ones. This is, as previously mentioned, to help the
 * network detect the edge of the board when using convolutional filters.
 */
public class DeepLearningAGZ {

    static private Logger logger = LoggerFactory.getLogger(DeepLearningAGZ.class);
    static private Logger logFile = LoggerFactory.getLogger("FILE");

    static public final int MAX_POLICY_INDEX = 4672;

    static final int FIT_CHUNK = 50;

    INN nn;

    private double[][][] inputs;

    private final TrainGame trainGame = new TrainGame();

    public DeepLearningAGZ(final INN nn) {
        this.nn = nn;
    }

    public NeuralNetwork getNetwork() {
        return nn.getNetwork();
    }

    public void setUpdateLr(UpdateLr updateLr, int nbGames) {
        nn.setUpdateLr(updateLr, nbGames);
    }

    private final Map<Integer, OutputNN> cacheNN = new HashMap<>(4000);

    public void clearCache() {
        logFile.info("EMPTY cacheNNValues: {}", this.cacheNN.size());
        this.cacheNN.clear();
    }

    public void clearCache(Board board) {
        int key = board.hashCode();
        logFile.info("clear cache: {}", key);
        this.cacheNN.remove(key);
    }

    private Dirichlet dirichlet = game -> {
        return false;
    };

    public double getScore() {
        return nn.getScore();
    }

    public void setDirichlet(Dirichlet dirichlet) {
        this.dirichlet = dirichlet;
    }

    public void batchValue() {

    }

    private void clear() {
        this.trainGame.clear();
    }

    public void save() throws IOException {
        logger.info("SAVING NN ... (do not stop the jvm)");
        nn.save();
        logger.info("SAVE DONE");
    }

    @SuppressWarnings("incomplete-switch")
    private double[][][] processCastling(double[][][] nbIn, Move move) {
        int pieceIndex = 0;
        int y = 0;
        if (move.getColor() == Color.WHITE) {
            y = 0;
            pieceIndex = 0;
        } else {
            y = 7;
            pieceIndex = 6;
        }
        switch (move.getCastling()) {
            case SHORT:
                // king
                nbIn[pieceIndex + 5][6][y] = 1;
                nbIn[pieceIndex + 5][4][y] = 0;
                // rook
                nbIn[pieceIndex + 3][5][y] = 1;
                nbIn[pieceIndex + 3][7][y] = 0;
            case LONG:
                // king
                nbIn[pieceIndex + 5][2][y] = 1;
                nbIn[pieceIndex + 5][4][y] = 0;
                // rook
                nbIn[pieceIndex + 3][3][y] = 1;
                nbIn[pieceIndex + 3][0][y] = 0;
        }
        return nbIn;
    }

    private double[][][] processPromotedPiece(double[][][] nbIn, Move move) {
        Piece promotedPiece = move.getPromotedPiece();
        final Location end = move.getEndLocation();
        if (promotedPiece != null) {
            int promotedPieceIndex = getIndex(promotedPiece);
            nbIn[promotedPieceIndex][end.getX()][end.getY()] = 1;
        } else {
            int promotedPieceIndex = move.getColor() == Color.WHITE ? Board.QUEEN_INDEX : 6 + Board.QUEEN_INDEX;
            nbIn[promotedPieceIndex][end.getX()][end.getY()] = 1;
        }
        return nbIn;
    }

    /**
     * initialize value and policies for the current board pieces positions
     *
     * @param gameCopy   a clone of the current game
     * @param color2play the color of the player asking for values and policies
     * @param moves      list of legal moves
     * @param isRootNode     true if the MCTS search is on the root node
     * @return the length of the moves
     * @throws InterruptedException
     */
    public int initValueAndPolicies(final Game gameCopy, final Color color2play, final Moves moves, final boolean isRootNode) throws InterruptedException, ChessPositionException {
        int length = moves.size();
        final double[][][][] nbIn = new double[length][DL4JAlphaGoZeroBuilder.FEATURES_PLANES][Board.NB_COL][Board.NB_COL];
        int indexNbIn = 0;

        for (Move move : moves) {
            createInputs(nbIn[indexNbIn], trainGame, -1, gameCopy, color2play, move);
            indexNbIn++;
        }
        List<OutputNN> outputs = nn.outputs(nbIn, moves.size());
        int index = 0;
        for (Move move : moves) {
            int key = gameCopy.hashCode(move);
            double value = outputs.get(index).value;
            double[] policies = outputs.get(index).policies;
            int[] indexes = getIndexesFilteredPolicies(moves);
            boolean withDirichlet = false;
            if (this.dirichlet.update(gameCopy)) {
                withDirichlet = isRootNode && move == null;
            }
            double[] normalisedPolicies = Utils.toDistribution(policies, indexes, withDirichlet);
            cacheNN.put(key, new OutputNN(value, normalisedPolicies));
            index++;
        }
        return length;
    }

    public int initCache(Game game, Color color2play, Moves moves, final boolean isRootNode, MCTSSearch.Statistic statistic) {
        int key = game.hashCode();
        if (cacheNN.containsKey(key) == false) {
            try {
                initValueAndPolicies(game, color2play, moves, isRootNode);
                statistic.nbRetrieveNNValues++;
            } catch (InterruptedException e) {
                logFile.error("Error during loading of NN caches", e);
            } catch (ChessPositionException e) {
                logFile.error("Error during loading of NN caches", e);
            }
        } else statistic.nbRetrieveNNCachedValues++;
        return key;
    }

    public double getValue(Game game, Color color2play, Move move, Moves moves, final boolean isRootNode, MCTSSearch.Statistic statistic) {
        int key = initCache(game, color2play, moves, isRootNode, statistic);
        Double value = cacheNN.get(key).value;
        if (value != null) {
            return value;
        } else throw new RuntimeException("cache not valid for move: " + move + "game:\n" + game.toPGN());
    }

    public double getPolicy(Game game, Color color2play, Move move, Moves moves, final boolean isRootNode, MCTSSearch.Statistic statistic) {
        int key = initCache(game, color2play, moves, isRootNode, statistic);
        double[] policies = cacheNN.get(key).policies;
        if (policies != null) {
            int index = indexFromMove(move);
            double policy = policies[index];
            return policy;
        } else new RuntimeException("cache not valid for move: " + move + "game:\n" + game.toPGN());
        return 0;
    }

    /**
     * Convert moves into their corresponding array of index
     *
     * @param moves the list of move
     * @return - list of indexes using policies coding ([1 - 45XX])
     */
    private int[] getIndexesFilteredPolicies(Moves moves) {
        return moves.stream().filter((move) -> move != null).mapToInt((move) -> indexFromMove(move)).toArray();
    }

    static class BatchValue {
        final Game gameCopy;
        final Color color2play;
        final Moves moves;
        final boolean state0;

        BatchValue(final Game gameCopy, final Color color2play, final Moves moves, final boolean state0) {
            this.gameCopy = gameCopy;
            this.color2play = color2play;
            this.moves = moves;
            this.state0 = state0;
        }
    }

    /**
     * @param game - the board on which we apply the move
     * @param move - the move to apply
     * @return the normalize board for 1 position using board and move. dimensions:
     * [13][NB_COL][NB_COL]
     */
    @SuppressWarnings("incomplete-switch")
    public double[][][] createInputsForOnePosition(final Game game, final Move move) {
        final var nbIn = new double[12][Board.NB_COL][Board.NB_COL];

        for (int x = 0; x < Board.NB_COL; x++) {
            for (int y = 0; y < Board.NB_COL; y++) {
                final Piece optionalPiece = game.getBoard().getPiece(x, y);
                if (optionalPiece != null) {
                    final Piece piece = optionalPiece;
                    int pieceIndex = getIndex(piece);
                    nbIn[pieceIndex][x][y] = 1;
                }
            }
        }
        if (move != null) {
            if (move.getCastling() != null && move.getCastling() != Castling.NONE) {
                processCastling(nbIn, move);
            } else if (move.isGettingPromoted()) {
                processPromotedPiece(nbIn, move);
            } else {
                // Normal move
                final Location start = move.getStartLocation();
                final Location end = move.getEndLocation();
                Piece piece2move = move.getPiece();
                int piece2moveIndex = getIndex(piece2move);
                nbIn[piece2moveIndex][start.getX()][start.getY()] = 0;
                nbIn[piece2moveIndex][end.getX()][end.getY()] = 1;
            }
        }
        final var nbInNew = new double[13][Board.NB_COL][Board.NB_COL];
        // copy WHITE pieces as is (player view)
        for (int planes = 0; planes < 6; planes++) {
            for (int y = 0; y < Board.NB_COL; y++) {
                System.arraycopy(nbIn[planes][y], 0, nbInNew[planes][y], 0, Board.NB_COL);
            }
        }
        // copy flipped board for BLACK (player view)
        for (int planes = 6; planes < 11; planes++) {
            for (int y = 0; y < Board.NB_COL; y++) {
                for (int x = 0; x < Board.NB_COL; x++) {
                    nbInNew[planes][x][y] = nbIn[planes][Board.NB_COL - 1 - x][Board.NB_COL - 1 - y];
                }
            }
        }
        if (move != null && move.getCanBeCaptured() != null) {
            fill(nbInNew[12], game.IsRepetitedMove(move) ? 1.0 : 0.0);
        }
        return nbInNew;
    }

    /**
     * @formatter:off <pre>
     * [0-6]: Pawn:0, Bishop:1, Knight:2, Rook:3, Queen:4, King:5
     * [0-6] pieces for White
     * [7-12] pieces for Black
     * </pre>
     * @formatter:on
     */
    private int getIndex(Piece piece) {
        int index = piece.getColor().ordinal() * 6;
        if (piece instanceof Pawn) return index;
        if (piece instanceof Bishop) return index + Board.BISHOP_INDEX;
        if (piece instanceof Knight) return index + Board.KNIGHT_INDEX;
        if (piece instanceof Rook) return index + Board.ROOK_INDEX;
        if (piece instanceof Queen) return index + Board.QUEEN_INDEX;
        if (piece instanceof King) return index + Board.KING_INDEX;
        return -100; // sure this will failed at least
    }

    public String ToStringInputs(final double[][] nbIn) {
        final StringBuffer sb = new StringBuffer();
        for (int y = Board.NB_COL - 1; y >= 0; y--) {
            sb.append(String.format("[%3d] [%3d] [%3d] [%3d] [%3d] [%3d] [%3d] [%3d]\n", //
                    (int) nbIn[0][y], //
                    (int) nbIn[1][y], //
                    (int) nbIn[2][y], //
                    (int) nbIn[3][y], //
                    (int) nbIn[4][y], //
                    (int) nbIn[5][y], //
                    (int) nbIn[6][y], //
                    (int) nbIn[7][y])); //
        }
        return sb.toString();
    }

    private double getSignedValue(double value, Color color) {
        int sign = 0;
        switch (color) {
            case WHITE:
                sign = 1;
                break;
            case BLACK:
                sign = -1;
                break;
            case NOT_DEFINED:
                break;
        }
        return sign * value;
    }

    private void trainChunk(final int indexChunk, final int chunkSize, final TrainGame trainGame) {
        double[][][][] inputs = new double[chunkSize][DL4JAlphaGoZeroBuilder.FEATURES_PLANES][Board.NB_COL][Board.NB_COL];
        double[][] policies = new double[chunkSize][Board.NB_COL * Board.NB_COL * 73];
        double[][] values = new double[chunkSize][1];
        final AtomicInteger atomicInteger = new AtomicInteger();
        Double value = trainGame.value;
        List<InputList> inputsList = trainGame.inputList;
        for (int chunkNumber = 0; chunkNumber < chunkSize; chunkNumber++) {
            atomicInteger.set(chunkNumber);
            int gameRound = indexChunk * chunkSize + chunkNumber;
            InputList input = inputsList.get(gameRound);
            inputs[chunkNumber] = input.getInputs();
            Map<Integer, Double> probabilities = input.getProbabilities();
            double value2fit = getSignedValue(value, gameRound % 2 == 0 ? Color.WHITE : Color.BLACK);
            values[chunkNumber][0] = value2fit;
            if (probabilities != null) {
                if (value2fit > 0) {
                    probabilities.forEach((indexFromMove, probability) -> {
                        policies[atomicInteger.get()][indexFromMove] = probability;
                    });
                } else if (value2fit < 0) {
                    // complement the distribution of policies
                    double average = probabilities.values().stream().mapToDouble(Double::doubleValue).average().getAsDouble();
                    double doubleAverage = 2.0 * average;
                    probabilities.forEach((indexFromMove, probability) -> {
                        policies[atomicInteger.get()][indexFromMove] = doubleAverage - probability;
                    });
//                } else {
//                    double average = probabilities.values().stream().mapToDouble(Double::doubleValue).average().getAsDouble();
//                    double policy = average; // 0.5; // total / probabilities.values().size();
//                    probabilities.forEach((indexFromMove, childrenVisits) -> {
//                        policies[atomicInteger.get()][indexFromMove] = policy;
//                    });
                }
            }
        }
        logFile.info("NETWORK FIT[{}]: {}", chunkSize, value);
        nn.fit(inputs, policies, values);
    }

    /**
     * <h1>Network Input</h1>
     * <p>
     * The input encoding follows the approach taken for AlphaZero.
     * The main difference is that the move count is no longer encoded — it is
     * technically not required since it’s just some superfluous extra-information. We should
     * also mention that Leela Chess Zero is an ongoing project, and naturally improvements
     * and code changes happen. The input format was subject to such changes
     * as well, for example to cope with chess variants such as Chess960 or Armageddon, or
     * simply to experiment with encodings. The encoding described here is
     * the classic encoding, referred to in source code as INPUT_CLASSICAL_112_PLANE.
     * For those who want to look up things in code, the relevant source files are
     * lc0/src/neural/encoder.cc and lc0/src/neural/encoder_test.cc.
     * The input consists of 112 planes of size 8 × 8. Information w.r.t. the placement
     * of pieces is encoded from the perspective of the player whose current turn it
     * is. Assume that we take that player’s perspective. The first plane encodes
     * the position of our own pawns. The second plane encodes the position of our
     * knights, then our bishops, rooks, queens and finally the king. Starting from
     * plane 6 we encode the position of the enemy’s pawns, then knights, bishops,
     * rooks, queens and the enemy’s king. Plane 12 is set to all ones if one or more
     * repetitions occurred.
     * These 12 planes are repeated to encode not only the current position, but also
     * the seven previous ones. Planes 104 to 107 are set to 1 if White can castle
     * queenside, White can castle kingside, Black can castle queenside and Black can
     * castle kingside (in that order). Plane 108 is set to all ones if it is Black’s turn and
     * to 0 otherwise. Plane 109 encodes the number of moves where no capture has
     * been made and no pawn has been moved, i.e. the 50 moves rule. Plane 110 used
     * to be a move counter, but is simply set to always 0 in current generations of Lc0.
     * Last, plane 111 is set to all ones. This is, as previously mentioned, to help the
     * network detect the edge of the board when using convolutional filters.
     * </p>
     * <ul>
     * <li>0 - 103: 8 times 13 -> 104 planes:
     * <ul>
     * <li> 0: positions for white pawn</li>
     * <li> 1: positions for white knights</li>
     * <li> 2: positions for white bishops</li>
     * <li> 3: positions for white rooks</li>
     * <li> 4: positions for white queens</li>
     * <li> 5: positions for white king</li>
     * <li> 6: positions for black pawn</li>
     * <li> 7: positions for black knights</li>
     * <li> 8: positions for black bishops</li>
     * <li> 9: positions for black rooks</li>
     * <li>10: positions for black queens</li>
     * <li>11: positions for black king</li>
     * <li>12: 1 if One or more repetition, what does it mean ?</li>
     * </ul>
     * </li>
     * <li>104: White can castle queenside (LONG)</li>
     * <li>105: White can castle kingside (SHORT)</li>
     * <li>106: Black can castle queenside (LONG)</li>
     * <li>107: Black can castle kingside (SHORT)</li>
     * <li>108: set to all ones if it is Black’s turn and to 0 otherwise</li>
     * <li>109: encodes the number of moves where no capture has been made and no pawn has been moved</li>
     * <li>110: move counter, seems 0 from now on</li>
     * <li>111: all ones, to help the network detect the edge of the board when using convolutional filters</li>
     * </ul>
     *
     * @param move : null if we are training
     */
    // FIXME: remove last boolean isTraining
    public void createInputs(final double[][][] inputs, TrainGame trainGame, int gameRound, final Game game, final Color color, final Move move) throws ChessPositionException {
        List<InputList> inputsList = trainGame.inputList;
        if (gameRound == -1) gameRound = inputsList.size();
        double[][][] oneBoardInputs;
        oneBoardInputs = createInputsForOnePosition(game, move);
        System.arraycopy(oneBoardInputs, 0, inputs, 0, 13);
        for (int i = 1; i < 7; i++) {
            int inputsIndice = gameRound - i;
            if (inputsIndice >= 0 && inputsIndice < inputsList.size()) {
                oneBoardInputs = inputsList.get(inputsIndice).inputs;
                System.arraycopy(oneBoardInputs, 0, inputs, 12 * i, 13);
            }
        }
        Game gameCopy = game;
        Moves moveWhites = gameCopy.getPlayer(Color.WHITE).getPossibleLegalMoves();
        fill(inputs[104], moveWhites.stream().filter(move1 -> move1.getCastling() == Castling.LONG).count() > 0 ? 1.0 : 0.0);
        fill(inputs[105], moveWhites.stream().filter(move1 -> move1.getCastling() == Castling.SHORT).count() > 0 ? 1.0 : 0.0);
        Moves moveBlacks = gameCopy.getPlayer(Color.BLACK).getPossibleLegalMoves();
        fill(inputs[106], moveBlacks.stream().filter(move1 -> move1.getCastling() == Castling.LONG).count() > 0 ? 1.0 : 0.0);
        fill(inputs[107], moveBlacks.stream().filter(move1 -> move1.getCastling() == Castling.SHORT).count() > 0 ? 1.0 : 0.0);
        fill(inputs[108], color == Color.BLACK ? 1.0 : 0.0);
        inputs[109][0][0] = gameCopy.getNbMovesFor50End();
        fill(inputs[111], 1.0);
    }

    private void fill(double planes[][], double value) {
        if (value == 0.0) return;
        for (int i = 0; i < 8; i++) {
            Arrays.fill(planes[i], (double) value);
        }
    }

    public void save(final ResultGame resultGame, int numGame) throws IOException {
        TrainGame trainGame = new TrainGame();
        trainGame.save(numGame, resultGame);
    }

    public void train(final TrainGame trainGame) throws IOException {
        final int nbStep = trainGame.inputList.size();
        // the first result correspond to WHITE Player

        logFile.info("NETWORK TO FIT[{}]: {}", nbStep, trainGame.value);
        int nbChunk = nbStep / FIT_CHUNK;
        int restChunk = nbStep % FIT_CHUNK;
        for (int indexChunk = 0; indexChunk < nbChunk; indexChunk++) {
            trainChunk(indexChunk, FIT_CHUNK, trainGame);
        }
        if (restChunk > 0) {
            trainChunk(nbChunk, restChunk, trainGame);
        }
        logFile.info("NETWORK score: {}", nn.getScore());
        // TODO fit with invert inputs, but starting when inputs could be revert
//		trainSymetrique(resultGame);
        clearCache();
    }

    /**
     * @param trainGame
     * @param numGame
     * @param game
     * @throws IOException
     */
    @Deprecated
    public void train(final TrainGame trainGame, final int numGame, final Game game) throws IOException {
        final int nbStep = trainGame.inputList.size();
        // the first result correspond to WHITE Player

        logFile.info("NETWORK TO FIT[{}]: {}", nbStep, trainGame.value);
        int nbChunk = nbStep / FIT_CHUNK;
        int restChunk = nbStep % FIT_CHUNK;
        for (int indexChunk = 0; indexChunk < nbChunk; indexChunk++) {
            trainChunk(indexChunk, FIT_CHUNK, trainGame);
        }
        if (restChunk > 0) {
            trainChunk(nbChunk, restChunk, trainGame);
        }
        // TODO fit with invert inputs, but starting when inputs could be revert
//		trainSymetrique(resultGame);
        clearCache();
        logger.info("SAVING Neuronal Network ... (do not stop the jvm)");
        save();
        logger.info("SAVE DONE");
        nn.updateLr(numGame);
    }

    public void saveBatch(ResultGame resultGame, int numGames) throws IOException {
        logger.info("SAVING Batch ... (do not stop the jvm)");
        logger.info("Result: {}   Game size: {} inputsList(s)", resultGame.value, trainGame.inputList.size());
        trainGame.save(numGames, resultGame);
        logger.info("SAVE DONE");
        clear();
    }

    @SuppressWarnings("unused")
    private int countPawn(double[][][] inputs) {
        int ret = 0;
        for (int y = 0; y < Board.NB_COL; y++) {
            for (int x = 0; x < Board.NB_COL; x++) {
                double power = inputs[0][x][y];
                if (power == 1 || power == 254) ret++;
            }
        }
        return ret;
    }

    @FunctionalInterface
    static interface Selection {
        boolean select(float score, float limit);
    }

    /**
     * @param x           [0 .. 7] coordinate x of the piece to move
     * @param y           [0 .. 7] coordinate Y of the piece to move
     * @param nbStep      [1..7] number of step (absolute)
     * @param orientation orientation of the move starting with index 0 ->
     *                    [N,NE,E,SE,S,SW,W,NW]
     * @return
     */
    private int indexFromQueenMove(int x, int y, int nbStep, int orientation) {
        return to1D(x, Board.NB_COL, y, Board.NB_COL, nbStep + orientation * Board.NB_COL);
    }

    /**
     * @param x          [0..7] coordinate x of the knight to move
     * @param y          [0..7] coordinate Y of the knight to move
     * @param knightMove [0..7] [Up+Up+Left,Up+Up+Right,Right+Right+Up,
     *                   Right+Right+Down, Down+Down+Right,
     *                   Down+Down+Left,Left+Left+Down,Left+Left+Up]
     * @return
     */
    private int indexFromKnightMove(int x, int y, int knightMove) {
        return to1D(x, Board.NB_COL, y, Board.NB_COL, 56 + knightMove);
    }

    private int to1D(int x, int xMax, int y, int yMax, int z) {
        return (x * xMax * yMax) + (y * xMax) + z;
    }

    public int indexFromMove(BasicMove basicMove, Piece piece) {
        int startX = basicMove.getStartX();
        int startY = basicMove.getStartY();
        int endX = basicMove.getEndX();
        int endY = basicMove.getEndY();
        return indexFromMove(startX, startY, endX, endY, piece);
    }

    public int indexFromMove(Move move) {
        int startX = move.getStartLocation().getX();
        int startY = move.getStartLocation().getY();
        int endX = move.getEndLocation().getX();
        int endY = move.getEndLocation().getY();
        return indexFromMove(startX, startY, endX, endY, move.getPiece());
    }

    /**
     * @return
     * @formatter:off <pre>
     *   out: 8x8x73: [0..72]  ->     [0..55)(Queen moves: nbStep + orientation) [56..63](Knights moves) [64..72](underpromotion)
     * Queen moves: [1 .. 7] ->     7 number of steps  [N,NE,E,SE,S,SW,W,NW]: 8 orientation -> 7*8
     * Knight moves: [0..7]  ->     [Up+Up+Left,Up+Up+Right,Right+Right+Up, Right+Right+Down,
     * Down+Down+Right, Down+Down+Left,Left+Left+Down,Left+Left+Up]
     * UnderPromotion:
     * </pre>
     * @formatter:on
     */
    private int indexFromMove(int startX, int startY, int endX, int endY, Piece piece) {
        int ret = 0;
        int deltaX = endX - startX;
        int nbStepX = Math.abs(deltaX);
        deltaX = deltaX < 0 ? 0 : deltaX > 0 ? 2 : 1;
        int deltaY = endY - startY;
        int nbStepY = Math.abs(deltaY);
        int nbStep = Math.max(nbStepX, nbStepY);
        deltaY = deltaY < 0 ? 100 : deltaY > 0 ? 300 : 200;
        if (piece instanceof Knight) {
            switch (deltaX + deltaY) {
                case 302: // NE
                    if (nbStepY > nbStepX) ret = indexFromKnightMove(startX, startY, 0);
                    else ret = indexFromKnightMove(startX, startY, 1);
                    break;
                case 300: // SE
                    if (nbStepY > nbStepX) ret = indexFromKnightMove(startX, startY, 2);
                    else ret = indexFromKnightMove(startX, startY, 3);
                    break;
                case 100: // SW
                    if (nbStepY > nbStepX) ret = indexFromKnightMove(startX, startY, 4);
                    else ret = indexFromKnightMove(startX, startY, 5);
                    break;
                case 102: // NW
                    if (nbStepY > nbStepX) ret = indexFromKnightMove(startX, startY, 6);
                    else ret = indexFromKnightMove(startX, startY, 7);
                    break;
            }
        } else {
            switch (deltaX + deltaY) {
                case 202:
                    ret = indexFromQueenMove(startX, startY, nbStep, 0);
                    break;
                case 302:
                    ret = indexFromQueenMove(startX, startY, nbStep, 1);
                    break;
                case 301:
                    ret = indexFromQueenMove(startX, startY, nbStep, 2);
                    break;
                case 300:
                    ret = indexFromQueenMove(startX, startY, nbStep, 3);
                    break;
                case 200:
                    ret = indexFromQueenMove(startX, startY, nbStep, 4);
                    break;
                case 100:
                    ret = indexFromQueenMove(startX, startY, nbStep, 5);
                    break;
                case 101:
                    ret = indexFromQueenMove(startX, startY, nbStep, 6);
                    break;
                case 102:
                    ret = indexFromQueenMove(startX, startY, nbStep, 7);
                    break;
            }
        }
        if (ret > MAX_POLICY_INDEX) {
            logFile.info("(startX:{} startY:{}) -> (endX:{} endY:{})", startX, startY, endX, endY);
            logFile.info("indexFromMove nbStep: {} ret:{}", nbStep, ret);
        }
        return ret;
    }

    static public class VirtualChild {
        public VirtualChild(double policy) {
            this.policy = policy;
        }

        double policy;
        MCTSNode node = null;
    }

    public void storeInputs(final Game game) throws ChessPositionException {
        double[][][] inputs = new double[DL4JAlphaGoZeroBuilder.FEATURES_PLANES][Board.NB_COL][Board.NB_COL];
        createInputs(inputs, trainGame, game.getNbStep(), game, game.getColorToPlay(), null);
        this.inputs = inputs;
    }

    /**
     * @param game       the game to store
     * @param playerRoot the rootNode of the last MCTS Search
     */
    public void commitInputs(final Game game, MCTSNode playerRoot) {
        if (playerRoot != null && playerRoot.getVisits() > 0) {
            // convert MCTSNode::getBasicMove into the index in move probabilities
            final Map<Integer, Double> probabilities = new HashMap<>();
            for (int i = 0; i < MAX_POLICY_INDEX; i++) {
                probabilities.put(i, 0.0);
            }
            playerRoot.childNodes.values().forEach(node -> {
                int index = indexFromMove(node.getBasicMove(), node.getPiece());
                double probability = (double) node.getVisits() / (double) playerRoot.getVisits();
                probabilities.put(index, probability);
            });
            InputList inputList = new InputList(inputs, game, probabilities);
            trainGame.add(inputList);
            this.inputs = null;
        } else {
            logFile.error("Error in commitInputs: playerRoot is null, parent: {}", playerRoot);
        }
    }

    public String getFilename() {
        return nn.getFilename();
    }

}
