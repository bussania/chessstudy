package com.aquila.chess.player.mcts;

import com.aquila.chess.Game;
import com.aquila.chess.Move;
import com.aquila.chess.Moves;
import com.aquila.chess.exception.ChessPositionException;
import com.aquila.chess.exception.EndOfGameException;
import com.aquila.chess.pieces.Color;
import com.aquila.chess.player.ChessPlayer;

public class FixPlayer extends ChessPlayer {

	private Move nextMove;

	public FixPlayer() {
	}

	public FixPlayer(Game game, Color color) {
		super.init(game, color);
	}

	@Override
	public void setNextMove(Move move) {
		this.nextMove = move;
	}

	@Override
	public Move nextPlay(Move moveOpponent, Moves moves) throws EndOfGameException, ChessPositionException {
		return nextMove;
	}

	@Override
	public String getName() {
		return "SimulationPlayer";
	}

}