package com.aquila.chess.player.mcts;

import com.aquila.chess.player.mcts.agz.DL4JAlphaGoZeroBuilder;
import com.aquila.chess.player.mcts.agz.DualResnetModel;
import org.deeplearning4j.nn.api.NeuralNetwork;
import org.deeplearning4j.nn.conf.CacheMode;
import org.deeplearning4j.nn.graph.ComputationGraph;
import org.deeplearning4j.optimize.listeners.PerformanceListener;
import org.nd4j.jita.allocator.enums.AllocationStatus;
import org.nd4j.jita.conf.Configuration;
import org.nd4j.jita.conf.CudaEnvironment;
import org.nd4j.linalg.api.buffer.DataType;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class NN implements INN {

    static private final Logger logger = LoggerFactory.getLogger(NN.class);
    static private final Logger logFile = LoggerFactory.getLogger("FILE");
    private final String filename;
    public int NUM_RESIDUAL_BLOCKS = 20;
    public int NUM_FEATURE_PLANES = DL4JAlphaGoZeroBuilder.FEATURES_PLANES;
    private UpdateLr updateLr;

    private ComputationGraph network;

    public NN(final String filename, final boolean loadUpdater, final int nbGames) {
        CudaEnvironment.getInstance().getConfiguration()
                // key option enabled
                .allowMultiGPU(false) //
                .setFirstMemory(AllocationStatus.DEVICE) //
                .setMaximumDeviceMemoryUsed(0.80) //
                .setMemoryModel(Configuration.MemoryModel.DELAYED) //
                // cross-device access is used for faster model averaging over pcie
                .allowCrossDeviceAccess(false) //
                .setMaximumDeviceCacheableLength(6L * 1024 * 1024 * 1024L) //
                .setMaximumDeviceCache(6L * 1024 * 1024 * 1024L) //
                .setMaximumHostCacheableLength(6L * 1024 * 1024 * 1024L) //
                .setMaximumHostCache(6L * 1024 * 1024 * 1024L); //
        CudaEnvironment.getInstance().getConfiguration().setVerbose(true);
        CudaEnvironment.getInstance().getConfiguration().setAllocationModel(Configuration.AllocationModel.CACHE_ALL);
        logFile.info("getMaximumDeviceCache: {}", CudaEnvironment.getInstance().getConfiguration().getMaximumDeviceCache());
        Nd4j.setDefaultDataTypes(DataType.FLOAT, DataType.FLOAT);
        this.filename = filename;
        try {
            network = load(loadUpdater);
        } catch (final IOException e) {
            logger.error(String.format("Exception when trying to load [%s], creating a default", filename), e);
        }
        if (network == null) {
            network = DualResnetModel.getModel(NUM_RESIDUAL_BLOCKS, NUM_FEATURE_PLANES);
        }
        setUpdateLr(UpdateLr.createDefault(), nbGames);
        network.setListeners(new PerformanceListener(1));
        network.setCacheMode(CacheMode.DEVICE);
    }

    @Override
    public void reset() {

    }

    @Override
    public double getScore() {
        return network.score();
    }

    @Override
    public void setUpdateLr(UpdateLr updateLr, int nbGames) {
        this.updateLr = updateLr;
        this.updateLr(nbGames);
    }

    @Override
    public void updateLr(int nbGames) {
        double lr = updateLr.update(nbGames);
        logFile.info("[{}] Setting learning rate: {}", nbGames, lr);
        this.setLR(lr);
        logFile.info("[{}] Getting learning rate: {}", nbGames, this.getLR());
    }

    private ComputationGraph load(final boolean loadUpdater) throws IOException {
        final File file = new File(filename);
        if (file.canRead() == false) return null;
        final ComputationGraph ret = ComputationGraph.load(file, loadUpdater);
        logger.info("LOADED ComputationGraph");
        return ret;
    }

    @Override
    public void save() throws IOException {
        final File file = new File(filename);
        this.network.save(file);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Score: ");
        sb.append(network.score());
        return sb.toString();
    }

    @Override
    public void fit(final double[][][][] inputs, final double[][] policies, final double[][] values) {
        if (this.network.score() == Double.NaN) throw new RuntimeException("network broken !!");
        INDArray[] inputsArray = new INDArray[]{Nd4j.create(inputs)};
        INDArray[] labelsArray = new INDArray[]{Nd4j.create(policies), Nd4j.create(values)};
        network.fit(inputsArray, labelsArray);
    }

    /**
     * @return the filename
     */
    @Override
    public String getFilename() {
        return filename;
    }

    @Override
    public double getLR() {
        return this.network.getLearningRate("policy_head_conv_");
    }

    @Override
    public void setLR(double lr) {
        this.network.setLearningRate(lr);
    }

    @Override
    public List<OutputNN> outputs(double[][][][] nbIn, final int len) {
        List<OutputNN> ret = new ArrayList<>();
        INDArray[] outputs = output(nbIn);
        for (int i = 0; i < len; i++) {
            double value = outputs[1].getColumn(0).getDouble(i);
            double[] policies = outputs[0].getRow(i).toDoubleVector();
            ret.add(new OutputNN(value, policies));
        }
        return ret;
    }

    @Override
    public NeuralNetwork getNetwork() {
        return this.network;
    }


    private INDArray[] output(double[][][][] nbIn) {
        INDArray inputsArray = Nd4j.create(nbIn);
        return network.output(inputsArray);

    }
}
