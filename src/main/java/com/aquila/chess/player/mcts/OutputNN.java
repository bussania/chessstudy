package com.aquila.chess.player.mcts;

public class OutputNN {
    double value;
    double[] policies;

    public OutputNN(double value, double[] policies) {
        this.value = value;
        this.policies = policies;
    }
}
