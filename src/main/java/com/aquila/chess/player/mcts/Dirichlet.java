package com.aquila.chess.player.mcts;

import com.aquila.chess.Game;

/**
 *
 */
@FunctionalInterface
public interface Dirichlet {

    boolean update(Game game);

    /**
     * Create a default UpdateLr from alphazero settings
     *
     * @return
     */
    public static Dirichlet createDefault() {
        return new Dirichlet() {
            @Override
            public boolean update(Game game) {
                return true;
            }
        };
    }
}