package com.aquila.chess.player.mcts;

public class ResultGame {
	public ResultGame(final int whiteWin, final int blackWin) {
		if (whiteWin == 1 && blackWin == 1)
			value = 0.0;
		else if (whiteWin == 1)
			value = 1.0; // 1
		else if (blackWin == 1)
			value = -1.0; // -1
	}

	public double value;
}
