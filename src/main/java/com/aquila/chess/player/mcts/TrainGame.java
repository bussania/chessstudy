package com.aquila.chess.player.mcts;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;

public class TrainGame implements Serializable {
    static {
        Path path = Paths.get("train/");

        //java.nio.file.Files;
        try {
            Files.createDirectories(path);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    Double value = null;
    List<InputList> inputList = new LinkedList<>();

    public TrainGame() {
    }

    public static TrainGame load(int num) throws IOException, ClassNotFoundException {
        FileInputStream fileInputStream
                = new FileInputStream("train/" + num);
        ObjectInputStream objectInputStream
                = new ObjectInputStream(fileInputStream);
        TrainGame ret = (TrainGame) objectInputStream.readObject();
        objectInputStream.close();
        return ret;
    }

    public void save(int num, final ResultGame resultGame) throws IOException {
        this.value = resultGame.value;
        FileOutputStream fileOutputStream
                = new FileOutputStream("train/" + num);
        ObjectOutputStream objectOutputStream
                = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(this);
        objectOutputStream.flush();
        objectOutputStream.close();
    }

    public void add(InputList inputList) {
        this.inputList.add(inputList);
    }

    public void clear() {
        this.inputList.clear();
        value = null;
    }
}
