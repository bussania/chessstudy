package com.aquila.chess.player.mcts;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aquila.chess.Move;
import com.aquila.chess.Moves;
import com.aquila.chess.Piece;
import com.aquila.chess.exception.ChessPositionException;
import com.aquila.chess.pieces.Color;

public class MCTSNode {

	@SuppressWarnings("unused")
	static private Logger logger = LoggerFactory.getLogger(MCTSNode.class);
	@SuppressWarnings("unused")
	static private Logger logFile = LoggerFactory.getLogger("FILE");

	private final BasicMove basicMove;

	private Piece piece;

	private double expectedReward = 0.0;
	private double value;
	private double sumValues = 0.0;

	private int visits = 0;

	Map<Integer, MCTSNode> childNodes = new HashMap<>();

	State state = State.INTERMEDIATE;

	MCTSNode parent;

	private boolean chessmate = false;
	private Color color;
	private double policy;

	public MCTSNode(final Move move, double value, double policy) {
		this.policy = policy;
		this.value = value;
		this.setExpectedReward(value);
		if (move != null) {
			this.color = move.getColor();
			try {
				this.basicMove = new BasicMove(move);
			} catch (ChessPositionException e) {
				e.printStackTrace();
				throw new RuntimeException(e);
			}
			this.piece = move.getPiece();
		} else {
			this.basicMove = null;
		}
	}

	public void setState(State state) {
		this.state = state;
	}

	public static enum State {
		INTERMEDIATE, WIN, LOOSE, PAT, REPETITION_X3, REPEAT_50, NOT_ENOUGH_PIECES, NB_MOVES_200
	}

	public State getState() {
		return state;
	}

	public Color getColor() {
		return this.color;
	}

	public List<MCTSNode> getChilds(final Moves currentMoves) {
		return this.childNodes.values().stream().filter(node -> {
			try {
				return currentMoves.find(node.basicMove) != null;
			} catch (ChessPositionException e) {
				e.printStackTrace();
				return false;
			}
		}).collect(Collectors.toList());
	}

	/**
	 * @return the childNodes
	 */
	public List<MCTSNode> getChilds() {
		return childNodes.values().stream().collect(Collectors.toList());
	}

	public MCTSNode findChild(final Move move) {
		if (move == null)
			return null;
		return this.childNodes.get(move.hashCode());
	}

	public void addChild(final MCTSNode node) {
		this.childNodes.put(node.basicMove.hashCode(), node);
		node.setParent(this);
	}

	@Override
	public String toString() {
		return String.format("MCTS -> Move: %s visit:%d expectedReward:%e parent:%b childs:%d state:%s", //
				this.basicMove, //
				this.visits, //
				this.expectedReward, //
				this.parent != null, //
				this.childNodes.size(), this.getState());
	}

	/**
	 * equals method working with MCTSNode and Move
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (obj instanceof Move)
			try {
				return equals((Move) obj);
			} catch (ChessPositionException e) {
				e.printStackTrace();
				return false;
			}
		return equals((MCTSNode) obj);
	}

	/**
	 * @param move
	 * @return
	 * @throws ChessPositionException
	 */
	public boolean equals(final Move move) throws ChessPositionException {
		if (move == null) {
			return this.basicMove == null;
		}
		if (basicMove == null)
			return false;
		return this.basicMove.equals(move);
	}

	public boolean equals(final MCTSNode mctsNode) {
		if (basicMove == null) {
			return mctsNode.basicMove == null;
		}
		if (basicMove == null)
			return false;
		return basicMove.equals(mctsNode.basicMove);
	}

	public int getNumberAllSubNodes() {
		int subNode = 1;
		for (final MCTSNode node : this.childNodes.values()) {
			subNode += node.getNumberAllSubNodes();
		}
		return subNode;
	}

	public void setChessMate(final boolean chessmate) {
		this.chessmate = chessmate;
	}

	/**
	 * @return the chessmate
	 */
	public boolean isChessmate() {
		return chessmate;
	}

	/**
	 * @return the parent
	 */
	public MCTSNode getParent() {
		return parent;
	}

	/**
	 * @param parent the parent to set
	 */
	public void setParent(MCTSNode parent) {
		this.parent = parent;
	}

	/**
	 * @return the basicMove
	 */
	public BasicMove getBasicMove() {
		return basicMove;
	}

	/**
	 * @return the piece
	 */
	public Piece getPiece() {
		return piece;
	}

	/**
	 * @param chessmate the chessmate to set
	 */
	public void setChessmate(boolean chessmate) {
		this.chessmate = chessmate;
	}

	/**
	 * @return the valueSum
	 */
	public double getValueSum() {
		return sumValues;
	}

	public int getSumChildVisits() {
		return this.childNodes.values().stream().mapToInt(node -> node.visits).sum();
	}

	public double getSumChildRewards() {
		return this.childNodes.values().stream().mapToDouble(node -> node.expectedReward).sum();
	}

	public double getPolicy() {
		return policy;
	}

	public double getExpectedReward() {
		return this.expectedReward;
	}

	public int getVisits() {
		return this.visits;
	}

	public MCTSNode incVisits() {
		this.visits++;
		return this;
	}

	public void setExpectedReward(double expectedReward) {
		this.expectedReward = expectedReward;
	}

	/**
	 * @return the value
	 */
	public double getValue() {
		return value;
	}

	public void addSumValues(double value) {
		this.sumValues += value;
	}

	public double getSumValues() {
		return this.sumValues;
	}

	public void resetVisits() {
		this.visits = 0;
	}

	public void resetChilds() {
		this.childNodes.clear();
	}

}