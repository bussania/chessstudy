package com.aquila.chess.player.mcts;

import org.deeplearning4j.nn.api.NeuralNetwork;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static com.aquila.chess.player.mcts.DeepLearningAGZ.MAX_POLICY_INDEX;

public class NNSimul implements INN {
    static private final double valueRangeMin = -0.500;
    static private final double valueRangeMax = 0.500;
    static private final double policyRangeMin = 0.100;
    static private final double policyRangeMax = 0.900;
    private final Random randomGenerator = new Random();
    private double mediumValue;
    private double mediumPolicies;

    public NNSimul() {
        reset();
    }

    @Override
    public void reset() {
        mediumValue = valueRangeMin + (valueRangeMax - valueRangeMin) * randomGenerator.nextDouble();
        mediumPolicies = policyRangeMin + (policyRangeMax - policyRangeMin) * randomGenerator.nextDouble();

    }

    @Override
    public List<OutputNN> outputs(double[][][][] nbIn, int len) {
        List<OutputNN> ret = new ArrayList<>();
        for (int i = 0; i < len; i++) {
            double value = mediumValue + (-0.01 + (0.01 + 0.01) * randomGenerator.nextDouble());
            double[] policies = new double[MAX_POLICY_INDEX];
            for (int policyIndex = 0; policyIndex < MAX_POLICY_INDEX; policyIndex++) {
                policies[policyIndex] = mediumPolicies + (-0.01 + (0.01 + 0.01) * randomGenerator.nextDouble());
            }
            ret.add(new OutputNN(value, policies));
        }
        return ret;
    }

    @Override
    public double getScore() {
        return 0;
    }

    @Override
    public void setUpdateLr(UpdateLr updateLr, int nbGames) {

    }

    @Override
    public void updateLr(int nbGames) {

    }

    @Override
    public void save() throws IOException {

    }

    @Override
    public void fit(double[][][][] inputs, double[][] policies, double[][] values) {
        throw new RuntimeException("fit not allow with this implementation");
    }

    @Override
    public String getFilename() {
        return null;
    }

    @Override
    public double getLR() {
        return 0;
    }

    @Override
    public void setLR(double lr) {

    }

    @Override
    public NeuralNetwork getNetwork() {
        return null;
    }

}
