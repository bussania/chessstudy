package com.aquila.chess.player.mcts;

import com.aquila.chess.Castling;
import com.aquila.chess.Game;
import com.aquila.chess.pieces.Color;

import java.io.Serializable;
import java.util.Map;

class InputList implements Serializable {
    /**
     * Board inputs from 1 position -> double[12][][]
     */
    double[][][] inputs;

    private Map<Integer, Double> probabilities;

    /**
     * @param inputs
     * @param game
     * @param move
     * @param probabilities
     */
    public InputList(double[][][] inputs, final Game game, Map<Integer, Double> probabilities) {
        super();
        this.inputs = inputs;
        this.probabilities = probabilities;
    }

    public double[][][] getInputs() {
        return inputs;
    }

    public Map<Integer, Double> getProbabilities() {
        return probabilities;
    }


}
