package com.aquila.chess.player.mcts;

import com.aquila.chess.Move;
import com.aquila.chess.Moves;
import com.aquila.chess.exception.ChessPositionException;
import com.aquila.chess.exception.EndOfGameException;
import com.aquila.chess.player.ChessPlayer;
import com.aquila.chess.utils.DotGenerator;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.deeplearning4j.nn.api.NeuralNetwork;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;
import java.util.Random;

public class MCTSPlayer extends ChessPlayer {

    static private final Logger logger = LoggerFactory.getLogger(MCTSPlayer.class);
    static private final Logger logFile = LoggerFactory.getLogger("FILE");

    static int[] totalResults = new int[3];

    private int nbStep = 0;

    private final MCTSNode root = new MCTSNode(null, 0.0, 0.0);

    private MCTSNode currentPlay = root;

    private final long timeMillisPerStep;
    private long nbMaxSearchCalls = -1;

    private Random rand;

    private DeepLearningAGZ deepLearning;

    private final boolean partner;

    private UpdateCpuct updateCpuct;

    public MCTSPlayer(final DeepLearningAGZ deepLearning, final boolean partner, final long seed,
                      UpdateCpuct updateCpuct, final long timeMillisPerStep) {
        super();
        if (seed == 0)
            rand = new Random();
        else
            rand = new Random(seed);
        this.deepLearning = deepLearning;
        this.timeMillisPerStep = timeMillisPerStep;
        this.partner = partner;
        this.updateCpuct = updateCpuct;
    }

    public MCTSPlayer withNbMaxSearchCalls(long nbMaxSearchCalls) {
        this.nbMaxSearchCalls = nbMaxSearchCalls;
        return this;
    }

    /**
     * @throws IOException
     * @deprecated
     */
    @Deprecated
    public void save() throws IOException {
        this.deepLearning.save();
        System.gc();
    }

    public void init() {
        currentPlay = root;
    }

    @Override
    public Move nextPlay(final Move moveOpponent, final Moves moves) throws EndOfGameException, ChessPositionException {
        for (Move move : moves) {
            if (move.isCreateChessMate()) {
                logFile.info("\n########################################### CHESSMATE FOR " + this.getColor()
                        + " DETECTED #####################");
                logFile.info("move: {}", move);
                this.nbStep++;
                return move;
            }
        }
        Move move;
        move = mctsStep(moveOpponent, moves); // moveOpponent == null);
        this.nbStep++;
        if (this.nbStep % 20 == 0) {
            deepLearning.clearCache();
        }
        return move;
    }

    @Override
    public String getName() {
        return String.format("[%s] %S DL:%s Nodes:%d", color, this.getClass().getSimpleName(),
                this.deepLearning.getFilename(), root.getNumberAllSubNodes());
    }

    private Move mctsStep(final Move opponentMove, final Moves currentMoves)
            throws ChessPositionException, EndOfGameException {
        MCTSNode parent = this.currentPlay;
        MCTSNode node;
        if (opponentMove != null) {
            node = parent.findChild(opponentMove);
            if (node == null) {
                node = new MCTSNode(opponentMove, 0.0, 0.0);
                parent.addChild(node);
            }
            parent = node;
        } else {
            logFile.warn("calling mctsStep with opponentMove: null");
        }
        MCTSSearch mctsSearch = new MCTSSearch(this, this.deepLearning, parent, game, this.getColor(), this.updateCpuct,
                rand);
        final long startTime = System.currentTimeMillis();
        long nbNumberSearchCalls = mctsSearch.search(this.nbStep, startTime, this.timeMillisPerStep,
                this.nbMaxSearchCalls);
        final long endTime = System.currentTimeMillis();
        final long length = endTime > startTime ? endTime - startTime : Long.MIN_VALUE;
        final long speed = (nbNumberSearchCalls * 1000) / length;
        MCTSNode bestNode = mctsSearch.findBest();
        if (this.game.getLogBoard()) {
            logFile.warn("[{}] STATS: {}", this.getColor(), mctsSearch.getStatistic().toString());
            logFile.warn("[{}] nbSearch calls:{} - term:{} ms - speed:{} calls/s", this.getColor(), nbNumberSearchCalls,
                    length, speed);
        }
        Move ret = currentMoves.find(bestNode.getBasicMove());
        if (ret == null) {
            logFile.warn(
                    "##########################################################################################################");
            logFile.warn("[{}] ALARM: currentMoves: {}", this.getColor(), currentMoves.toString());
            logFile.warn("[{}] opponentMove: {}", this.getColor(), opponentMove);
            logFile.warn("[{}] bestNode: {}", this.getColor(), bestNode);
            logFile.warn(DotGenerator.toString(parent.getParent(), 4));
            logFile.warn("[{}] Game:\n{}", this.getColor(), game.toPGN());
            ret = mctsSearch.getRandomMove(currentMoves);
            parent.addChild(new MCTSNode(ret, 0.0, 0.0));
            logFile.warn("[{}] choosing randomly: {}", this.getColor(), ret);
            logFile.warn(
                    "##########################################################################################################");
        }
        this.currentPlay = parent.findChild(ret);
        return ret;
    }

    @SuppressWarnings("unused")
    private MCTSNode getRandom(final List<MCTSNode> nodes, final Random rand) {
        final int index = rand.nextInt(nodes.size());
        return nodes.get(index);
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer();
        sb.append(getName());
        return sb.toString();
    }

    /**
     * @return the root
     */
    public MCTSNode getRoot() {
        return root;
    }

    public DeepLearningAGZ getDeepLearning() {
        return this.deepLearning;
    }

    public NeuralNetwork getNetwork() {
        return this.deepLearning.getNetwork();
    }

    /**
     * Fit the NN with the current score
     *
     * @param e
     * @return
     * @throws Exception
     */
    @SuppressWarnings("incomplete-switch")
    @Deprecated
    public ResultGame storeResultGame(final EndOfGameException e, int numGames) throws Exception {
        ResultGame resultGame = super.getResultGame(e);
        this.deepLearning.save(resultGame, numGames);
        return resultGame;
    }

    public int[] getTotalResults() {
        return totalResults;
    }

    public void setNetwork(DeepLearningAGZ deepLearning) {
        logFile.info("reloading network: {}",
                ToStringBuilder.reflectionToString(deepLearning, ToStringStyle.MULTI_LINE_STYLE));
        this.deepLearning = deepLearning;
    }

}
