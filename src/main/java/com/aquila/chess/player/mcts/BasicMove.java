/**
 *
 */
package com.aquila.chess.player.mcts;

import com.aquila.chess.Castling;
import com.aquila.chess.Location;
import com.aquila.chess.Move;
import com.aquila.chess.Piece;
import com.aquila.chess.exception.ChessPositionException;
import com.aquila.chess.pieces.Color;
import com.aquila.chess.utils.Utils;

/**
 * @author bussa
 *
 */
public class BasicMove {
    private Piece piece = null;
    private Location start;
    private Location end;

    private Castling castling = Castling.NONE;

    /**
     * @param startX
     * @param startY
     * @param endX
     * @param endY
     * @throws ChessPositionException
     */
    public BasicMove(final int startX, final int startY, final int endX, final int endY, final Piece piece) throws ChessPositionException {
        init(startX, startY, endX, endY);
        this.piece = piece;
    }

    public BasicMove(final Move move) throws ChessPositionException {
        this.piece = move.getPiece();
        int y = move.getColor() == Color.WHITE ? 0 : 7;
        this.castling = move.getCastling();
        switch (move.getCastling()) {
            case NONE:
                init(move.getStartLocation(), move.getEndLocation());
                break;
            case LONG:
                init(4, y, 2, y);
                break;
            case SHORT:
                init(4, y, 6, y);
                break;
        }
    }

    private void init(final int startX, final int startY, final int endX, final int endY)
            throws ChessPositionException {
        this.start = Location.get(startX, startY);
        this.end = Location.get(endX, endY);
    }

    private void init(final Location start, final Location end) {
        this.start = start;
        this.end = end;
    }

    @Override
    public int hashCode() {
        return this.start.moveHashcode(this.end) | (this.piece.hashCode() << 16);
    }

    @Override
    public String toString() {
        switch (this.getCastling()) {
            case SHORT:
                return "O-O";
            case LONG:
                return "O-O-O";
            case NONE:
                return String.format("%s-%s", Utils.coordAlgebrique(start.getX(), start.getY()), Utils.coordAlgebrique(end.getX(), end.getY()));
        }
        return "???";
    }

    public Castling getCastling() {
        return this.castling;
    }

    public boolean equals(final Move move) throws ChessPositionException {
        switch (move.getCastling()) {
            case NONE:
                if (move.getStartLocation() != this.start)
                    return false;
                if (move.getEndLocation() != this.end)
                    return false;
                return true;
            default:
                return move.getCastling() == this.getCastling();
        }
    }

    /**
     * work with Move and BasicMove
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (obj instanceof Move) {
            final Move move = (Move) obj;
            try {
                return this.equals(move);
            } catch (ChessPositionException e) {
                e.printStackTrace();
                return false;
            }
        }
        final BasicMove other = (BasicMove) obj;
        if (start != other.start) return false;
        if (end != other.end) return false;
        return true;
    }

    public int getStartX() {
        return this.start.getX();
    }

    public int getStartY() {
        return this.start.getY();
    }

    public int getEndX() {
        return this.end.getX();
    }

    public int getEndY() {
        return this.end.getY();
    }

    public void setPiece(final Piece piece) {
        this.piece = piece;
    }
}
