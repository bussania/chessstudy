/**
 *
 */
package com.aquila.chess.player.mcts;

import com.aquila.chess.Game;
import com.aquila.chess.Move;
import com.aquila.chess.Moves;
import com.aquila.chess.exception.ChessPositionException;
import com.aquila.chess.exception.EndOfGameException;
import com.aquila.chess.pieces.Color;
import com.aquila.chess.player.ChessPlayer;
import com.aquila.chess.player.mcts.MCTSNode.State;
import com.aquila.chess.utils.DotGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * @author bussa
 */
public class MCTSSearch {

    public static class Statistic {
        public int nbCalls;
        public int nbPlay;
        public int nbPossibleMoves;
        public int nbRetrieveNNCachedValues;
        public int nbRetrieveNNValues;
        public int nbRetrieveNNCachedPolicies;

        public void clear() {
            nbCalls = 0;
            nbPlay = 0;
            nbPossibleMoves = 0;
            nbRetrieveNNCachedValues = 0;
            nbRetrieveNNCachedPolicies = 0;
            nbRetrieveNNValues = 0;
        }

        @Override
        public String toString() {
            return String.format("calls:%d play:%d possibleMovesCalls:%d NNCached:%d NNPolicies:%d NNretrieved:%d",
                    nbCalls, nbPlay, nbPossibleMoves, nbRetrieveNNCachedValues, nbRetrieveNNCachedPolicies,
                    nbRetrieveNNValues);
        }
    }

    private final Statistic statistic = new Statistic();

    private static final double WIN_LOOSE_VALUE = 1;
    static private final Logger logger = LoggerFactory.getLogger(MCTSSearch.class);
    static private final Logger logFile = LoggerFactory.getLogger("FILE");

    DeepLearningAGZ deepLearning;
    private final FixPlayer whitePlayer;
    private final FixPlayer blackPlayer;
    private Game gameCopy;

    /**
     * this hyperparameter control the exploration inside the system. 1 means no
     * exploration
     */
    private double cpuct;

    private final MCTSNode parent;
    private final Game gameOriginal;
    private final Color color;
    private int nbStep;
    private final UpdateCpuct updateCpuct;

    private final Random rand;

    /**
     * @param player
     * @param deepLearning
     * @param parent
     * @param game
     * @param color
     * @param updateCpuct
     * @param rand
     * @throws ChessPositionException
     */
    public MCTSSearch(MCTSPlayer player, DeepLearningAGZ deepLearning, MCTSNode parent, Game game, Color color,
                      UpdateCpuct updateCpuct, Random rand) throws ChessPositionException {
        this.parent = parent;
        this.deepLearning = deepLearning;
        this.whitePlayer = new FixPlayer(game, Color.WHITE);
        this.blackPlayer = new FixPlayer(game, Color.BLACK);
        this.gameOriginal = game;
        this.color = color;
        this.updateCpuct = updateCpuct;
        this.rand = rand;
    }

    /**
     * @param nbStep
     * @param startTime
     * @param timeMillisPerStep
     * @param nbMaxSearchCalls
     * @return number of search calls
     * @throws ChessPositionException
     * @throws InterruptedException
     */
    public long search(int nbStep, long startTime, long timeMillisPerStep, long nbMaxSearchCalls)
            throws ChessPositionException {
        this.nbStep = nbStep;
        getStatistic().clear();
        cpuct = updateCpuct.update(nbStep);
        long nbNumberSearchCalls = 0;
        deepLearning.clearCache(this.gameOriginal.getBoard());
        do {
            gameCopy = gameOriginal.copy(whitePlayer, blackPlayer);
            gameCopy.setColorToPlay(color);
            Moves selectNodesMoves = this.getPossibleMoves(gameCopy);
            search(parent, selectNodesMoves, 0, nbNumberSearchCalls == 0);
            if (timeMillisPerStep > 0) {
                long endTime = System.currentTimeMillis();
                if (endTime - startTime >= timeMillisPerStep)
                    break;
            }
            if (nbNumberSearchCalls % 50 == 0) System.out.print(".");
            nbNumberSearchCalls++;
            if (nbMaxSearchCalls > 0 && nbNumberSearchCalls >= nbMaxSearchCalls)
                break;
        } while (true);
        getStatistic().nbCalls += nbNumberSearchCalls;
        return nbNumberSearchCalls;
    }

    private double endOfGame(MCTSNode node, Game gameCopy, ChessPlayer player, ChessPlayer opponentPlayer, int depth)
            throws ChessPositionException {
        try {
            gameCopy.isEndOfGame(player, opponentPlayer, null);
        } catch (EndOfGameException e) {
            return returnEndOfSimulatedGame(node, depth, player.getColor(), e);
        }
        throw new ChessPositionException("This game should be ending: no possible move for player: " + player);
    }

    private double search(MCTSNode opponentNode, Moves moves, int depth, final boolean isRootNode) throws ChessPositionException {
        FixPlayer player = (FixPlayer) gameCopy.getPlayer(gameCopy.getColorToPlay());
        FixPlayer opponentPlayer = (FixPlayer) gameCopy.getPlayer(gameCopy.getColorToPlay().complement());
        if (moves.isEmpty()) {
            // opponentNode.incVisits();
            return endOfGame(opponentNode, gameCopy, player, opponentPlayer, depth);
        }
        Color simulatedPlayerColor = gameCopy.getColorToPlay();
        // selection
        moves.add(0, null); // for policy - last index: policies for the current board
        Move selectedMove = selection(player, opponentNode, moves, isRootNode);
        // expansion
        MCTSNode selectedNode = opponentNode.findChild(selectedMove);
        if (selectedNode == null) {
            selectedNode = expansion(player, opponentNode, selectedMove, moves, isRootNode, depth);
            double ret = selectedNode.getExpectedReward();
            return ret;
        }
        if (selectedNode.getState() != State.INTERMEDIATE) {
            selectedNode.incVisits();
            double ret = selectedNode.getExpectedReward();
            opponentNode.addSumValues(ret);
            opponentNode.incVisits();
            double qSA = opponentNode.getSumValues() / (opponentNode.getVisits());
            opponentNode.setExpectedReward(qSA);
            return ret;
        }
        // evaluate
        try {
            player.setNextMove(selectedMove);
            gameCopy.play(true);
            getStatistic().nbPlay++;
        } catch (EndOfGameException e) {
            // detect chessmate or drawn
            selectedNode.incVisits();
            double ret = returnEndOfSimulatedGame(selectedNode, depth, simulatedPlayerColor, e);
            opponentNode.addSumValues(ret);
            opponentNode.incVisits();
            double qSA = opponentNode.getSumValues() / (opponentNode.getVisits());
            opponentNode.setExpectedReward(qSA);
            return ret;
        }
        // recursive calls
        Moves selectNodesMoves = this.getPossibleMoves(gameCopy);
        double value = search(selectedNode, selectNodesMoves, depth + 1, false);
        // retro-propagate
        opponentNode.addSumValues(value);
        // Q[s][a] = (N[s][a]*Q[s][a] + v)/(N[s][a]+1)
        //		double qSA = ((opponentNode.getVisits()) * opponentNode.getExpectedReward() + value)
        //				/ (1 + opponentNode.getVisits());
        opponentNode.incVisits();
        double qSA = opponentNode.getSumValues() / (opponentNode.getVisits());
        opponentNode.setExpectedReward(qSA);
        return qSA;
    }

    private Move selection(FixPlayer player, MCTSNode opponentNode, Moves moves, final boolean isRootNode)
            throws ChessPositionException {
        double maxUcb = Double.NEGATIVE_INFINITY;
        double policy;
        Moves bestMoves = new Moves();
        int sumVisits = parent.getVisits();

        if (moves.size() == 1)
            return null;
        deepLearning.initCache(gameCopy, player.getColor(), moves, isRootNode, getStatistic());
        for (Move possibleMove : moves) {
            if (possibleMove == null)
                continue;
            if (possibleMove.isCreateChessMate()) {
                return possibleMove;
            }
            MCTSNode child = opponentNode.findChild(possibleMove);
            double exploitation;
            double exploration = 0.0;
            int visits = 0;
            if (child != null) {
                exploitation = child.getExpectedReward();
                visits = child.getVisits();
            } else {
                exploitation = deepLearning.getValue(gameCopy, player.getColor(), possibleMove, moves, isRootNode, getStatistic());
            }
            if (sumVisits > 0) {
                policy = deepLearning.getPolicy(gameCopy, player.getColor(), possibleMove, moves, isRootNode, getStatistic());
                exploration = policy * cpuct * (Math.sqrt(sumVisits) / (1 + visits));
            }
            double ucb = exploitation + exploration;
            if (ucb > maxUcb) {
                maxUcb = ucb;
                bestMoves.clear();
                bestMoves.add(possibleMove);
            } else if (ucb == maxUcb) {
                bestMoves.add(possibleMove);
            }
        }
        int nbBestMoves = bestMoves.size();
        Move bestMove = null;
        if (nbBestMoves == 1) {
            bestMove = bestMoves.get(0);
        } else if (nbBestMoves > 1) {
            bestMove = getRandomMove(bestMoves);
        }
        return bestMove;
    }

    private Moves getPossibleMoves(Game game) throws ChessPositionException {
        getStatistic().nbPossibleMoves++;
        ChessPlayer player = game.getPlayer(game.getColorToPlay());
        return player.getPossibleLegalMoves();
    }

    private MCTSNode expansion(FixPlayer player, MCTSNode opponentNode, Move selectedMove, Moves moves, boolean isRootNode, int depth) {
        double value = deepLearning.getValue(gameCopy, player.getColor(), selectedMove, moves, isRootNode, getStatistic());
        double policyValue = deepLearning.getPolicy(gameCopy, player.getColor(), selectedMove, moves, isRootNode, getStatistic());
        MCTSNode node = new MCTSNode(selectedMove, value, policyValue); // .incVisits();
        opponentNode.addChild(node);
        if (opponentNode.getState() != State.INTERMEDIATE) {
            logger.warn("#{} [{} - {}] value:{} New Node:{} parent:{}", depth, color, player.getColor(), value,
                    selectedMove, opponentNode);
        }
        return node;
    }

    @Deprecated
    public MCTSNode findBestOld() {
        if (this.gameOriginal.getLogBoard()) {
            logFile.warn("[{}] FINDBEST [nb search: {}] avec MCTS: {}", this.color, this.parent);
            logFile.warn("[{}] FINDBEST: {}", this.color, DotGenerator.toString(parent, 5));
        }

        double maxExpectedReward = Double.NEGATIVE_INFINITY;
        List<MCTSNode> bestNodes = new ArrayList<>();
        for (MCTSNode mctsNode : parent.childNodes.values()) {
            if (mctsNode.getExpectedReward() > maxExpectedReward) {
                maxExpectedReward = mctsNode.getExpectedReward();
                bestNodes.clear();
                bestNodes.add(mctsNode);
            } else if (mctsNode.getExpectedReward() == maxExpectedReward) {
                bestNodes.add(mctsNode);
            }
        }
        double maxVisits = Double.NEGATIVE_INFINITY;
        List<MCTSNode> restBestNodes = new ArrayList<>();
        for (MCTSNode mctsNode : bestNodes) {
            if (mctsNode.getVisits() > maxVisits) {
                maxVisits = mctsNode.getVisits();
                restBestNodes.clear();
                restBestNodes.add(mctsNode);
            } else if (mctsNode.getVisits() == maxVisits) {
                restBestNodes.add(mctsNode);
            }
        }
        int nbBest = restBestNodes.size();
        MCTSNode ret;
        if (nbBest > 1) {
            ret = getRandomMove(restBestNodes, gameOriginal);
        } else
            ret = restBestNodes.get(0);
        String state = "MEDIUM";
        int nbRewards = parent.childNodes.size();
        if (nbBest == 1 && nbRewards >= 1)
            state = "GOOD";
        else if (nbBest == nbRewards)
            state = "BAD";
        float percentGood = (nbBest * 100) / nbRewards;
        if (this.gameOriginal.getLogBoard()) {
            logFile.info(
                    "[{}] State: {}:{}% Step:{} nbRewards:{} nbBestReward:{} maxReward:{} Piece:{} RetNode:{} cpuct:{}",
                    color, state, percentGood, nbStep, nbRewards, nbBest, maxExpectedReward, ret.getPiece(), ret, cpuct);
        }
        return ret;
    }

    public MCTSNode findBest() {
        if (this.gameOriginal.getLogBoard()) {
            logFile.warn("[{}] FINDBEST [nb search: {}] avec MCTS: {}", this.color, this.parent);
            logFile.warn("[{}] FINDBEST: {}", this.color, DotGenerator.toString(parent, 5));
        }

        double maxVisits = Double.NEGATIVE_INFINITY;
        List<MCTSNode> restBestNodes = new ArrayList<>();
        for (MCTSNode mctsNode : parent.childNodes.values()) {
            if (mctsNode.getVisits() > maxVisits) {
                maxVisits = mctsNode.getVisits();
                restBestNodes.clear();
                restBestNodes.add(mctsNode);
            } else if (mctsNode.getVisits() == maxVisits) {
                restBestNodes.add(mctsNode);
            }
        }
        int nbBest = restBestNodes.size();
        MCTSNode ret;
        if (nbBest > 1) {
            ret = getRandomMove(restBestNodes, gameOriginal);
        } else
            ret = restBestNodes.get(0);
        String state = "MEDIUM";
        int nbRewards = parent.childNodes.size();
        if (nbBest == 1 && nbRewards >= 1)
            state = "GOOD";
        else if (nbBest == nbRewards)
            state = "BAD";
        float percentGood = (nbBest * 100) / nbRewards;
        if (this.gameOriginal.getLogBoard()) {
            logFile.info(
                    "[{}] State: {}:{}% Step:{} nbRewards:{} nbBestReward:{} maxReward:{} Piece:{} RetNode:{} cpuct:{}",
                    color, state, percentGood, nbStep, nbRewards, nbBest, ret.getExpectedReward(), ret.getPiece(), ret, cpuct);
        }
        return ret;
    }

    public MCTSNode findBest(int nbFirstBests) {
        List<MCTSNode> bestNodes = parent.childNodes.values().stream().sorted((node1, node2) -> {
            double reward1 = node1.getExpectedReward();
            double reward2 = node2.getExpectedReward();
            if (reward1 == reward2)
                return 0;
            return (reward1 > reward2 ? -1 : 1);
        }).limit(nbFirstBests).collect(Collectors.toList());
        return getRandomMove(bestNodes, gameOriginal);
    }

    public double returnEndOfSimulatedGame(MCTSNode node, int depth, Color simulatedPlayerColor, EndOfGameException e) {
        switch (e.getTypeOfEnding()) {
            case CHESSMATE:
                if (e.getColor() == color) {
                    if (node.getState() != State.WIN) {
                        String sequence = sequenceMoves(node);
                        logFile.warn("#{} [{} - {}] moves:{} CURRENT COLOR WIN", depth, color, simulatedPlayerColor,
                                sequence);
                        node.setState(State.WIN);
                        node.setExpectedReward(WIN_LOOSE_VALUE);
                    }
                    return WIN_LOOSE_VALUE;
                } else {
                    if (node.getState() != State.LOOSE) {
                        String sequence = sequenceMoves(node);
                        logFile.warn("#{} [{} - {}] move:{} CURRENT COLOR LOOSE", depth, color, simulatedPlayerColor,
                                sequence);
                        node.setState(State.LOOSE);
                        node.setExpectedReward(-WIN_LOOSE_VALUE);
                    }
                    return -WIN_LOOSE_VALUE;
                }
            case PAT:
                node.setState(State.PAT);
                break;
            case REPETITION_X3:
                node.setState(State.REPETITION_X3);
                break;
            case REPEAT_50:
                node.setState(State.REPEAT_50);
                break;
            case NOT_ENOUGH_PIECES:
                node.setState(State.NOT_ENOUGH_PIECES);
                break;
            case NB_MOVES_200:
                node.setState(State.NB_MOVES_200);
                break;
            default:
        }
        logFile.info("#{} [{} - {}] move:{} {} RETURN: 0", depth, color, simulatedPlayerColor, e.getLastMove(),
                e.getTypeOfEnding());
        node.setExpectedReward(0);
        return 0;
    }

    private String sequenceMoves(MCTSNode node) {
        if (node == this.parent)
            return "";
        return sequenceMoves(node.getParent()) + " -> " + node.getBasicMove().toString();
    }

    public Move getRandomMove(final Moves bestNodes) {
        Collections.shuffle(bestNodes, rand);
        return bestNodes.get(0);
    }

    public MCTSNode getRandomMove(List<MCTSNode> nodes, Game game) {
        Collections.shuffle(nodes, rand);
        return nodes.get(0);
    }

    public Statistic getStatistic() {
        return statistic;
    }

}
