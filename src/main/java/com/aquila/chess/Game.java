/**
 *
 */
package com.aquila.chess;

import com.aquila.chess.exception.ChessPositionException;
import com.aquila.chess.exception.EndOfGameException;
import com.aquila.chess.exception.EndOfGameException.TypeOfEnding;
import com.aquila.chess.exception.NotInitializedGameException;
import com.aquila.chess.pgn.PGNParser;
import com.aquila.chess.pieces.*;
import com.aquila.chess.player.ChessPlayer;
import com.aquila.chess.player.mcts.FixPlayer;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.*;

public class Game {

    static private Logger logger = LoggerFactory.getLogger(Game.class);
    @SuppressWarnings("unused")
    static private Logger logFile = LoggerFactory.getLogger("FILE");

    final private String name;

    final private Board board;

    private final Map<Color, ChessPlayer> players = new HashMap<>();

    final private Moves movesGame = new Moves();

    private boolean isInitialized = false;

    private int nbMovesFor50End = 0;

    private Color colorToPlay = Color.WHITE;
    private boolean logBoard = false;

    /**
     * @param board
     * @param playerWhite
     * @param playerBlack
     * @throws ChessPositionException
     */
    public Game(final Board board, final ChessPlayer playerWhite, final ChessPlayer playerBlack)
            throws ChessPositionException {
        this.board = board;
        this.name = "Aquila Game";
        players.put(Color.WHITE, playerWhite);
        players.put(Color.BLACK, playerBlack);
        playerWhite.init(this, Color.WHITE);
        playerBlack.init(this, Color.BLACK);
    }

    public void setPlayers(final FixPlayer playerWhite, final FixPlayer playerBlack) {
        players.put(Color.WHITE, playerWhite);
        players.put(Color.BLACK, playerBlack);
        playerWhite.init(this, Color.WHITE);
        playerBlack.init(this, Color.BLACK);
    }

    public Map<Color, ChessPlayer> getPlayers() {
        return players;
    }

    public Game(final Board board) {
        this.board = board;
        this.name = "Copy (for simulation)";
    }

    public final void init() {
        players.get(Color.WHITE).initCastlingPossibility();
        players.get(Color.BLACK).initCastlingPossibility();
        isInitialized = true;
    }

    /**
     * Create a new Game from the current game with the given players
     *
     * @param playerWhite the white player
     * @param playerBlack the black player
     * @return
     * @throws ChessPositionException
     */
    public Game copy(final ChessPlayer playerWhite, final ChessPlayer playerBlack) throws ChessPositionException {
        final Board board = new Board();
        final Game game = new Game(board);
        playerWhite.inheritProperties(this.getPlayer(Color.WHITE), playerBlack, game);
        playerBlack.inheritProperties(this.getPlayer(Color.BLACK), playerWhite, game);
        playerWhite.installPieces();
        playerBlack.installPieces();
        game.players.put(Color.WHITE, playerWhite);
        game.players.put(Color.BLACK, playerBlack);
        game.nbMovesFor50End = this.nbMovesFor50End;
        game.isInitialized = this.isInitialized;
        // copy the last 12 moves to detect repetition
        int totalLength = this.movesGame.size();
        int length = totalLength >= 12 ? 12 : totalLength;
        for (int i = 0; i < length; i++) {
            game.movesGame.add(this.movesGame.get(i));
        }
        // logFile.info("GameCopy.Moves: {}", game.movesGame.toString());
        // game.check();
        return game;
    }

    /**
     * place all pieces from a normal game
     *
     * @throws ChessPositionException
     */
    public final void initWithAllPieces() throws ChessPositionException {
        // Pawns
        for (int i = 0; i < 8; i++) {
            players.get(Color.WHITE).addPiece(new Pawn(Color.WHITE, Location.get(i, 1)));
            players.get(Color.BLACK).addPiece(new Pawn(Color.BLACK, Location.get(i, 6)));
        }
        // Rook
        players.get(Color.WHITE).addPiece(new Rook(Color.WHITE, Location.get(0, 0)));
        players.get(Color.WHITE).addPiece(new Rook(Color.WHITE, Location.get(7, 0)));
        players.get(Color.BLACK).addPiece(new Rook(Color.BLACK, Location.get(0, 7)));
        players.get(Color.BLACK).addPiece(new Rook(Color.BLACK, Location.get(7, 7)));

        // Knight
        players.get(Color.WHITE).addPiece(new Knight(Color.WHITE, Location.get(1, 0)));
        players.get(Color.WHITE).addPiece(new Knight(Color.WHITE, Location.get(6, 0)));
        players.get(Color.BLACK).addPiece(new Knight(Color.BLACK, Location.get(1, 7)));
        players.get(Color.BLACK).addPiece(new Knight(Color.BLACK, Location.get(6, 7)));
        // Bishop
        players.get(Color.WHITE).addPiece(new Bishop(Color.WHITE, Location.get(2, 0)));
        players.get(Color.WHITE).addPiece(new Bishop(Color.WHITE, Location.get(5, 0)));
        players.get(Color.BLACK).addPiece(new Bishop(Color.BLACK, Location.get(2, 7)));
        players.get(Color.BLACK).addPiece(new Bishop(Color.BLACK, Location.get(5, 7)));
        // Queen
        players.get(Color.WHITE).addPiece(new Queen(Color.WHITE, Location.get(3, 0)));
        players.get(Color.BLACK).addPiece(new Queen(Color.BLACK, Location.get(3, 7)));
        // King
        players.get(Color.WHITE).addPiece(new King(Color.WHITE, Location.get(4, 0)));
        players.get(Color.BLACK).addPiece(new King(Color.BLACK, Location.get(4, 7)));
        init();
    }

    public final Move undo() throws ChessPositionException {
        if (isInitialized == false)
            throw new NotInitializedGameException();
        final Move move = this.movesGame.pop();
        this.players.get(move.getPiece().getColor()).unmove(move);
        return move;
    }

    public final Move play() throws EndOfGameException, ChessPositionException {
        return play(false);
    }

    public final Move play(final boolean simulation) throws EndOfGameException, ChessPositionException {
        final Color color = this.colorToPlay;
        if (isInitialized == false)
            throw new NotInitializedGameException();
        final ChessPlayer currentPlayer = players.get(color);
        final ChessPlayer opponentPlayer = players.get(color.complement());
        final Moves moves = currentPlayer.getPossibleLegalMoves();
        try {
            isEndOfGame(currentPlayer, opponentPlayer, moves);
        } catch (EndOfGameException endOfGameException) {
            this.movesGame.peekLast().setEndGameException(endOfGameException);
            throw endOfGameException;
        }
        final Move move = currentPlayer.play(movesGame.peekFirst(), moves);
        final Moves movesPlayer = opponentPlayer.getPossibleLegalMoves();
        if (movesPlayer.setPossibleKingCheck(opponentPlayer)) {
            move.setCheck(true);
        }
        if (move.isGettingPromoted()) {
            final Class<? extends Piece> promotedPieceClass = currentPlayer.doPromote(board, move);
            try {
                final Constructor<? extends Piece> constructor = promotedPieceClass.getConstructor(Color.class,
                        Location.class);
                final Piece promotedPiece = constructor.newInstance(currentPlayer.getColor(), move.getEndLocation());
                move.setPromotedPiece(promotedPiece);
            } catch (InstantiationException | IllegalAccessException | IllegalArgumentException
                    | InvocationTargetException | NoSuchMethodException | SecurityException e) {
                logger.error("Problem when creating the promoted piece", e);
            }
        }
        currentPlayer.move(move, simulation, false);
        if (move.getCapturePiece() == null && !(move.getPiece() instanceof Pawn))
            nbMovesFor50End++;
        else
            nbMovesFor50End = 0;
        if (move.getCapturePiece() != null)
            opponentPlayer.processCastlingCancelationFromOpponentMove(move);
        this.movesGame.push(move);
        this.colorToPlay = this.colorToPlay.complement();
        if (!simulation && this.logBoard) {
            logFile.warn("[{}] MOVE:{} - createCheck: {} - capturePiece:{} - canBeCaptured:{} - promoted2Piece:{}", move.getColor(),
                    move.toString(), move.isCreateCheck(), move.getCapturePiece(), move.getCanBeCaptured(), move.getPromotedPiece());
            logger.warn(this.getBoard().toString());
        }

        return move;
    }

    /**
     * Check if this is not the end of the game <a href=
     * "https://www.chess.com/article/view/how-chess-games-can-end-8-ways-explained#:~:text=One%20of%20the%20most%20common,checking%20piece%20cannot%20be%20captured.">Rule
     * for the end of game</>
     *
     * @param currentPlayer
     * @return
     * @throws EndOfGameException
     * @throws ChessPositionException
     */
    public final void isEndOfGame(final ChessPlayer currentPlayer, final ChessPlayer opponentPlayer, final Moves moves) throws EndOfGameException, ChessPositionException {
        if (moves == null || moves.size() == 0) {
            final Moves movesOpponent = opponentPlayer.getPossibleLegalMoves();
            if (movesOpponent.setPossibleKingCheck(currentPlayer)) {
                // if (currentPlayer.getKing().isCheck()) {
                throw new EndOfGameException(currentPlayer.getColor().complement(), "ChessMate", TypeOfEnding.CHESSMATE,
                        movesGame.peek());
            } else {
                throw new EndOfGameException(Color.NOT_DEFINED, "PAT: no legal move possible but king not in chess",
                        TypeOfEnding.PAT, movesGame.peek());
            }
        }
        testEnoughMaterials();
        // Check or PATH (no legal moves found
        // 50 moves without capture or pawn move
        if (nbMovesFor50End == 50)
            throw new EndOfGameException(Color.NOT_DEFINED, "50 moves without capture or pawn move",
                    TypeOfEnding.REPEAT_50, movesGame.peek());

        // Repetition (a position arises three times in a raw)
        final int numberOfMoves = movesGame.size();
        if (numberOfMoves >= 12) {
            final Move move[][] = new Move[3][4];
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 4; j++) {
                    move[i][j] = this.movesGame.get(j + (4 * i));
                }
            }
            boolean repetition = true;
            for (int j = 0; j < 4; j++) {
                if (!(move[0][j].equals(move[1][j]) && move[1][j].equals(move[2][j]))) {
                    repetition = false;
                    break;
                }
            }
            if (repetition) {
                throw new EndOfGameException(Color.NOT_DEFINED, "Repetition: 3*4 rounds", TypeOfEnding.REPETITION_X3,
                        movesGame.peek());
            }
        }
        if (numberOfMoves >= 300) {
            throw new EndOfGameException(Color.NOT_DEFINED, "Number of Move >=200", TypeOfEnding.NB_MOVES_200,
                    movesGame.peek());
        }
    }

    private void testEnoughMaterials() throws EndOfGameException {
        final ChessPlayer whitePlayer = this.players.get(Color.WHITE);
        final ChessPlayer blackPlayer = this.players.get(Color.BLACK);
        final boolean whiteKingAlone = whitePlayer.getPieces().size() == 1;
        final boolean whiteHasOnly2knights = whitePlayer.getPieces().size() == 3
                && whitePlayer.nbPiecesOfType(Knight.class) == 2;
        final boolean whiteHasOnly1knight = whitePlayer.getPieces().size() == 2
                && whitePlayer.nbPiecesOfType(Knight.class) == 1;
        final boolean whiteHasOnly1Bishop = whitePlayer.getPieces().size() == 2
                && whitePlayer.nbPiecesOfType(Bishop.class) == 1;
        final boolean blackKingAlone = blackPlayer.getPieces().size() == 1;
        final boolean blackHasOnly2knights = blackPlayer.getPieces().size() == 3
                && blackPlayer.nbPiecesOfType(Knight.class) == 2;
        final boolean blackHasOnly1knight = blackPlayer.getPieces().size() == 2
                && blackPlayer.nbPiecesOfType(Knight.class) == 1;
        final boolean blackHasOnly1Bishop = blackPlayer.getPieces().size() == 2
                && blackPlayer.nbPiecesOfType(Bishop.class) == 1;

        if ((whiteKingAlone || whiteHasOnly2knights || whiteHasOnly1knight || whiteHasOnly1Bishop) && //
                (blackKingAlone || blackHasOnly2knights || blackHasOnly1knight || blackHasOnly1Bishop))
            throw new EndOfGameException(Color.NOT_DEFINED, "Not enought materials", TypeOfEnding.NOT_ENOUGH_PIECES,
                    movesGame.peek());
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer();
        this.players.forEach((color, player) -> {
            sb.append(color + " -> " + player.toString());
            sb.append("\n");
        });
        sb.append(ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE));
        sb.append(board.toString());
        sb.append("\nPGN format to use with -> https://lichess.org/paste ");
        sb.append("\n------------ P G N -------------\n");
        sb.append(toPGN());
        sb.append("\n------------ P G N -------------\n");
        return sb.toString();
    }

    /**
     * <ul>
     * <li><a href="https://fr.wikipedia.org/wiki/Portable_Game_Notation">Portable
     * Game Notation</a></li>
     * <li><a href="https://lichess.org/import">Example site to import PGN
     * file</a></li>
     * </ul>
     *
     * @return a PGN (Portable Game Notation) of the current game
     */
    @SuppressWarnings("incomplete-switch")
    public String toPGN() {
        final StringBuffer sb = new StringBuffer();
        sb.append(String.format("[Event \"%s\"]\n", this.name));
        sb.append(String.format("[Site \"%S\"]\n", "Mougins 06250"));
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy.MM.dd");
        final String date = simpleDateFormat.format(new Date());
        sb.append(String.format("[Date \"%s\"]\n", date));// "1992.11.04"));
        sb.append(String.format("[Round \"%d\"]\n", this.movesGame.size()));
        sb.append(String.format("[White \"%s\"]\n", this.players.get(Color.WHITE).getName()));
        sb.append(String.format("[Black \"%s\"]\n", this.players.get(Color.BLACK).getName()));
        final EndOfGameException endOfGameException = this.movesGame.peekLast() == null ? null
                : this.movesGame.peekLast().getEndOfGameException();
        String result = "*";
        if (endOfGameException != null) {
            switch (endOfGameException.getTypeOfEnding()) {
                case CHESSMATE:
                    switch (endOfGameException.getColor()) {
                        case WHITE:
                            result = "1-0";
                            break;
                        case BLACK:
                            result = "0-1";
                            break;
                    }
                    break;
                case PAT:
                case REPEAT_50:
                case REPETITION_X3:
                case NOT_ENOUGH_PIECES:
                case NB_MOVES_200:
                    result = "1/2-1/2";
                    break;
                default:
                    break;
            }
        }
        sb.append(String.format("[Result \"%s\"]\n", result)); // [Result "0-1"], [Result "1-0"], [Result "1/2-1/2"],
        // [Result "*"]
        movesToPGN(sb);
        return sb.toString();
    }

    private void movesToPGN(final StringBuffer sb) {
        int i = 1;
        int nbCol = 0;

        final ListIterator<Move> it = movesGame.listIterator(movesGame.size());
        while (it.hasPrevious()) {
            nbCol += ("" + i).length() + 9;
            if (nbCol > 70) {
                nbCol = 0;
                sb.append("\n");
            }
            if ((i & 1) == 1) {
                sb.append((i / 2 + 1) + ".");
            }
            final Move move = it.previous();
            sb.append(move.toPGN());
            sb.append(" ");
            i++;
        }
    }

    private String toSHA1(final byte[] convertme) throws NoSuchAlgorithmException {
        final MessageDigest md = MessageDigest.getInstance("SHA-1");
        return Base64.getEncoder().encodeToString(md.digest(convertme));
    }

    public String toSHA1() throws NoSuchAlgorithmException {
        final StringBuffer sb = new StringBuffer();
        movesToPGN(sb);
        return toSHA1(sb.toString().getBytes());
    }

    /**
     * @return the same hashcode as {@link Board#hashCode()}
     */
    @Override
    public int hashCode() {
        return board.hashCode();
    }

    /**
     * @return the same hashcode as {@link Board#hashCode()}
     */
    public int hashCode(Move move) {
        return board.hashCode(move);
    }

    public final Moves getPossibleLegalMoves(final Color color) throws ChessPositionException {
        if (isInitialized == false)
            throw new NotInitializedGameException();
        return this.players.get(color).getPossibleLegalMoves();
    }

    public final Moves getPossibleLegalMoves(final Piece piece) throws ChessPositionException {
        return this.players.get(piece.getColor()).getPossibleLegalMoves();
    }

    public final ChessPlayer getPlayer(final Color color) {
        return this.players.get(color);
    }

    public final King getKing(final Color color) {
        return this.players.get(color).getKing();
    }

    public int getNbStep() {
        return movesGame.size();
    }

    public void playPGN(String pgn) {
        PGNParser parser = new PGNParser();
        parser.PlayPGN(pgn, this);
    }

    public Color getColorToPlay() {
        return colorToPlay;
    }

    public Board getBoard() {
        return this.board;
    }

    public void setColorToPlay(Color colorToPlay) {
        this.colorToPlay = colorToPlay;
    }

    public Piece getPiece(Location location) {
        return this.board.getPiece(location.getX(), location.getY());
    }

    public final Piece getPiece(final String coordinate) throws ChessPositionException {
        return this.board.getPiece(coordinate);
    }

    public int getNbMovesFor50End() {
        return nbMovesFor50End;
    }

    public void setNextMove(Move move) {
        this.players.get(move.getColor()).setNextMove(move);
    }

    public void logBoard(boolean logBoard) {
        this.logBoard = logBoard;
    }

    public boolean getLogBoard() {
        return this.logBoard;
    }

    public boolean IsRepeteatedMove(final Move move) {
        return this.movesGame.find(move) != null ||
                this.movesGame.find(move.reverse()) != null ;
    }
}
