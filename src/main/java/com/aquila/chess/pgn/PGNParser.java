/**
 * 
 */
package com.aquila.chess.pgn;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.aquila.chess.Game;
import com.aquila.chess.Move;
import com.aquila.chess.Moves;
import com.aquila.chess.Piece;
import com.aquila.chess.exception.ChessPositionException;
import com.aquila.chess.exception.EndOfGameException;
import com.aquila.chess.pieces.Bishop;
import com.aquila.chess.pieces.Color;
import com.aquila.chess.pieces.King;
import com.aquila.chess.pieces.Knight;
import com.aquila.chess.pieces.Pawn;
import com.aquila.chess.pieces.Queen;
import com.aquila.chess.pieces.Rook;
import com.aquila.chess.player.ChessPlayer;

/**
 * Part of code copy from <a href=
 * "https://github.com/supareno/pgn-parser/blob/master/src/main/java/org/supareno/pgnparser/pgn/parser/PGNParser.java">supareno/pgn-parser</a><br/>
 * <br/>
 * Copyright 2008-2018 supareno
 *
 */
public class PGNParser {

	/**
	 * The String representation of the pattern used to match the attributes of the
	 * PGN file. The value of the pattern is set to {@literal \\[[^\\[]*\\]}
	 *
	 * @see #ATTRIBUTES_PATTERN
	 */
	public static final String ATTRIBUTES_STRING_PATTERN = "\\[[^\\[]*\\]";

	/**
	 * The String representation of the pattern used to check a number
	 * validity.<br />
	 * The value is set to {@code [1-9]+([0-9]+)?}.
	 */
	public static final String NUMBER_VALIDITY_STRING_PATTERN = "[1-9]+([0-9]+)?";

	/**
	 * The String representation of the pattern used to match a single hit.<br />
	 * This pattern matches hit value like {@code e5} or {@code R8c7}.<br />
	 * The value is set to as follow (divided in multilines to a better
	 * comprehension): <br />
	 *
	 * <pre>
	 * [a-h][x][a-h][36][e][.][p][.][\\+|#]?|       // en passant
	 * ([a-h][x])?[a-h][18]([=][RNBQ])?[\\+|#]?|    // pawn promotion
	 * [O][-][O]([-][O])?[\\+|#]?|                  // castle[ ]?
	 * [RNBQK]?[a-h]?[1-8]?[x]?[a-h][1-8][\\+|#]?   // every other move
	 * </pre>
	 *
	 * @see #SINGLE_HIT_PATTERN
	 */
	public static final String SINGLE_HIT_STRING_PATTERN = "([a-hA-H]+[1-8]{1}[=][A-Z]{1}[\\+|#]?|" + // promotion
			"[a-hrnqkA-HRNQK]{1}[1-8]{1}[\\+]?|" + // one letter / one number pattern
			// : simple hit
			"[a-hA-H]{1}[1-8]?[a-hA-H]{1,3}?[1-8]{1}[\\+|#]?|" + // complex hit
			"[O]+[\\-][O]+[\\-][O]+[\\+|#]?|" + // queenside castling hit
			"[O]+[\\-][O]+[\\+|#]?|" + // kingside castling hit
			"[a-hpA-HP]{1}[\\-][a-hpA-HP]{1}[\\+|#]?|" + //
			"[RNBQK]?[a-hp]?[1-8]?[x]?[a-hp][1-8][\\+|#]?)"; //
	/**
	 * The String representation of the pattern used to match the hits of the PGN
	 * file.<br>
	 * This pattern is composed in two parts: <br>
	 * The first one is for the hit number<br>
	 * {@code #NUMBER_VALIDITY_STRING_PATTERN} concatened with {@code [.][ ]?}<br>
	 * The second one is for the hit (composed in two same part seperate with a
	 * space)<br>
	 * {@code #SINGLE_HIT_STRING_PATTERN}
	 *
	 * @see #HITS_PATTERN
	 */
	public static final String HITS_STRING_PATTERN = NUMBER_VALIDITY_STRING_PATTERN + "[.]" + "[ ]?"
			+ SINGLE_HIT_STRING_PATTERN + "([ ]?" + SINGLE_HIT_STRING_PATTERN + "[ ]?)?";

	/**
	 * Pattern used to parse the attributes of the PGN file. It is compiled with the
	 * {@link PGNParser#ATTRIBUTES_STRING_PATTERN} pattern.
	 */
	public static final Pattern ATTRIBUTES_PATTERN = Pattern.compile(ATTRIBUTES_STRING_PATTERN);

	/**
	 * Pattern used to parse the hits of the PGN file. It is compiled with the
	 * {@link PGNParser#HITS_STRING_PATTERN} pattern.
	 */
	public static final Pattern HITS_PATTERN = Pattern.compile(HITS_STRING_PATTERN);

	/**
	 * Pattern used to check the validity of a Number. It is compiled with the
	 * {@link PGNParser#NUMBER_VALIDITY_STRING_PATTERN} pattern.
	 */
	public static final Pattern NUMBER_VALIDITY_PATTERN = Pattern.compile(NUMBER_VALIDITY_STRING_PATTERN);

	/**
	 * Pattern used to parse one hit. It is compiled with the
	 * {@link PGNParser#SINGLE_HIT_STRING_PATTERN} pattern.
	 */
	public static final Pattern SINGLE_HIT_PATTERN = Pattern.compile(SINGLE_HIT_STRING_PATTERN);

	/**
	 * Parses the PGN hits. Uses the {@link #HITS_PATTERN} Pattern to parse the
	 * hits.
	 *
	 * @param pgn  the {@code PGNGame} to fill
	 * @param hits the String representation of the hits to parse
	 * @return the {@code PGNGame} filled with the attributes found
	 * @see PGNParser#HITS_PATTERN
	 */
	private List<Hit> parseHits(final String hits) {
		StringBuilder newHit = new StringBuilder();
		String[] strings = hits.split("\n");
		for (String s : strings) {
			newHit.append(s.trim());
		}
		List<Hit> list = new ArrayList<>();
		Matcher matcher = HITS_PATTERN.matcher(newHit.toString());
		while (matcher.find()) {
			String[] str = matcher.group().split("\\.");
			if (str.length < 1) {
				continue;
			}
			Hit hit = new Hit();
			hit.setNumber(str[0]);
			hit.setContent(normalizeHit(str[1]));
			list.add(hit);
		}
		return list;
	}

	public void PlayPGN(final String hitsSz, Game game) {
		List<Hit> hits = this.parseHits(hitsSz);
		Map<Color, ChessPlayer> players = game.getPlayers();
		hits.stream().forEach(ele -> {
			String[] separatedHit = ele.getHitSeparated();
			Color currentColor = Color.WHITE;
			Moves moves;
			for (String hit : separatedHit) {
				try {
					moves = game.getPossibleLegalMoves(currentColor);
					Move move = createMoveFromPGN(hit, moves);
					players.get(currentColor).setNextMove(move);
					game.play();
				} catch (ChessPositionException e) {
					e.printStackTrace();
				} catch (EndOfGameException e) {
					e.printStackTrace();
				}
				currentColor = currentColor.complement();
			}
		});

	}

	private Move createMoveFromPGN(String hit, Moves moves) {
		Class<? extends Piece> clazz = Pawn.class;
		switch (hit.charAt(0)) {
		case 'N':
			clazz = Knight.class;
			break;
		case 'B':
			clazz = Bishop.class;
			break;
		case 'R':
			clazz = Rook.class;
			break;
		case 'Q':
			clazz = Queen.class;
			break;
		case 'K':
			clazz = King.class;
			break;
		case 'P':
			break;
		}
		String end = hit.substring(hit.length() - 2);
		Moves specialMoves = moves.find(clazz);
		return specialMoves.stream().filter(move -> move.getEndLocation().coordAlgebrique().equals(end)).findFirst()
				.get();
//		return specialMoves.stream().filter(move -> move.getEndChessBox().coordAlgebrique().equals(end)).findFirst().get();
	}

	/**
	 * Return the hit normalized.
	 *
	 * @param hitToNormalize the to normalized
	 * @return the hit normalized.
	 */
	private String normalizeHit(final String hitToNormalize) {
		StringBuilder sb = new StringBuilder();
		String[] hitSplitted = hitToNormalize.split(" ");
		for (String str : hitSplitted) {
			if (str.trim().length() > 0) {
				if (SINGLE_HIT_PATTERN.matcher(str).matches()) {
					sb.append(str).append(" ");
				}
			}
		}
		return sb.toString().trim();
	}
}
