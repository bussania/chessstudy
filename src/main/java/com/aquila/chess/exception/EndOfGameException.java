/**
 * 
 */
package com.aquila.chess.exception;

import com.aquila.chess.Move;
import com.aquila.chess.pieces.Color;

/**
 * @author bussa
 *
 */
public class EndOfGameException extends Exception {

	static public enum TypeOfEnding {
		UNKNOWN,
		CHESSMATE,
		PAT,
		REPEAT_50,
		REPETITION_X3,
		NOT_ENOUGH_PIECES,
		NB_MOVES_200
	};

	private final Color color;

	private final TypeOfEnding typeOfEnding;

	private final Move lastMove;

	private final String reason;

	/**
	 * 
	 */
	private static final long serialVersionUID = 7944574066105008303L;

	/**
	 * @param color
	 * @param string
	 * @param pat
	 */
	public EndOfGameException(final Color color, final String reason, final TypeOfEnding typeOfEnding, final Move lastMove) {
		super("END OF GAME: ");
		this.color = color;
		this.reason = reason;
		this.typeOfEnding = typeOfEnding;
		this.lastMove = lastMove;
	}

//	public EndOfGameException(final Color color, final TypeOfEnding typeOfEnding) {
//		this(color, "", typeOfEnding, null);
//	}
//
//	public EndOfGameException(final Color color, final String reason, final TypeOfEnding typeOfEnding) {
//		this(color, reason, typeOfEnding, null);
//	}

	@Override
	public String toString() {
		return String.format("END OF GAME. Reason: %s Color: %s TypeOfEnding: %s lastMove:%s", this.reason, this.color, this.typeOfEnding, this.lastMove);
	}

	@Override
	public String getMessage() {
		return toString();
	}

	@Override
	public String getLocalizedMessage() {
		return toString();
	}

	public TypeOfEnding getTypeOfEnding() {
		return this.typeOfEnding;
	}

	public Color getColor() {
		return this.color;
	}

	public Move getLastMove() {
		return this.lastMove;
	}

}
