/**
 * 
 */
package com.aquila.chess.exception;

/**
 * @author bussa
 *
 */
public class ChessPositionException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7944574066105008303L;

	public ChessPositionException(String msg) {
		super(msg);
	}

}
