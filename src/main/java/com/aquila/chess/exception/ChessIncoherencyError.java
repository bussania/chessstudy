package com.aquila.chess.exception;

public class ChessIncoherencyError extends Error {

	public ChessIncoherencyError(String msg) {
		super(msg);
	}

	public ChessIncoherencyError(String string, Object... args) {
		super(String.format(string, args));
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -5252684289457706921L;

}
