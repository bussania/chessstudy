package com.aquila.chess.manager;

import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.aquila.chess.Game;
import com.aquila.chess.exception.EndOfGameException;

public class Record {

	static final DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.FULL, DateFormat.FULL,
			new Locale("EN", "en"));

	public static enum Status {
		NORMAL, SWITCHING
	}

	public Status status;
	public int totalWhiteWin = 0;
	public int totalBlackWin = 0;
	public int totalDrawn = 0;
	public int intermediateWhiteWin = 0;
	public int intermediateBlackWin = 0;
	public int intermediateDrawn = 0;
	public int whiteWin = 0;
	public int blackWin = 0;
	public int drawn = 0;
	public int intermediateNbGame = 0;
	public String reason;
	public int percentage;
	public int round;
	public Date startDate;
	public Date endDate;
	public long durationSeconds;
	public double nnScore;
	public String gameSha1;
	public String png;

	public Record(String[] recordSz) throws ParseException {
		int i = 0;
		this.status = Status.valueOf(recordSz[i++]);
		this.intermediateNbGame = Integer.valueOf(recordSz[i++]);
		this.totalWhiteWin = Integer.valueOf(recordSz[i++]);
		this.totalBlackWin = Integer.valueOf(recordSz[i++]);
		this.totalDrawn = Integer.valueOf(recordSz[i++]);
		this.intermediateWhiteWin = Integer.valueOf(recordSz[i++]);
		this.intermediateBlackWin = Integer.valueOf(recordSz[i++]);
		this.intermediateDrawn = Integer.valueOf(recordSz[i++]);
		this.whiteWin = Integer.valueOf(recordSz[i++]);
		this.blackWin = Integer.valueOf(recordSz[i++]);
		this.drawn = Integer.valueOf(recordSz[i++]);
		this.reason = recordSz[i++];
		this.percentage = Integer.valueOf(recordSz[i++]);
		this.round = Integer.valueOf(recordSz[i++]);
		this.startDate = dateFormat.parse(recordSz[i++]);
		this.endDate = dateFormat.parse(recordSz[i++]);
		this.durationSeconds = Integer.valueOf(recordSz[i++]);
		this.nnScore = Double.valueOf(recordSz[i++]);
		this.gameSha1 = recordSz[i++];
		this.png = recordSz[i++];
	}

	@SuppressWarnings("incomplete-switch")
	public Record(Record lastRecord, Status status, int intermediateNbGame, EndOfGameException e, Sequence sequence,
			Game game, double nnScore) throws NoSuchAlgorithmException {
		long endDate = System.currentTimeMillis();
		this.status = status;
		switch (e.getTypeOfEnding()) {
		case NOT_ENOUGH_PIECES:
		case PAT:
		case REPEAT_50:
		case REPETITION_X3:
		case NB_MOVES_200:
			this.drawn = 1;
			break;
		case CHESSMATE:
			switch (e.getColor()) {
			case WHITE:
				this.whiteWin = 1;
				break;
			case BLACK:
				this.blackWin = 1;
				break;
			}
		}
		if (lastRecord != null) {
			if (status == Status.SWITCHING) {
				this.intermediateWhiteWin = this.whiteWin;
				this.intermediateBlackWin = this.blackWin;
				this.intermediateDrawn = this.drawn;
			} else {
				this.intermediateWhiteWin = lastRecord.intermediateWhiteWin + this.whiteWin;
				this.intermediateBlackWin = lastRecord.intermediateBlackWin + this.blackWin;
				this.intermediateDrawn = lastRecord.intermediateDrawn + this.drawn;
			}
			this.totalWhiteWin = lastRecord.totalWhiteWin + this.whiteWin;
			this.totalBlackWin = lastRecord.totalBlackWin + this.blackWin;
			this.totalDrawn = lastRecord.totalDrawn + this.drawn;
		} else {
			this.intermediateWhiteWin = this.whiteWin;
			this.intermediateBlackWin = this.blackWin;
			this.intermediateDrawn = this.drawn;
			this.totalWhiteWin = this.whiteWin;
			this.totalBlackWin = this.blackWin;
			this.totalDrawn = this.drawn;
		}
		this.intermediateNbGame = intermediateNbGame;
		this.reason = e.getColor() + "-" + e.getTypeOfEnding().toString();
		this.startDate = new Date(sequence.startDate);
		this.endDate = new Date(endDate);
		this.durationSeconds = endDate - sequence.startDate;
		if (this.intermediateWhiteWin + this.intermediateBlackWin > 0)
			this.percentage = ((this.intermediateWhiteWin) * 100)
					/ (this.intermediateWhiteWin + this.intermediateBlackWin);
		else
			this.percentage = 0;
		this.round = sequence.nbStep;
		this.nnScore = nnScore;
		this.gameSha1 = game.toSHA1();
		this.png = Base64.getEncoder().encodeToString(game.toPGN().getBytes());
	}

	public String[] toArray() {
		List<String> records = new ArrayList<>();
		records.add(this.status.toString());
		records.add(String.valueOf(this.intermediateNbGame));
		records.add(String.valueOf(this.totalWhiteWin));
		records.add(String.valueOf(this.totalBlackWin));
		records.add(String.valueOf(this.totalDrawn));
		records.add(String.valueOf(this.intermediateWhiteWin));
		records.add(String.valueOf(this.intermediateBlackWin));
		records.add(String.valueOf(this.intermediateDrawn));
		records.add(String.valueOf(this.whiteWin));
		records.add(String.valueOf(this.blackWin));
		records.add(String.valueOf(this.drawn));
		records.add(this.reason);
		records.add(String.valueOf(this.percentage));
		records.add(String.valueOf(this.round));
		records.add(dateFormat.format(this.startDate));
		records.add(dateFormat.format(this.endDate));
		records.add(String.valueOf(this.durationSeconds));
		records.add(String.valueOf(this.nnScore));
		records.add(this.gameSha1);
		records.add(this.png);
		return records.toArray(new String[0]);
	}

}