package com.aquila.chess.manager;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aquila.chess.Game;
import com.aquila.chess.exception.EndOfGameException;
import com.aquila.chess.manager.Record.Status;
import com.aquila.chess.player.mcts.ResultGame;
import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import com.opencsv.exceptions.CsvException;

public class GameManager {

	static private Logger logFile = LoggerFactory.getLogger("FILE");

	private File file;
	String filename;
	int nbGames = 0;

	private Record lastRecord = null;

	private int maxPercentage;

	private int intermediateNbGame;

	private int minNbGame;

	public GameManager(String filename, int minNbGame, int maxPercentage)
			throws FileNotFoundException, IOException, CsvException {
		this.minNbGame = minNbGame;
		this.maxPercentage = maxPercentage;
		this.filename = filename;
		file = new File(filename);
		if (file.canRead()) {
			try (CSVReader reader = new CSVReader(new FileReader(file))) {
				reader.forEach(recordSz -> {
					nbGames++;
					try {
						lastRecord = new Record(recordSz);
						this.intermediateNbGame = lastRecord.intermediateNbGame;
					} catch (ParseException e) {
						logFile.error("Error when reading: " + file, e);
					}
				});
				reader.close();
				logFile.info("Number of line read from {}: {}", file.getAbsolutePath(), reader.getLinesRead());
				logFile.info("Number of games played: {}", nbGames);
			}
		}
	}

	public Sequence createSequence() {
		return new Sequence();
	}

	public Status endGame(ResultGame resultGame, Game game, double nnScore, EndOfGameException e, Sequence sequence)
			throws IOException, NoSuchAlgorithmException {
		Status retStatus = Status.NORMAL;

		this.intermediateNbGame++;
		this.nbGames++;
		Record record = new Record(lastRecord, retStatus, this.intermediateNbGame, e, sequence, game, nnScore);
		logFile.info("Intermediate: whites:{} blacks:{} drawn:{} percentage White win:{} %"//
				, record.intermediateWhiteWin//
				, record.intermediateBlackWin//
				, record.intermediateDrawn//
				, record.percentage);
		if (this.intermediateNbGame >= this.minNbGame) {
			logFile.warn(String.format(
					"\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n%%%% PERCENTAGE: %d (%d <-> %d)\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%",
					record.percentage, record.whiteWin, record.blackWin));
			if (record.percentage >= this.maxPercentage) {
				retStatus = Status.SWITCHING;
				this.intermediateNbGame = 1;
				record = new Record(lastRecord, retStatus, this.intermediateNbGame, e, sequence, game, nnScore);
			}
		}
		FileWriter fileWriter = new FileWriter(filename, true);
		try (CSVWriter writer = new CSVWriter(fileWriter)) {
			writer.writeNext(record.toArray());
		}
		fileWriter.close();
		this.lastRecord = record;
		return retStatus;
	}

	public int getNbGames() {
		return this.nbGames;
	}
}
